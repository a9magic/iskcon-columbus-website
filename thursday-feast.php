<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <title>Thursday Feast</title>
    <?php
    include('meta_links.php');
    ?>
</head>

<body class="events-template-default single single-events postid-74 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">

    <?php
    include('header.php');
    ?>
    <!-- Blog Heading -->
    <section id="blog-heading">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text">
                    <h1 class="blog-title text-center">Thursday Feast</h1>
                </div><!-- /.heading-text -->
            </div><!-- /.container -->
        </div><!-- /.heading-fullwidth -->
    </section><!-- /#blog-heading -->
    <!-- Blog Heading end -->

    <!-- Blog Page Container -->
    <section id="blog-page-container">
        <div class="blog-page-wrapper">
            <div class="container">
                <div class="row">


                    <div class="col-md-8 wow fadeInLeft" data-wow-delay="0.4s">
                        <div class="all-event">
                            <div class="event-post">
                                <div class="events-date">
                                    <span class="event-date">29</span> <br>
                                    <span class="event-month">June</span>
                                </div>

                                <h2 class="uppercase"><a
                                            href=""
                                            title="ISKCON Columbus - Thursday Feast"> Thursday Feast</a></h2>
                                <div class="event-period">
									<span class="period-session">
										<i class="fa fa-clock-o"></i> 2 Hours <span class="separator"></span> <i
                                                class="fa fa-map-marker"></i> 379 West 8th Ave Columbus, OH									</span><span
                                            class="separator"></span>
                                    <?php include('socialmedia-share.php'); ?><!-- /.comments-share -->
                                </div><!-- /.event-period -->
                                <div class="feature-img">
                                    <img width="800" height="550"
                                         src="images/thursday-program-header.jpg"
                                         class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                         srcset="images/thursday-program-header.jpg 800w, images/thursday-program-header.jpg 300w"
                                         sizes="(max-width: 800px) 100vw, 800px"/></div>
                                <div class="event-description">
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-music base-color"></i></div>
                                        <div class="media-body">Lively kirtan (call-and-response chanting)<br></div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-bullhorn base-color"></i></div>
                                        <div class="media-body">Thought-provoking keynotes by eminent Bhakti Yogis,<br>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-heart base-color"></i></div>
                                        <div class="media-body">Mantra meditation<br></div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-cutlery base-color"></i></div>
                                        <div class="media-body">Evening Prasadam<br></div>
                                    </div>
                                </div>

                            </div>
                            <br/>

                            <h2 class="wow fadeInUp" data-wow-delay="0.4s">We welcome anyone, regardless of one’s belief
                                system, into an experience of selfless
                                loving service .</h2>


                        </div>
                    </div>
                    <div class="col-md-4 wow fadeInRight" data-wow-delay="0.4s">
                        <div class="event-location">
                            <h2 class="widget-title">View Location</h2>
                            <div class="eventMap">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3056.732827642373!2d-83.01912858510767!3d39.99207557941673!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88388eea2e220463%3A0x4f95e3ec9b88a211!2sISKCON+Temple!5e0!3m2!1sen!2sus!4v1494100183136"
                                        width="100%" height="100%" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>
                            </div>


                        </div>

                        <div class="event-detail">
                            <h2 class="widget-title">Event Details</h2>
                            <div class="address">
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-map-marker base-color"></i></div>
                                    <div class="media-body"> <strong>Where:</strong> 379 West 8th Ave Columbus, OH
                                        <br></div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-calendar-o base-color"></i></div>
                                    <div class="media-body"><strong>When:</strong> Every Thursday at <strong>6:30 PM</strong><br></div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-clock-o base-color"></i></div>
                                    <div class="media-body"><strong>Who:</strong> Everyone is invited to attend<br></div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-money base-color"></i></div>
                                    <div class="media-body"><strong>Cost:</strong> FREE <br></div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-user base-color"></i></div>
                                    <div class="media-body"><strong>What to wear:</strong> Casual<br></div>
                                </div>
                            </div>
                            <div>
                                <form action="//maps.google.com/maps" method="get" target="_blank">
                                    <input type="text" placeholder="Enter Your Location" name="saddr"/>
                                    <input type="hidden" name="daddr" value="379 West 8th Ave Columbus"/>
                                    <input type="submit" class="btn custom-btn"
                                           style="color: white; border-radius:20px;" value="Get directions"/>
                                </form>
                            </div>
                        </div>
                    </div>

                </div><!-- row -->
            </div> <!-- #container -->
        </div><!-- #blog-page-wrapper -->
    </section>

    <!-- gallery -->
    <div class="full-width">
        <div class="vc_row-fluid">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">
                    <div class="vc_empty_space" style="height: 20px"><span
                                class="vc_empty_space_inner"></span>
                    </div>

                    <!---->
                    <!--                    <a target="_blank"-->
                    <!--                       href="https://www.youtube.com/channel/UCbhrTLxLFZuH-n3Gz1HhJ-w?sub_confirmation=1">-->
                    <!--                        <h2 class="fa-image section-title wow fadeInUp" data-wow-delay="0.4s"><i-->
                    <!--                                    class="fa fa-image fa-2x"></i> Gallery </h2>-->
                    <!--                    </a>-->
                    <!-- /.section-title -->

                    <!--                    --><?php //include ('gallery.php')?>
                </div>
            </div>
        </div>
    </div>
    <!-- end gallery -->

    <!-- festival -->
    <div class="full-width">
        <div class="vc_row-fluid">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">
                    <div class="vc_empty_space" style="height: 20px"><span
                                class="vc_empty_space_inner"></span>
                    </div>


                    <a target="_blank"
                       href="https://www.youtube.com/channel/UCbhrTLxLFZuH-n3Gz1HhJ-w?sub_confirmation=1">
                        <h2 class="section-title wow fadeInUp" data-wow-delay="0.4s"><i
                                    class="fa fa-youtube fa-2x"></i> Watch
                            our Videos </h2>
                    </a>
                    <!-- /.section-title -->


                    <div class="portfolio gallery-section wow fadeInUp" data-wow-delay="0.6s">
                        <div class="container">

                            <div class="isotope-filters portfolio-filter">

                                <button class="button is-checked" data-sort-by="original-order">ALL
                                </button>
                                <button data-filter=".sundayFeast">Sunday Feast</button>
                                <button data-filter=".namayagna">Nama Yagna</button>
                                <button data-filter=".kirtans">Kirtans</button>
                                <button data-filter=".home-programs">Home Programs</button>

                            </div>
                        </div>
                        <div class="clearfix portfolio-item isotope-items isotope-masonry-items">

                            <div class="item  home-programs effect-oscar">
                                <img width="800" height="532"
                                     src="https://img.youtube.com/vi/30dkMLBerK8/mqdefault.jpg"
                                     class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                                <div class="item-description">
                                    <div class="item-link">
                                        <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                           href="https://www.youtube.com/watch?v=30dkMLBerK8">
                                            <i class="fa fa-desktop"></i>
                                        </a>
                                    </div><!-- /.item-link -->

                                </div>
                            </div>

                            <div class="item home-programs effect-oscar">
                                <img width="800" height="532"
                                     src="https://img.youtube.com/vi/o1rqSP1N6JI/mqdefault.jpg"
                                     class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                                <div class="item-description">
                                    <div class="item-link">
                                        <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                           href="https://www.youtube.com/watch?v=o1rqSP1N6JI">
                                            <i class="fa fa-desktop"></i>
                                        </a>
                                    </div><!-- /.item-link -->

                                </div>
                            </div>

                            <div class="item sundayFeast effect-oscar">
                                <img width="800" height="532"
                                     src="https://img.youtube.com/vi/e0LfkamYYJU/mqdefault.jpg"
                                     class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                                <div class="item-description">
                                    <div class="item-link">
                                        <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                           href="https://www.youtube.com/watch?v=e0LfkamYYJU"
                                           target="_blank">
                                            <i class="fa fa-desktop"></i>
                                        </a>
                                    </div><!-- /.item-link -->

                                </div>
                            </div>

                            <div class="item sundayFeast effect-oscar">
                                <img width="800" height="532"
                                     src="https://img.youtube.com/vi/o8aZRdKEVCE/mqdefault.jpg"
                                     class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                                <div class="item-description">
                                    <div class="item-link">
                                        <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                           href="https://www.youtube.com/watch?v=o8aZRdKEVCE">
                                            <i class="fa fa-desktop"></i>
                                        </a>
                                    </div><!-- /.item-link -->

                                </div>
                            </div>

                            <div class="item sundayFeast effect-oscar">
                                <img width="800" height="532"
                                     src="https://img.youtube.com/vi/hKhGu1XLsVk/mqdefault.jpg"
                                     class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                                <div class="item-description">
                                    <div class="item-link">
                                        <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                           href="https://www.youtube.com/watch?v=hKhGu1XLsVk">
                                            <i class="fa fa-desktop"></i>
                                        </a>
                                    </div><!-- /.item-link -->

                                </div>
                            </div>

                            <div class="item kirtans effect-oscar">
                                <img width="800" height="532"
                                     src="https://img.youtube.com/vi/Phi2IgLQaMA/mqdefault.jpg"
                                     class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                                <div class="item-description">
                                    <div class="item-link">
                                        <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                           href="https://www.youtube.com/watch?v=Phi2IgLQaMA">
                                            <i class="fa fa-desktop"></i>
                                        </a>
                                    </div><!-- /.item-link -->

                                </div>
                            </div>

                            <div class="item kirtans effect-oscar">
                                <img width="800" height="532"
                                     src="https://img.youtube.com/vi/QYWcuv3xcp8/mqdefault.jpg"
                                     class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                                <div class="item-description">
                                    <div class="item-link">
                                        <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                           href="https://www.youtube.com/watch?v=QYWcuv3xcp8">
                                            <i class="fa fa-desktop"></i>
                                        </a>
                                    </div><!-- /.item-link -->

                                </div>
                            </div>

                            <div class="item kirtans effect-oscar">
                                <img width="800" height="532"
                                     src="https://img.youtube.com/vi/ozhTXbAz1QI/mqdefault.jpg"
                                     class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                                <div class="item-description">
                                    <div class="item-link">
                                        <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                           href="https://www.youtube.com/watch?v=ozhTXbAz1QI">
                                            <i class="fa fa-desktop"></i>
                                        </a>
                                    </div><!-- /.item-link -->

                                </div>
                            </div>

                            <div class="item kirtans effect-oscar">
                                <img width="800" height="532"
                                     src="https://img.youtube.com/vi/0wO3pHKaABk/mqdefault.jpg"
                                     class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                                <div class="item-description">
                                    <div class="item-link">
                                        <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                           href="https://www.youtube.com/watch?v=0wO3pHKaABk">
                                            <i class="fa fa-desktop"></i>
                                        </a>
                                    </div><!-- /.item-link -->

                                </div>
                            </div>

                            <div class="item namayagna effect-oscar">
                                <img width="800" height="532"
                                     src="https://img.youtube.com/vi/6phMsWqeCHk/mqdefault.jpg"
                                     class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                                <div class="item-description">
                                    <div class="item-link">
                                        <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                           href="https://www.youtube.com/watch?v=6phMsWqeCHk">
                                            <i class="fa fa-desktop"></i>
                                        </a>
                                    </div><!-- /.item-link -->

                                </div>
                            </div>

                            <div class="item namayagna effect-oscar">
                                <img width="800" height="532"
                                     src="https://img.youtube.com/vi/BFKz_RbkbEA/mqdefault.jpg"
                                     class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                                <div class="item-description">
                                    <div class="item-link">
                                        <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                           href="https://www.youtube.com/watch?v=BFKz_RbkbEA">
                                            <i class="fa fa-desktop"></i>
                                        </a>
                                    </div><!-- /.item-link -->

                                </div>
                            </div>

                        </div><!-- /.YouTube videos -->

                        <a class="read-more"
                           href="https://www.youtube.com/channel/UCbhrTLxLFZuH-n3Gz1HhJ-w?sub_confirmation=1"
                           target="_blank">View
                            More</a>
                        <div class="clearfix"></div>
                    </div><!-- /.gallery-section -->


                    <script>
                        /*------------------------------------------------------------------------------------------------------------------*/
                        /*   isotop.
                         /*------------------------------------------------------------------------------------------------------------------*/
                        /*-------------------------- Isotope Init --------------------*/
                        jQuery(window).on("load resize", function (e) {

                            var $container = jQuery('.isotope-items'),
                                colWidth = function () {
                                    var w = $container.width(),
                                        columnNum = 1,
                                        columnWidth = 0;
                                    if (w > 1040) {
                                        columnNum = 4;
                                    }
                                    else if (w > 850) {
                                        columnNum = 2;
                                    }
                                    else if (w > 768) {
                                        columnNum = 2;
                                    }
                                    else if (w > 480) {
                                        columnNum = 2;
                                    }
                                    columnWidth = Math.floor(w / columnNum);

                                    //Isotop Version 1
                                    var $containerV1 = jQuery('.isotope-items');
                                    $containerV1.find('.item').each(function () {
                                        var $item = jQuery(this),
                                            multiplier_w = $item.attr('class').match(/item-w(\d)/),
                                            multiplier_h = $item.attr('class').match(/item-h(\d)/),
                                            width = multiplier_w ? columnWidth * multiplier_w[1] - 10 : columnWidth,
                                            height = multiplier_h ? columnWidth * multiplier_h[1] * 0.7 : columnWidth * 0.7;
                                        $item.css({width: width, height: height,});
                                    });


                                    return columnWidth;
                                },
                                isotope = function () {
                                    $container.isotope({
                                        resizable: true,
                                        itemSelector: '.item',
                                        masonry: {
                                            columnWidth: colWidth(),
                                            gutterWidth: 10
                                        }
                                    });
                                };
                            isotope();


                            // bind filter button click
                            jQuery('.isotope-filters').on('click', 'button', function () {
                                var filterValue = jQuery(this).attr('data-filter');
                                $container.isotope({filter: filterValue});
                            });

// change active class on buttons
                            jQuery('.isotope-filters').each(function (i, buttonGroup) {
                                var $buttonGroup = jQuery(buttonGroup);
                                $buttonGroup.on('click', 'button', function () {
                                    $buttonGroup.find('.active').removeClass('active');
                                    jQuery(this).addClass('active');
                                });
                            });


// Masonry Isotope
                            var $masonryIsotope = jQuery('.isotope-masonry-items').isotope({
                                itemSelector: '.item',
                            });

// bind filter button click
                            jQuery('.isotope-filters').on('click', 'button', function () {
                                var filterValue = jQuery(this).attr('data-filter');
                                $masonryIsotope.isotope({filter: filterValue});
                            });

// change active class on buttons
                            jQuery('.isotope-filters').each(function (i, buttonGroup) {
                                var $buttonGroup = jQuery(buttonGroup);
                                $buttonGroup.on('click', 'button', function () {
                                    $buttonGroup.find('.active').removeClass('active');
                                    jQuery(this).addClass('active');
                                });
                            });
                        });
                    </script>


                </div>
            </div>
        </div>
    </div>
    <!-- end festivals -->
    <?php
    include('footer.php');
    ?>


</body>
</html>
