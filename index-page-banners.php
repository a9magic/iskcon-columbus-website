<!-- Diwali Dinner -->
<div class="portal item wow fadeInUp" data-wow-delay="0.1s"
     style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp; background-image:url(images/black-wood-bg.jpg)">
    <div class="col-md-6">
        <a href="https://www.eventbrite.com/e/diwali-dinner-and-kartik-damodar-pooja-tickets-36826156024?aff=es2#tickets"
           target="_blank">
            <img style="box-shadow: 0 4px 8px 0 rgba(255,109,0,0.55), 0 6px 20px 0 rgba(255,109,0,0.5);" height="600"
                 src="images/diwali-dinner.jpg"
                 class="attachment-thumbnail size-thumbnail wp-post-image" alt=""
                 srcset="images/diwali-dinner.jpg 180w, images/diwali-dinner.jpg 300w"
                 sizes="(max-width: 300px) 100vw, 150px">
        </a>
    </div>

    <div class="col-md-6 text-center" style="color:white;">
        <h2 style="color:white;font-family:Satisfy;font-size:2em;position:relative">Purchase tickets and <u>more details</u> on <br/></h2>
        <a href="https://www.eventbrite.com/e/diwali-dinner-and-kartik-damodar-pooja-tickets-36826156024?aff=es2#tickets"
           target="_blank">
            <img src="images/eventbrite.jpg" style="width: 25%; height: auto;">
        </a>
        <br/><br/><h2 style="font-family:Satisfy;font-size:2em;position:relative">View Location</h2>
        <div class="eventMap eventMap1"></div>
        <script>
            jQuery(document).ready(function () {
                var teamID = jQuery(".eventMap1");
                if (teamID.length) {
                    function isMobile() {
                        return ('ontouchstart' in document.documentElement);
                    }

                    function init_gmap() {
                        if (typeof google == 'undefined') return;
                        var options = {
                            center: [40.1071128, -83.1409204],
                            zoom: 15,
                            mapTypeControl: true,
                            mapTypeControlOptions: {
                                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                            },
                            navigationControl: true,
                            scrollwheel: false,
                            streetViewControl: true
                        }

                        if (isMobile()) {
                            options.draggable = false;
                        }

                        jQuery('.eventMap1').gmap3({
                            map: {
                                options: options
                            },
                            marker: {
                                latLng: [40.1071128, -83.1409204],
                                options: {icon: ""}
                            }
                        });
                    }

                    init_gmap();
                }

            });

        </script>
        <div>
            <form action="//maps.google.com/maps" method="get" target="_blank">
                <input type="text" placeholder="Enter Your Location" name="saddr"/>
                <input type="hidden" name="daddr" value="5600 Post Road
Dublin, OH 43017"/>
                <input type="submit" class="btn custom-btn"
                       style="color: white; border-radius:20px;"
                       value="Get directions"/>
            </form>
        </div>
    </div>

</div>


<!-- Ekadashi Counter -->
<!--    <div class="full-width hidden-sm hidden-xs hidden-md">-->
<!--        <div class="vc_row-fluid">-->
<!--            <div class="vc_col-sm-12 wpb_column vc_column_container ">-->
<!--                <div class="wpb_wrapper">-->
<!---->
<!---->
<!--                    <div class="wow fadeIn" data-wow-delay="0.4s"-->
<!--                         style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp width: 100%;"><img-->
<!--                                src="images/ekadashi/ekadashi-bg.jpg"-->
<!--                                alt=""-->
<!--                                title="" style="height: 80%; width: 100%; margin-top: 0px; margin-left: -0.5px;">-->
<!--                        <div id="countdown">-->
<!---->
<!--                            <div id='tiles'></div>-->
<!--                            <div class="labels">-->
<!--                                <li>Days</li>-->
<!--                                <li>Hours</li>-->
<!--                                <li>Mins</li>-->
<!--                                <li>Secs</li>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->