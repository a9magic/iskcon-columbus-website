<!-- Microdata markup added by Google Structured Data Markup Helper. -->
<meta charset="UTF-8">
<meta name="google-site-verification" content="1j_IRf5_X88RvfdMLWJ-9ZIGbw5gDh0fJxP3mWZAeaQ"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="Description" content="<?php echo $pageDescription; ?>">
<meta name="Keywords" content="<?php echo $pageKeywords; ?>">
<?php
//-- If canonical URL is specified, include canonical link element
if ($pageCanonical) {
    echo '<link rel="canonical" href="' . $pageCanonical . '">';
}

?>

<link rel="profile" href="https://gmpg.org/xfn/11">
<link rel="pingback" href="#/xmlrpc.php">


<link rel='dns-prefetch' href='//maps.googleapis.com'/>
<link rel='dns-prefetch' href='//s.w.org'/>

<style type="text/css">
    img.wp-smiley,
    img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
    }
</style>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

<link rel='stylesheet' id='contact-form-7-css' href='css/styles.css?ver=4.8' type='text/css' media='all'/>
<link rel='stylesheet' id='jquery.mCustomScrollbar-css' href='css/jquery.mCustomScrollbar.css?ver=4.8' type='text/css'
      media='all'/>
<!--<link rel='stylesheet' id='font-awesome-css' href='css/font-awesome.min.css?ver=4.12.1' type='text/css' media='all'/>-->
<link rel='stylesheet' id='font-awesome-css' href='css/fontawesome.css' type='text/css' media='all'/>
<link rel='stylesheet' id='style-css' href='css/style.css?ver=4.8' type='text/css' media='all'/>

<link rel='stylesheet' id='bootstrap-css' href='css/bootstrap.min.css?ver=4.8' type='text/css' media='all'/>
<!--<link rel='stylesheet' id='fontawesome-css' href='css/font-awesome.min.css?ver=4.8' type='text/css' media='all'/>-->
<link rel='stylesheet' id='theme-css-css' href='css/theme-style.css?ver=4.8' type='text/css' media='all'/>
<link rel='stylesheet' id='peace-style-css' href='css/style1.css?ver=4.8' type='text/css' media='all'/>
<link rel='stylesheet' id='js_composer_front-css' href='css/js_composer.min.css?ver=4.12.1' type='text/css'
      media='all'/>
<!--<link rel='stylesheet' id='ms-main-css' href='css/masterslider.main.css?ver=3.0.4' type='text/css' media='all'/>-->
<link rel='stylesheet' id='ms-custom-css' href='css/custom.css?ver=25.1' type='text/css' media='all'/>
<script type='text/javascript' src='js/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='js/jquery-migrate.min.js?ver=1.4.1'></script>

<script type='text/javascript'
        src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBgpM9ahRFQKHujFCLboaSGbKJJs7DT8y8&#038;ver=4.8'></script>
<link rel='https://api.w.org/' href='#/?rest_route=/'/>
<!--<link rel="EditURI" type="application/rsd+xml" title="RSD" href="#/xmlrpc.php?rsd"/>-->
<!--<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="#/wp-includes/wlwmanifest.xml"/>-->
<meta name="generator" content="WordPress 4.8"/>
<meta name="generator" content="WooCommerce 3.0.8"/>
<link rel="canonical" href="#/"/>
<link rel='shortlink' href='#/'/>
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/fontawesome.css">


<script>var ms_grabbing_curosr = 'images/common/grabbing.cur', ms_grab_curosr = 'images/common/grab.cur';</script>
<meta name="generator" content="MasterSlider 3.0.4 - Responsive Touch Image Slider"/>

<!--===============================================================================================-->
<!--CONTACT-US-FORM-->
<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<link rel="stylesheet" type="text/css" href="contact-us-form/css/util.css">
<link rel="stylesheet" type="text/css" href="contact-us-form/css/main.css">
<!--===============================================================================================-->
<!-- MUSIC PLAYER CSS-->
<link rel="stylesheet" type="text/css" href="css/music-player.scss">

<noscript>
    <style>.woocommerce-product-gallery {
            opacity: 1 !important;
        }</style>
</noscript>
<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]>
<link rel="stylesheet" type="text/css" href="#/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css"
      media="screen"><![endif]--><!--[if IE  8]>
<link rel="stylesheet" type="text/css" href="#/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css"
      media="screen"><![endif]-->
<style>

</style>
<style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1475847905924 {
        padding-top: 80px !important;
        padding-bottom: 80px !important;
        background-color: #f7f7f7 !important;
    }

    .vc_custom_1475847924477 {
        padding-bottom: 100px !important;
        background-color: #f7f7f7 !important;
    }

    .vc_custom_1475847937518 {
        padding-top: 35px !important;
        background-color: #f7f7f7 !important;
    }

    .vc_custom_1475847967102 {
        padding-top: 50px !important;
        background-color: #f7f7f7 !important;
    }

    .vc_custom_1475847994262 {
        padding-top: 55px !important;
        background-color: #ffffff !important;
    }

    .vc_custom_1475848013333 {
        padding-top: 110px !important;
        padding-bottom: 110px !important;
        background-color: #f7f7f7 !important;
    }

    .vc_custom_1475848026271 {
        padding-right: 0px !important;
        padding-left: 0px !important;
    }

    .vc_custom_1441954675195 {
        padding-bottom: 55px !important;
    }

    .vc_custom_1441955531097 {
        padding-bottom: 55px !important;
    }</style>
<noscript>
    <style type="text/css"> .wpb_animate_when_almost_visible {
            opacity: 1;
        }</style>
</noscript>

<!-- JSON-LD markup generated by Google Structured Data Markup Helper. -->
<script type="application/ld+json">
[ {
  "@context" : "http://schema.org",
  "@type" : "LocalBusiness",
  "name" : "Home",
  "image" : "https://www.iskconcolumbus.com/images/Background-1.jpg",
  "telephone" : "+1(614)421-1661",
  "email" : "iskconcolumbus@gmail.com",
  "address" : {
    "@type" : "PostalAddress",
    "streetAddress" : "379 West 8th Ave",
    "addressLocality" : "Columbus",
    "addressRegion" : "OH",
    "postalCode" : "43201"
  }
}, {
  "@context" : "http://schema.org",
  "@type" : "LocalBusiness",
  "name" : "Events",
  "image" : "https://www.iskconcolumbus.com/images/Background-1.jpg",
  "telephone" : "+1(614)421-1661",
  "email" : "iskconcolumbus@gmail.com",
  "address" : {
    "@type" : "PostalAddress",
    "streetAddress" : "379 West 8th Ave",
    "addressLocality" : "Columbus",
    "addressRegion" : "OH",
    "postalCode" : "43201"
  }
}, {
  "@context" : "http://schema.org",
  "@type" : "LocalBusiness",
  "name" : "Services",
  "image" : "https://www.iskconcolumbus.com/images/Background-1.jpg",
  "telephone" : "+1(614)421-1661",
  "email" : "iskconcolumbus@gmail.com",
  "address" : {
    "@type" : "PostalAddress",
    "streetAddress" : "379 West 8th Ave",
    "addressLocality" : "Columbus",
    "addressRegion" : "OH",
    "postalCode" : "43201"
  }
}, {
  "@context" : "http://schema.org",
  "@type" : "LocalBusiness",
  "name" : "Donate",
  "image" : "https://www.iskconcolumbus.com/images/Background-1.jpg",
  "telephone" : "+1(614)421-1661",
  "email" : "iskconcolumbus@gmail.com",
  "address" : {
    "@type" : "PostalAddress",
    "streetAddress" : "379 West 8th Ave",
    "addressLocality" : "Columbus",
    "addressRegion" : "OH",
    "postalCode" : "43201"
  }
} ]



</script>

</head>
