<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <title>One Time Donation
    </title>
    <?php
    include('meta_links.php');
    ?>
</head>
<body class="product-template-default single single-product postid-247 _masterslider _msp_version_3.0.4 woocommerce woocommerce-page wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- /#preloader -->
<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php include('header.php'); ?>
    <section id="blog-heading" style="height:auto;">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text pull-left" style="width:99%;padding-top:25px;">
                    <h1 class="blog-title" style="text-align:center;font-size:31px;">One Time Donation
                    </h1>
                </div>
                <!-- /.heading-text -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.heading-fullwidth -->
    </section>

    <section id="shop" style="padding:0 !important;">

        <div class="container">
            <!-- festival -->
            <div class="full-width">
                <div class="vc_row-fluid">
                    <div class="vc_col-sm-12 wpb_column vc_column_container ">
                        <div class="wpb_wrapper">

                            <h2 class="section-title wow fadeInUp" style="padding: 15px 0 15px 0;"
                                data-wow-delay="0.4s"><i
                                        class="fa fa-cogs fa-2x"></i> How to make a One Time Donation </h2>
                            <!-- /.section-title -->


                            <div class="col-md-8 col-sm-8 hidden-xs">
                                <?php include "donate_options.html"; ?>
                            </div>

                            <div class="col-md-4 col-sm-4 text-center">
                                <h1 class="wow fadeInUp" data-wow-delay="0.4s"><u>Use
                                        Credit/Debit Card</u></h1>


                                <div class="col-md-12 col-sm-12 text-center">
                                    <div class="vc_box_shadow_border wow fadeInLeft text-center"
                                         data-wow-delay="0.6s"
                                         style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post"
                                              target="_blank">
                                            <input type="hidden" name="cmd" value="_s-xclick">
                                            <input type="hidden" name="hosted_button_id" value="K94FLQYGZG3W2">
                                            <input type="image"
                                                   src="https://www.paypalobjects.com/WEBSCR-640-20110429-1/en_US/i/btn/btn_donateCC_LG.gif"
                                                   border="0" name="submit"
                                                   alt="PayPal - The safer, easier way to pay online!">
                                            <img alt="" border="0"
                                                 src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif"
                                                 width="1" height="1">
                                        </form>
                                    </div>
                                </div>
                                <br/>
                                <hr>
                                <br/>
                                <div class="vc_box_shadow_border wow fadeInRight text-center" data-wow-delay="0.6s"
                                     style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                    <h1 style="padding: 10px;"><u>ACH Debit Form</u></h1>
                                    <?php include('ach-form.php') ?>
                                </div>
                            </div>


                            <div class="coll-md-12 col-sm-12 text-center wow fadeInUp" data-wow-delay="0.6s"
                                 style="padding: 30px;">
                                If setting up on your own: Payee name: ISKCON Columbus, Address: 379 West 8th Ave
                                Columbus, OH 43201. If you have any questions, contact <strong>Ram Tirtha
                                    Das </strong> at 614-404-8570 or stop by in the temple.
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- end festivals -->
        </div>

    </section>

    <h6 class="wow fadeInUp text-center" data-wow-delay="0.4s">
        We are a 501 (C) (3) non-profit charity organization. Our IRS tax id is <strong>34-1225-440</strong> and all
        donations
        made to 'ISKCON Columbus' are tax deductible.
    </h6>

</div>
<?php
include('footer.php');
?>
</body>
</html>
