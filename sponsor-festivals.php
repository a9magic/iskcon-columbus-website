<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="//gmpg.org/xfn/11">
    <title>Sponsor Festivals
    </title>
    <?php
    include('meta_links.php');
    ?>
</head>
<body class="product-template-default single single-product postid-247 _masterslider _msp_version_3.0.4 woocommerce woocommerce-page wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- /#preloader -->
<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php include('header.php'); ?>
    <section id="blog-heading" style="height:auto;">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text pull-left" style="width:99%;padding-top:25px;">
                    <h1 class="blog-title" style="text-align:center;font-size:31px;">Sponsor Festivals
                    </h1>
                </div>
                <!-- /.heading-text -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.heading-fullwidth -->
    </section>
    <script type="text/javascript">
        $(document).ready(function () {

        });

        $('#festival').click(function () {
            $('html,body').animate({'scrollTop': $('#festival-schedule').position().top}, 500);
        });

    </script>
    <section id="shop" style="padding:0 !important;">
        <h3 id="festival" class="text-center" style="padding-top:15px;">
            <a class="festival" href="#festival-schedule"> <u>See Festivals in 2017</u> </a></h3>

        <div class="vc_row-fluid">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">
                    <section class="shedule section-padding" style="background: url(images/Hindu_BG_03.jpg);">
                        <div class="container">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="wow fadeInLeft" data-wow-delay="0.6s">
                                        <img width="100%" height="100%" src="images/sponsor-festivals-header.jpg"
                                             class="attachment-shop_single size-shop_single wp-post-image"
                                             alt="Radha Natabara" title="Radha Natabara"/>
                                        <div class="thumbnails columns-3">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 wow fadeInRight" data-wow-delay="0.6s">
                                    <h2 class="section-title">Krishna Conscious Festivals</h2><br/>
                                    <h3><i class="fa fa-first-order"></i> Join us with your friends and family
                                    </h3>
                                    <div class="section-detail" style="color: white">
                                        <br/><br/>This worship program, as established by Srila Prabhupada over 35 years
                                        ago, includes the five primary devotional processes recommended by Sri Krishna
                                        Chaitanya Mahaprabhu.<br/>
                                        <br/>
                                        “One should associate with devotees, chant the holy name of the Lord, hear
                                        Śrīmad-Bhāgavatam, reside at Mathura and worship the Deity with faith and
                                        veneration."
                                        <strong>(Caitanya Caritamrita. Madhya-lila 22.128)</strong>
                                    </div>
                                </div>


                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>

                </div>
            </div>
        </div>

        <div class="container">
            <!-- festival -->
            <div class="full-width">
                <div class="vc_row-fluid">
                    <div class="vc_col-sm-12 wpb_column vc_column_container ">
                        <div class="wpb_wrapper">

                            <h2 class="section-title wow fadeInUp" style="padding: 15px 0 15px 0;"
                                data-wow-delay="0.4s"><i
                                        class="fa fa-cogs fa-2x"></i> How to Sponsor a Festival </h2>
                            <!-- /.section-title -->

                            <div class="sermons">

                                <div class="col-md-6 col-sm-6 text-center">
                                    <h1 class="wow fadeInUp" data-wow-delay="0.4s" style="padding: 10px;"><u>Use
                                            Credit/Debit</u></h1>

                                    <div class="col-md-12 col-sm-12 text-center">
                                        <div class="vc_box_shadow_border wow fadeInLeft text-center"
                                             data-wow-delay="0.6s"
                                             style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                            <h2 style="padding: 10px;">Sponsor festivals</h2>
                                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post"
                                                  target="_blank">
                                                <input type="hidden" name="cmd" value="_s-xclick">
                                                <input type="hidden" name="hosted_button_id" value="K94FLQYGZG3W2">
                                                <input type="image"
                                                       src="https://www.paypalobjects.com/WEBSCR-640-20110429-1/en_US/i/btn/btn_donateCC_LG.gif"
                                                       border="0" name="submit"
                                                       alt="PayPal - The safer, easier way to pay online!">
                                                <img alt="" border="0"
                                                     src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif"
                                                     width="1" height="1">
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="vc_box_shadow_border wow fadeInRight text-center" data-wow-delay="0.6s"
                                         style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                        <h1 style="padding: 10px;"><u>ACH Debit Form</u></h1>
                                        <?php include('ach-form.php') ?>
                                    </div>
                                </div>


                                <div class="coll-md-12 col-sm-12 text-center wow fadeInUp" data-wow-delay="0.6s"
                                     style="padding: 30px;">
                                    If setting up on your own: Payee name: ISKCON Columbus, Address: 379 West 8th Ave
                                    Columbus, OH 43201. If you have any questions, contact <strong>Ram Tirtha
                                        Das </strong> at 614-404-8570 or stop by in the temple.
                                </div>

                            </div>

                            <div class="col-md-12 wow fadeInUp" data-wow-delay="0.4s" style="padding: 30px;">
                                <strong>“One should distribute viṣṇu-prasāda to everyone. Know that Lord Viṣṇu (Krishna)
                                    is very pleased when everyone is sumptuously fed with viṣṇu-prasāda”. – Śrīmad-Bhāgavatam 8.16.56</strong>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end festivals -->
        </div>

        <div id="festival-schedule"><?php include('festival-schedule.php') ?></div>


        <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span>
        </div>

        <h6 class="wow fadeInUp text-center" data-wow-delay="0.4s">
            We are a 501 (C) (3) non-profit charity organization. Our IRS tax id is <strong>34-1225-440</strong> and all
            donations
            made to 'ISKCON Columbus' are tax deductible.
        </h6>
        <?php
        include('footer.php');
        ?>
</body>
</html>
