<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>subscribe</title>
    <?php
    include("meta_links.php");
    ?>
</head>
<body class="page-template page-template-page-template page-template-event-page page-template-page-templateevent-page-php page page-id-402 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php
    include("header.php");
    ?>
<br/>
    <br/>
    <?php
    include("subscribe.html");
    ?>

    <?php
    include("footer.php");
    ?>
</div>
<!-- /.peace layout end -->

</body>
</html>
