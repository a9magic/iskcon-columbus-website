<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <title>Harinaam Sankirtan</title>
    <?php
    include('meta_links.php');
    ?>
</head>

<body class="events-template-default single single-events postid-74 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">

    <?php
    include('header.php');
    ?>
    <!-- Blog Heading -->
    <section id="blog-heading">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text">
                    <h1 class="blog-title text-center">Harinaam Sankirtan</h1>
                </div><!-- /.heading-text -->
            </div><!-- /.container -->
        </div><!-- /.heading-fullwidth -->
    </section><!-- /#blog-heading -->
    <!-- Blog Heading end -->

    <!-- header image -->
    <div class="full-width">
        <div class="vc_row-fluid">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">


                    <div class="wow fadeIn" data-wow-delay="0.4s"
                         style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp width: 100%;"><img
                                src="images/harinaam-header.jpg"
                                alt=""
                                title="" style="height: 80%; width: 100%; margin-top: 0px; margin-left: -0.5px;">
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Blog Page Container -->
    <section id="blog-page-container">
        <div class="blog-page-wrapper">
            <div class="container">
                <div class="row">


                    <div class="col-md-8 wow fadeInLeft" data-wow-delay="0.4s">
                        <div class="all-event">
                            <div class="event-post">
                                <div class="events-date">
                                    <span class="event-date">30</span> <br>
                                    <span class="event-month">June</span>
                                </div>

                                <h2 class="uppercase"><a
                                            href=""
                                            title="ISKCON Columbus - Harinaam Sankirtan"> Harinaam Sankirtan </a></h2>
                                <div class="event-period">
									<span class="period-session">
										<i class="fa fa-clock-o"></i> 2 Hours <span class="separator"></span> <i
                                                class="fa fa-map-marker"></i> 379 West 8th Ave Columbus, OH									</span><span
                                            class="separator"></span>
                                    <?php include('socialmedia-share.php'); ?><!-- /.comments-share -->
                                </div><!-- /.event-period -->
                                <div class="feature-img">
                                    <img width="800" height="550"
                                         src="images/harinaam-2.jpg"
                                         class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                         srcset="images/harinaam-2.jpg 800w, images/harinaam-2.jpg 300w"
                                         sizes="(max-width: 800px) 100vw, 800px"/></div>
                                <div class="event-description">
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-music base-color"></i></div>
                                        <div class="media-body">Lively kirtan (call-and-response chanting)<br></div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-heart base-color"></i></div>
                                        <div class="media-body">Mantra meditation<br></div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-newspaper-o base-color"></i></div>
                                        <div class="media-body">Dancing<br></div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-cutlery base-color"></i></div>
                                        <div class="media-body">Ecstatic Experience<br></div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="wow fadeInUp" data-wow-delay="0.4s">Harinaam, short for Harinaam-sankirtana
                                (the congregational chanting of the holy names of the Lord) may be most familiar from
                                seeing the devotees out in the streets, dancing and chanting the maha-mantra <span
                                        style="color: orangered;">Hare
                                Krishna, Hare Krishna, Krishna Krishna, Hare Hare/ Hare Rama, Hare Rama, Rama Rama, Hare
                                    Hare</span> accompanied by mridangas (two headed drums) and karatalas (hand
                                cymbals).

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 wow fadeInRight" data-wow-delay="0.4s">
                        <div class="event-location">
                            <h2 class="widget-title">View Location</h2>
                            <div class="eventMap">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3056.732827642373!2d-83.01912858510767!3d39.99207557941673!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88388eea2e220463%3A0x4f95e3ec9b88a211!2sISKCON+Temple!5e0!3m2!1sen!2sus!4v1494100183136"
                                        width="100%" height="100%" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>
                            </div>


                        </div>

                        <div class="event-detail">
                            <h2 class="widget-title">Event Details</h2>
                            <div class="address">
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-map-marker base-color"></i></div>
                                    <div class="media-body"><strong>Where:</strong> 379 West 8th Ave Columbus, OH
                                        <br></div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-calendar-o base-color"></i></div>
                                    <div class="media-body"><strong>When:</strong> Every Friday at <strong>6:30 PM -
                                            8 PM</strong><br></div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-clock-o base-color"></i></div>
                                    <div class="media-body"><strong>Who:</strong> Everyone is invited to attend<br>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-money base-color"></i></div>
                                    <div class="media-body"><strong>Cost:</strong> FREE <br>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-user base-color"></i></div>
                                    <div class="media-body"><strong>What to wear:</strong> Casual<br></div>
                                </div>
                            </div>
                            <div>
                                <form action="https://maps.google.com/maps" method="get" target="_blank">
                                    <input type="text" placeholder="Enter Your Location" name="saddr"/>
                                    <input type="hidden" name="daddr" value="379 West 8th Ave Columbus"/>
                                    <input type="submit" class="btn custom-btn"
                                           style="color: white; border-radius:20px;" value="Get directions"/>
                                </form>
                            </div>
                        </div>
                    </div>

                </div><!-- row -->
            </div> <!-- #container -->
        </div><!-- #blog-page-wrapper -->
    </section>

    <?php
    include('footer.php');
    ?>


</body>
</html>