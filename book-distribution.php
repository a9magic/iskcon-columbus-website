<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <title>Book Distribution</title>
    <?php
    $pageDescription = 'ISKCON Columbus congregation performs book distribution of vedic literature on weekends, special
                                occasions and during festivals by
                                going out on streets, door to door and at different public places.';
    $pageKeywords = 'ISKCON Columbus,  Columbus Krishna House, Columbus temple, Hindu Temple in Columbus, Radha Krishna, ISKCON Ohio, Radha Natabara, book distribution';
    $pageCanonical = 'https://www.iskconcolumbus.com/book-distribution.php';

    include('meta_links.php');
    ?>
</head>

<body class="events-template-default single single-events postid-74 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">

    <?php
    include('header.php');
    ?>
    <!-- Blog Heading -->
    <section id="blog-heading">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text">
                    <h1 class="blog-title text-center">Book Distribution</h1>
                </div><!-- /.heading-text -->
            </div><!-- /.container -->
        </div><!-- /.heading-fullwidth -->
    </section><!-- /#blog-heading -->
    <!-- Blog Heading end -->

    <!-- header image -->
    <div class="full-width">
        <div class="vc_row-fluid">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">


                    <div class="wow fadeIn" data-wow-delay="0.4s"
                         style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp width: 100%;"><img
                                src="images/book-distribution-header.jpg"
                                alt=""
                                title="" style="height: 80%; width: 100%; margin-top: 0px; margin-left: -0.5px;">
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Blog Page Container -->
    <section id="blog-page-container">
        <div class="blog-page-wrapper">
            <div class="container">
                <div class="row">


                    <div class="col-md-8 wow fadeInLeft" data-wow-delay="0.4s">
                        <div class="all-event">
                            <section id="blog-heading">
                                <div class="heading-section">
                                    <div class="container">
                                        <div class="heading-text">
                                            <h1 class="blog-title text-center">Book Distribution</h1>
                                        </div><!-- /.heading-text -->
                                    </div><!-- /.container -->
                                </div><!-- /.heading-fullwidth -->
                            </section><!-- /#blog-heading -->
                            <br/>
                            <div class="wow fadeInUp" data-wow-delay="0.4s"><strong>ISKCON Columbus</strong>
                                congregation performs book distribution of vedic literature on weekends, special
                                occasions and during festivals by
                                going out on streets, door to door and at different public places. The books such as
                                Bhagavad-Gita and Śrīmad-Bhāgavatam are translated by ISKCON Founder Acarya Srila A.C
                                Bhakti Vendanta Swami Srila Prabhupada.

                            </div>
                            <div class="coll-md-12 col-sm-12 text-center wow fadeInUp" data-wow-delay="0.6s"
                                 style="padding: 40px;">
                                If you are interested to buy the books or want to join the Book distribution Teams,
                                please
                                contact Prema Sindhu das premasindhu@gmail.com or Prem Vilas das gulla007@hotmail.com
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 wow fadeInLeft" data-wow-delay="0.4s">
                        <div class="all-event">
                            <h3>Sponsor Book Distribution</h3> <a href="sponsor-book-distribution.php">here</a>
                        </div>
                    </div>

                </div><!-- row -->
            </div> <!-- #container -->
        </div><!-- #blog-page-wrapper -->
    </section>

    <?php
    include('footer.php');
    ?>


</body>
</html>