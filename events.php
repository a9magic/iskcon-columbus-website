<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <title>Events
    </title>
    <?php
    include('meta_links.php');
    ?>

    <style type="text/css">
        article {
            float: left;
            width: 23%;
            background: #ccc;
            margin: 10px 1%;
            padding: 1%;
        }

        @media all and (max-width: 900px) {
            article {
                width: 48%
            }
    </style>
    <script type="text/javascript">
        /* Thanks to CSS Tricks for pointing out this bit of jQuery
https://css-tricks.com/equal-height-blocks-in-rows/
It's been modified into a function called at page load and then each time the page is resized. One large modification was to remove the set height before each new calculation. */

        equalheight = function(container){

            var currentTallest = 0,
                currentRowStart = 0,
                rowDivs = new Array(),
                $el,
                topPosition = 0;
            $(container).each(function() {

                $el = $(this);
                $($el).height('auto')
                topPostion = $el.position().top;

                if (currentRowStart != topPostion) {
                    for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                        rowDivs[currentDiv].height(currentTallest);
                    }
                    rowDivs.length = 0; // empty the array
                    currentRowStart = topPostion;
                    currentTallest = $el.height();
                    rowDivs.push($el);
                } else {
                    rowDivs.push($el);
                    currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
                }
                for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
            });
        }

        $(window).load(function() {
            equalheight('.main-content article');
        });


        $(window).resize(function(){
            equalheight('.main-content article');
        });

    </script>

</head>
<body class="page-template page-template-page-template page-template-event-page page-template-page-templateevent-page-php page page-id-399 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- /#preloader -->
<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php
    include('header.php');
    ?>
    <!-- Blog Heading -->
    <section id="blog-heading" style="height:auto;border-bottom:2px solid #e1e1e1;">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text pull-left" style="width:99%;padding-top:41px;">
                    <h1 class="blog-title" style="text-align:center;font-size:31px;">Events
                    </h1>
                    <div class="event-description col-6 text-center" style="padding:11px;">
                        Join our “Think Out Loud” sessions as well as a few getaways at amazing spiritual
                        retreat in Columbus, OH. Located next to OSU. A chance for you to unwind and relax.
                        This society is about you. Krishna Consciousness society, where spirituality becomes practical.
                    </div>
                </div>
                <!-- /.heading-text -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.heading-fullwidth -->
    </section>
    <!-- /#blog-heading -->
    <!-- Blog Heading end -->
    <section id="blog-page-container">
        <div class="blog-page-wrapper">
            <div id="primary" class="container">
                <main id="main" class="main-content" role="main">

                    <!-- Ekadashis 2020-->
                   <div class="blog-content ">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <h2 class="uppercase">
                                        <a href="ekadashi.php"
                                           title="Ekadashi 2020">Ekadashis in 2020
                                        </a>
                                    </h2>
                                    <div class="event-period">

                                    </div>
                                    <!-- /.event-period -->
                                    <a href="ekadashi.php">
                                        <div class="feature-img">
                                            <img width="570" height="340"
                                                 src="images/vishnu.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""/>
                                        </div>
                                        <div class="event-description">
                                            Ekadashi fasting is observed on every 11th Tithi in Hindu calendar.
                                            Devotees of Lord observe Ekadashi fasting to seek His blessings.
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Janmashtami 2019-->
<!--                    <div class="blog-content ">-->
<!--                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">-->
<!--                            <div class="all-event">-->
<!--                                <div class="event-post">-->
<!--                                    <h2 class="uppercase">-->
<!--                                        <a href="janmashtami.php"-->
<!--                                           title="Janmashtami 2019">Janmashtami 2019-->
<!--                                        </a>-->
<!--                                    </h2>-->
<!--                                    <div class="event-period">-->
<!---->
<!--                                    </div>-->
                                    <!-- /.event-period -->
<!--                                    <a href="janmashtami.php">-->
<!--                                        <div class="feature-img">-->
<!--                                            <img width="570" height="341"-->
<!--                                                 src="images/festivals.jpg"-->
<!--                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"-->
<!--                                                 alt=""-->
<!--                                                 srcset="images/festivals.jpg 570w, images/festivals.jpg 300w"-->
<!--                                                 sizes="(max-width: 570px) 100vw, 570px"/>-->
<!--                                        </div>-->
<!--                                        <div class="event-description">-->
<!--                                            It is that time of year again when the ringing bells of ISKCON Columbus-->
<!--                                            announce the divine birthday celebration of Lord Krishna. Follow the sound-->
<!--                                            to the grandest 2019 Sri Krishna Janmashtami festival!-->
<!--                                        </div>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        </div>-->

                    <!-- Sunday Feast -->
                    <div class="blog-content ">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">18
                        </span>
                                        <br>
                                        <span class="event-month">June
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="sunday-feast.php"
                                           title="Sunday Feast">Sunday Feast
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-clock-o">
                          </i> 3 Hours
                          <span class="separator">
                          </span>
                          <i class="fa fa-map-marker">
                          </i> 379 West 8th Ave Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="sunday-feast.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/sunday-feast.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/sunday-feast.jpg 570w, images/sunday-feast.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                        <div class="event-description">
                                            It's a festival every Sunday!! Kirtan, seminar, drama, children's program,
                                            vegetarian feast and a lot of dancing and chanting...Join us for Krishna
                                            Fest!
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        </div>

                    <!-- Thursday Program -->
                    <div class="blog-content ">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">29
                        </span>
                                        <br>
                                        <span class="event-month">June
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="thursday-feast.php"
                                           title="Thursday Programs">Thursday Programs
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-clock-o">
                          </i> 2 Hours
                          <span class="separator">
                          </span>
                          <i class="fa fa-map-marker">
                          </i> 379 West 8th Ave Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="thursday-feast.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/thursday-program.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/thursday-program.jpg 570w, images/thursday-program.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                        <div class="event-description">
                                            Thurday Feast is a weekly gathering at the temple to discuss Vedic
                                            philosophy, engage in kirtans followed by sumptuous Prasadam (vegetarian
                                            Dinner)
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Friday Night Harinaam -->
                    <div class="blog-content ">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">30
                        </span>
                                        <br>
                                        <span class="event-month">June
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="harinaam.php"
                                           title="Friday Night Harinaam">Friday Night Harinaam
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-clock-o">
                          </i> 1.5 Hours
                          <span class="separator">
                          </span>
                          <i class="fa fa-map-marker">
                          </i> Downton, Columbus
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="harinaam.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/harinaam.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/harinaam.jpg 570w, images/harinaam.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                        <div class="event-description">
                                            Join us for harinaam on every <strong>Friday</strong>, in
                                            Downtown, Columbus. We will meet at the ISKCON Columbus Temple at 6:30 p.m.
                                            We will chant and dance until 8:30 p.m.
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Home Satsang -->
                    <div class="blog-content ">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">June
                        </span>
                                        <br>
                                        <span class="event-month">
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="home_satsang.php"
                                           title="Home Satsang">Home Gathering / Satsang
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-clock-o">
                          </i> 2 Hours
                          <span class="separator">
                          </span>
                          <i class="fa fa-map-marker">
                          </i> Across Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="home_satsang.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/home-programs.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/home-programs.jpg 570w, images/home-programs.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                        <div class="event-description">
                                            Families in our congregation invites you, your family and your friends to
                                            participate in their respective programs, discuss our scriptures, and enjoy
                                            the delicious prasadam
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Maha Satsang -->
                    <div class="blog-content ">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">8
                        </span>
                                        <br>
                                        <span class="event-month">July
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="maha-satsang.php"
                                           title="Maha Satsang">Maha Satsang
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-clock-o">
                          </i> 2 Hours
                          <span class="separator">
                          </span>
                          <i class="fa fa-map-marker">
                          </i> 3520 Walker Rd, Hilliard, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="maha-satsang.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/maha_satsang_programe.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/maha-satsang.jpg 570w, images/maha-satsang.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                        <div class="event-description">
                                            Monthly Gathering program starts about 5 pm, a discourse by leaders of our
                                            temple,1 round of japa meditation followed by a sumptuous feast for all to
                                            delight in.
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Columbus Bhakti Yoga -->
                    <div class="blog-content ">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">14
                        </span>
                                        <br>
                                        <span class="event-month">July
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="/columbusbhaktiyoga.html" target="_blank"
                                           title="Columbus Bhakti Yoga">Columbus Bhakti Yoga
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-clock-o">
                          </i> 1.5 Hours
                          <span class="separator">
                          </span>
                          <i class="fa fa-map-marker">
                          </i> 5985 Cara Road, Dublin, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="/columbusbhaktiyoga.html" target="_blank">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/bhakti-yoga.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/bhakti-yoga.jpg 570w, images/bhakti-yoga.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                        <div class="event-description">
                                            Practicing the Yoga of devotion through Asanas, Pranayama (by Prof. Yoga
                                            teacher) and Kirtan (Musical meditation)
                                            Contact Person: Lila Manjari Devi Dasi (727-804-9836) Email:
                                            spiritualyoga108@gmail.com

                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- All Festivals -->
                    <div class="blog-content ">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <h2 class="uppercase">
                                        <a href="festivals.php" target="_blank"
                                           title="All Festivals">Festivals in 2020
                                        </a>
                                    </h2>

                                    <!-- /.event-period -->
                                    <a href="festivals.php" target="_blank">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/festivals-thumbnail.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/festivals-thumbnail.jpg 570w, images/festivals-thumbnail.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                        <div class="event-description">
                                            With myriad of colourful decorations, multi-course feasts, elaborate
                                            rituals, kirtans, dance and dramas, the festivals makes spiritual progress a
                                            fun-filled experience

                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
    </section>

    <?php
    include('footer.php');
    ?>

</div>
<!-- /.peace layout end -->

</body>
</html>

