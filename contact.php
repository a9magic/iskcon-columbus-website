<!DOCTYPE html>
<html lang="en-US">
<?php include "includes/sendemail.php"; ?>
<head>
    <title>Contact Us</title>
    <?php
    $pageDescription = 'ISKCON Columbus look forward to hear from you and serve you!';
    $pageKeywords = 'ISKCON Columbus,  Columbus Krishna House, Columbus temple, Hindu Temple in Columbus, Radha Krishna, ISKCON Ohio, Radha Natabara, contact';
    $pageCanonical = 'https://www.iskconcolumbus.com/contact.php';

    include("meta_links.php");
    ?>

<body class="page-template page-template-page-template page-template-front-page page-template-page-templatefront-page-php page page-id-384 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">

    <?php


    include("header.php");
    ?>

    <div class="vc_row-fluid vc_custom_1475847937518">
        <div class="container">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">

                    <h2 class="section-title wow fadeInUp" data-wow-delay="0.4s"
                        style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;"><i
                                class="fa fa-volume-control-phone fa-2x"></i>CONTACT
                    </h2><!-- /.section-title -->
                    <div class="section-detail wow fadeInUp" data-wow-delay="0.6s"
                         style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">lets get
                        connected / we would love to hear from you
                    </div><!-- /.section-detail -->
                    <h3 class="text-center wow fadeInUp" data-wow-delay="0.6s">Temple Council</h3>
                    <div class="sermons">

                        <div class="col-md-6 col-sm-6">
                            <div class="portal-contact wow fadeInLeft" data-wow-delay="0.6s"
                                 style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                <div class="">
                                    <img width="150" height="150"
                                         src="images/premvilas-thumb.jpg">
                                </div>
                                <div class="">
                                    <div class="content"><a>Prem Vilas Das</a></div>
                                    <div class="post-date fa fa-phone">
                                        614-946-5568
                                    </div>
                                    <div class="post-date fa fa-envelope">
                                        gulla007@hotmail.com
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="portal-contact wow fadeInUp" data-wow-delay="0.6s"
                                 style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                <img width="150" height="150"
                                     src="images/naveen.png"
                                     class="attachment-thumbnail size-thumbnail wp-post-image" alt=""
                                     srcset="images/naveen.png 150w, images/naveen.png 180w"
                                     sizes="(max-width: 150px) 100vw, 150px">
                                <div class="content"><a>Naveen Krishna das</a></div>
                                <div class="post-date fa fa-phone">
                                    614-316-8936
                                </div>
                                <div class="post-date fa fa-envelope">
                                    rns.naveenkrishnadas@gmail.com
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="portal-contact wow fadeInUp" data-wow-delay="0.6s"
                                 style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                <img width="150" height="150"
                                     src="images/ram-tirtha.jpg"
                                     class="attachment-thumbnail size-thumbnail wp-post-image" alt=""
                                     srcset="images/ram-tirtha.jpg 150w, images/ram-tirtha.jpg 180w"
                                     sizes="(max-width: 150px) 100vw, 150px">
                                <div class="content"><a>Rama Tirtha Das</a></div>
                                <div class="post-date fa fa-phone">
                                    614-404-8570
                                </div>
                                <div class="post-date fa fa-envelope">
                                    ramatirtha.rns@gmail.com
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="portal-contact wow fadeInRight" data-wow-delay="0.6s"
                                 style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                <img width="150" height="150"
                                     src="images/premasindhu.jpg"
                                     class="attachment-thumbnail size-thumbnail wp-post-image" alt=""
                                     srcset="images/premasindhu.jpg 150w, images/premasindhu.jpg 180w"
                                     sizes="(max-width: 150px) 100vw, 150px">
                                <div class="content"><a>Prema Sindhu Das</a></div>
                                <div class="post-date fa fa-phone">
                                    727-804-9835
                                </div>
                                <div class="post-date fa fa-envelope">
                                    premasindhu@gmail.com
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="vc_row-fluid vc_custom_1441963525429">

        <div class="container">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">
                    <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span>
                    </div>
                    <h2 class="section-title wow fadeInUp" data-wow-delay="0.4s">Send Us a Message</h2>
                    <!-- /.section-title -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.section-detail -->
    <!--                    <div role="form" class="wpcf7" id="wpcf7-f7-p384-o1" lang="en-US" dir="ltr">-->
    <!--                        <div class="screen-reader-response"></div>-->
    <!--                        <form  method="post" class="wpcf7-form"-->
    <!--                              >-->
    <!--                            <div style="display: none;">-->
    <!--                                <input type="hidden" name="_wpcf7" value="7"/>-->
    <!--                                <input type="hidden" name="_wpcf7_version" value="4.8"/>-->
    <!--                                <input type="hidden" name="_wpcf7_locale" value="en_US"/>-->
    <!--                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f7-p384-o1"/>-->
    <!--                                <input type="hidden" name="_wpcf7_container_post" value="384"/>-->
    <!--                                <input type="hidden" name="_wpcf7_nonce" value="bfbea92b91"/>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-6 form-group"><label class="sr-only">Full Name</label><span-->
    <!--                                        class="wpcf7-form-control-wrap your-name"><input type="text" name="name"-->
    <!--                                                                                          size="40"-->
    <!--                                                                                         class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control"-->
    <!--                                                                                         aria-required="true"-->
    <!--                                                                                         aria-invalid="false"-->
    <!--                                                                                         placeholder="Full Name"/></span>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-6 form-group"><label class="sr-only">Email Address</label><span-->
    <!--                                        class="wpcf7-form-control-wrap your-email"><input type="email" name="email"-->
    <!--                                                                                           size="40"-->
    <!--                                                                                          class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control"-->
    <!--                                                                                          aria-required="true"-->
    <!--                                                                                          aria-invalid="false"-->
    <!--                                                                                          placeholder="Email Address"/></span>-->
    <!--                            </div>-->
    <!--							 <div class="col-md-6 form-group"><label class="sr-only">Phone</label><span-->
    <!--                                        class="wpcf7-form-control-wrap your-email"><input type="phone" name="phone"-->
    <!--                                                                                           size="40"-->
    <!--                                                                                          class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control"-->
    <!--                                                                                          aria-required="true"-->
    <!--                                                                                          aria-invalid="false"-->
    <!--                                                                                          placeholder="Phone"/></span>-->
    <!--                            </div>-->
    <!--                            <div class="form-group col-md-12"><label class="sr-only">Subject</label><span-->
    <!--                                        class="wpcf7-form-control-wrap your-subject"><input type="text"-->
    <!--                                                                                            name="subject"-->
    <!--                                                                                            size="40"-->
    <!--                                                                                            class="wpcf7-form-control wpcf7-text form-control"-->
    <!--                                                                                            aria-invalid="false"-->
    <!--                                                                                            placeholder="Subject"/></span>-->
    <!--                            </div>-->
    <!--                            <div class="form-group col-md-12">-->
    <!--                                <span class="wpcf7-form-control-wrap your-message"><textarea name="message"-->
    <!--                                                                                             cols="40" rows="5"-->
    <!--                                                                                             class="wpcf7-form-control wpcf7-textarea form-control input-md"-->
    <!--                                                                                             aria-invalid="false"-->
    <!--                                                                                             placeholder="YOUR MESSAGE"></textarea></span>-->
    <!--                            </div>-->
    <!--                            <p class="event-btn-container col-md-12">-->
    <!--                                <input type="submit" value="SEND NOW" name="sub"-->
    <!--                                       class="wpcf7-form-control wpcf7-submit btn custom-btn"/>-->
    <!--                            </p>-->
    <!--                            <div class="wpcf7-response-output wpcf7-display-none"></div>-->
    <!--                        </form>-->
    <!--                    </div>-->

    <div class="container-contact100">
        <div class="contact100-map" id="google_map" data-map-x="39.9919483" data-map-y="-83.0169319"
             data-pin="images/icons/map-marker.png" data-scrollwhell="0" data-draggable="1"></div>

        <div class="wrap-contact100">
            <div class="section-detail wow fadeInUp" data-wow-delay="0.6s">We’d love to hear your thoughts or
                questions. Write to us at the form below.
            </div>
            <form class="contact100-form validate-form">
                <div class="wrap-input100 validate-input" data-validate="Please enter your name">
                    <input class="input100" type="text" name="name" placeholder="Full Name">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input"
                     data-validate="Please enter email: user@domain.com">
                    <input class="input100" type="text" name="email" placeholder="Email">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input"
                     data-validate="Please enter phone number: XXX-XXX-XXXX">
                    <input class="input100" type="number" name="phone" placeholder="Phone">
                    <span class="focus-input100"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Please enter your message">
                    <textarea class="input100" name="message" placeholder="Your Message"></textarea>
                    <span class="focus-input100"></span>
                </div>

                <div class="g-recaptcha" data-sitekey="6LcuG2QUAAAAABV-Y_cjj8ukaWvWJv4g8ESPPvBg"></div>

                <div class="container-contact100-form-btn">
                    <button class="contact100-form-btn">
                        Send Email
                    </button>
                </div>
            </form>

            <div class="contact100-more">
                Contact us at: <span class="contact100-more-highlight">+1(614)421-1661</span>
            </div>
        </div>
    </div>
    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgpM9ahRFQKHujFCLboaSGbKJJs7DT8y8"></script>
    <script src="contact-us-form/js/map-custom.js"></script>
    <!--===============================================================================================-->
    <script src="contact-us-form/js/main.js"></script>


    <div class="modal fade" role="dialog" id="thank">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h2>Thank you!</h2>
                </div>
            </div>
        </div>
    </div>
    <?php
    if (isset($_POST['sub'])) {
        $name = $_POST['name'];
        $email_address = $_POST['email'];
        $message = $_POST['message'];
        $phone = $_POST['phone'];

// The message
//$message = "Line 1\r\nLine 2\r\nLine 3";
        $email_subject = "Website Contact Form:" . $name;
// In case any of our lines are larger than 70 characters, we should use wordwrap()
//$message = wordwrap($message, 70, "\r\n");
        $message = "You have received a new message from your website contact form.\n\n" . "Here are the details:\n\nName: $name\n\nEmail: $email_address\n\n\n\nPhone: $phone\n\nMessage:\n$message";

        $ToEmail = 'iskconcolumbus@gmail.com,support@iskcongreatercolumbus.com';
        $EmailSubject = 'Temple website contact form';
        $mailheader = "From: " . $_POST["email"] . "\r\n";
        $mailheader .= "Reply-To: " . $_POST["email"] . "\r\n";
        $mailheader .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $MESSAGE_BODY = "You have received a new message from your website contact form.<br/>";
        $MESSAGE_BODY .= "Name: " . $_POST["name"] . "<br/>";
        $MESSAGE_BODY .= "Email: " . $_POST["email"] . "<br/>";
        $MESSAGE_BODY .= "Phone: " . $_POST["phone"] . "<br/>";
        $MESSAGE_BODY .= "Comment: " . nl2br($_POST["message"]) . "<br/>";
//mail($ToEmail, $EmailSubject, $MESSAGE_BODY, $mailheader) or die ("Failure");
        sendemail($ToEmail, $EmailSubject, $MESSAGE_BODY) or die ("Failure");

// Send
        /*if(mail('abhi87700@gmail.com', $email_subject, $message))
        {*/
        ?>
        <script type="text/javascript">
            $('#thank').modal('show');
        </script>
        <?php
        /*}*/
    }
    ?>
    <!-- Google Maps -->
    <?php include "map.php"; ?>

    <?php include "temple-residents.php"; ?>
    <?php
    include("footer.php");
    ?>

</div>