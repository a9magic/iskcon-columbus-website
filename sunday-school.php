<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <title>Sunday School
    </title>
    <?php
    include('meta_links.php');
    ?>
</head>
<body class="product-template-default single single-product postid-247 _masterslider _msp_version_3.0.4 woocommerce woocommerce-page wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- /#preloader -->
<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php include('header.php'); ?>
    <section id="blog-heading" style="height:auto;">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text pull-left" style="width:99%;padding-top:25px;">
                    <h1 class="blog-title" style="text-align:center;font-size:31px;">Sunday School
                    </h1>
                </div>
                <!-- /.heading-text -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.heading-fullwidth -->
    </section>
    <div class="full-width">
        <div class="vc_row-fluid">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">


                    <div class="wow fadeIn" data-wow-delay="0.4s"
                         style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp width: 100%;"><img
                                src="images/sunday_school.jpg"
                                alt=""
                                title="" style="height: 80%; width: 100%; margin-top: 0px; margin-left: -0.5px;">
                    </div>

                </div>
            </div>
        </div>
    </div>


    <section id="shop" style="padding:0 !important;">
        <div class="container">
            <!-- festival -->
            <div class="full-width">
                <div class="vc_row-fluid">
                    <div class="vc_col-sm-12 wpb_column vc_column_container ">

                        <div class="wpb_wrapper">

                            <h2 class="section-title wow fadeInUp" style="padding: 15px 0 15px 0;"
                                data-wow-delay="0.4s"><i
                                        class="fa fa-graduation-cap fa-2x"></i> Sunday School </h2>
                            <!-- /.section-title -->

                            <div class="col-sm-6 wow fadeInLeft" data-wow-delay="0.4s">
                                <div class="all-event">
                                    <div class="event-post">

                                        <div class="event-period">
									<span class="period-session">
										<i class="fa fa-clock-o"></i> 1 Hour <span class="separator"></span> <i
                                                class="fa fa-map-marker"></i> 379 West 8th Ave Columbus, OH									</span><span
                                                    class="separator"></span>
                                            <?php include('socialmedia-share.php'); ?><!-- /.comments-share -->
                                        </div><!-- /.event-period -->
                                        <br/>
                                        <div class="feature-img">
                                            <img width="800" height="550"
                                                 src="images/sunday-school-2.png"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 srcset="images/sunday-school-2.png 800w, images/sunday-school-2.png 300w"
                                                 sizes="(max-width: 800px) 100vw, 800px"/></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 wow fadeInRight" data-wow-delay="0.4s" style="padding: 15px;">
                                ISKCON Columbus conducts two Sunday school classes every Sunday for children with aged 4
                                to 14. Children are taught Vedic knowledge in a very easy and practical in a fun and
                                safe environment. <br/><br/>
                                We follow a nice curriculum developed by very experienced and
                                talented devotees based on Vedic scriptures such as <span style="color: orangered;">Bhagavad-Gītā and Śrīmad-Bhāgavatam</span>.
                                Children also learn <span style="color: orangered;">sanskrit slokas, stories, participate in dramas and cultural
                                    activities</span>.
                                <br/><br/>
                                Some of the topics of the curriculum include: Devotional principles, Cow
                                protection, Vegetarian living, Festivals of India, <span style="color: orangered;">Introduction to                                              Bhagavad-Gita</span>,
                                Pastimes of Lord Krishna, Stories from Śrīmad-Bhāgavatam.

                            </div>

                            <div class="col-md-12 wow fadeInUp text-center" data-wow-delay="0.4s"
                                 style="padding: 15px;">
                                For further Information, Please contact Rama Tirtha das at 614-404-8570 or send an email
                                to ISKCONColumbus@gmail.com
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- end festivals -->

        <!-- Music player -->
        <?php include('music-player/music-player.html'); ?>


</div>
<div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span>
</div>

<?php
include('footer.php');
?>
</body>
</html>
