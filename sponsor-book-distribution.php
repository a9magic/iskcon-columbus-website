<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="//gmpg.org/xfn/11">
    <title>Sponsor Book Distribution
    </title>
    <?php
    include('meta_links.php');
    ?>
</head>
<body class="product-template-default single single-product postid-247 _masterslider _msp_version_3.0.4 woocommerce woocommerce-page wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- /#preloader -->
<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php include('header.php'); ?>
    <section id="blog-heading" style="height:auto;">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text pull-left" style="width:99%;padding-top:25px;">
                    <h1 class="blog-title" style="text-align:center;font-size:31px;">Sponsor Book Distribution
                    </h1>
                </div>
                <!-- /.heading-text -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.heading-fullwidth -->
    </section>

    <section id="shop" style="padding:0 !important;">
        <h3 class="text-center" style="padding-top:15px;">
            <a class="book" href="#book-distribution"> <u>Why Book Distribution is important?</u> </a></h3>

        <div class="vc_row-fluid">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">
                    <section class="shedule section-padding" style="background: url(images/books-dark.jpg);">
                        <div class="container">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="wow fadeInLeft" data-wow-delay="0.6s">
                                        <img width="100%" height="100%" src="images/book-distribution-front.jpg"
                                             class="attachment-shop_single size-shop_single wp-post-image"
                                             alt="Radha Natabara" title="Radha Natabara"/>
                                        <div class="thumbnails columns-3">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 wow fadeInRight" data-wow-delay="0.6s">
                                    <h2 class="section-title">Vedic Literature – Book Distribution</h2><br/>
                                    <h3><i class="fa fa-book"></i> Vedic Scriptures in 79 languages
                                    </h3>
                                    <div class="section-detail" style="color: white">
                                        <br/>At the <strong>ISKCON Columbus</strong> we take the
                                        distribution of Srila Prabhupada's books as a priority. Srila Prabhupada told us
                                        that Book Distribution is his life and soul. He also said,<br/> <i>"Even if one
                                            reads just one line carefully (from ISKCON books), his life will
                                            be successful!"<br/> "Every word, each and every word, is for the good of
                                            human society."</i></h4>
                                    </div>
                                </div>


                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>

                </div>
            </div>
        </div>

        <div class="container">
            <!-- festival -->
            <div class="full-width">
                <div class="vc_row-fluid">
                    <div class="vc_col-sm-12 wpb_column vc_column_container ">
                        <div class="wpb_wrapper">

                            <h2 class="section-title wow fadeInUp" style="padding: 15px 0 15px 0;"
                                data-wow-delay="0.4s"><i
                                        class="fa fa-cogs fa-2x"></i> How to Sponsor Book Distribution </h2>
                            <!-- /.section-title -->

                            <div class="sermons">

                                <div class="col-md-8 col-sm-8 text-center">
                                    <h1 class="wow fadeInUp" data-wow-delay="0.4s" style="padding: 10px;"><u>Use Credit/Debit Card</u></h1>
                                    <div class="col-md-6 col-sm-6 text-center">
                                        <div class="vc_box_shadow_border wow fadeInLeft text-center"
                                             data-wow-delay="0.6s"
                                             style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                            <h2 style="padding: 10px;">Book Distribution</h2>

                                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post"
                                                  target="_blank">
                                                <input type="hidden" name="cmd" value="_s-xclick">
                                                <input type="hidden" name="hosted_button_id" value="RZ52R52QTE2SL">
                                                <table>
                                                    <tr>
                                                        <td><input type="hidden" name="on0"
                                                                   value="Sponsor Bhagavad Gita Pack(s)">
                                                            <h3>Sponsor
                                                                Bhagavad Gita Pack(s)</h3>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><select name="os0">
                                                                <option value="Bhagavad Gita Pack">Bhagavad Gita Pack
                                                                    $108.00 USD
                                                                </option>
                                                                <option value="Srimad Bhagavatam Set">Śrīmad-Bhāgavatam
                                                                    Set $350.00 USD
                                                                </option>
                                                            </select></td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="hidden" name="on1" value="Memo">
                                                            <h3 style="padding-top: 15px">Memo</h3></td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="text" name="os1" maxlength="200"></td>
                                                    </tr>
                                                </table>
                                                <input type="hidden" name="currency_code" value="USD">
                                                <input type="image"
                                                       src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif"
                                                       border="0" name="submit"
                                                       alt="PayPal - The safer, easier way to pay online!">
                                                <img alt="" border="0"
                                                     src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1"
                                                     height="1">
                                            </form>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6 text-center">
                                        <div class="vc_box_shadow_border wow fadeInLeft text-center"
                                             data-wow-delay="0.6s"
                                             style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                            <h2 style="padding: 10px;">Distribute Books – General</h2>
                                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post"
                                                  target="_blank">
                                                <input type="hidden" name="cmd" value="_s-xclick">
                                                <input type="hidden" name="hosted_button_id" value="K94FLQYGZG3W2">
                                                <input type="image"
                                                       src="https://www.paypalobjects.com/WEBSCR-640-20110429-1/en_US/i/btn/btn_donateCC_LG.gif"
                                                       border="0" name="submit"
                                                       alt="PayPal - The safer, easier way to pay online!">
                                                <img alt="" border="0"
                                                     src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif"
                                                     width="1" height="1">
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="vc_box_shadow_border wow fadeInRight text-center" data-wow-delay="0.6s"
                                         style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                        <h1 style="padding: 10px;"><u>ACH Debit Form</u></h1>
                                        <?php include ('ach-form.php')?>
                                    </div>
                                </div>


                                <div class="coll-md-12 col-sm-12 text-center wow fadeInUp" data-wow-delay="0.6s"
                                     style="padding: 30px;">
                                    If setting up on your own: Payee name: ISKCON Columbus, Address: 379 West 8th Ave
                                    Columbus, OH 43201. If you have any questions, contact <strong>Ram Tirtha
                                        Das </strong> at 614-404-8570 or stop by in the temple.
                                </div>

                            </div>

                            <div class="col-md-12 wow fadeInUp" data-wow-delay="0.4s" style="padding: 30px;">
                                <strong>“One should distribute viṣṇu-prasāda to everyone. Know that Lord Viṣṇu (Krishna)
                                    is very pleased when everyone is sumptuously fed with viṣṇu-prasāda”. – Śrīmad-Bhāgavatam 8.16.56</strong>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end festivals -->
        </div>
        <br/>

        <h2 id="book-distribution" class="section-title wow fadeInUp" style="padding: 15px 0 15px 0;"
            data-wow-delay="0.4s"><i
                    class="fa fa-book fa-2x"></i> Book Distribution is Most Important and Pleasing to Srila Prabhupada: </h2>

        <hr>
        <blockquote> Your report of book distribution is very encouraging. Please go on increasing more and
            more. Yes, you
            should become the topmost temple for distributing my books. This is a very good proposal. That is
            the
            only way to please me. Distribute my books profusely. That is real preaching. (Letter to: Bala Krsna
            -
            Vrindaban 23 October, 1976)
        </blockquote>
        <hr>

        <blockquote>This book distribution is the essence of our mission. (Letter to: Rupanuga - Bombay 18
            December, 1974)
        </blockquote>
        <hr>
        <blockquote>Go on selling books. That is my desire. (Letter to: Kurusrestha: - Ahmedabad 26 September,
            1975)
        </blockquote>
        <hr>
        <blockquote>Your report of the book distribution there is very encouraging. Make program to distribute
            our books all
            over the world. Our books are being appreciated by learned circles, so we should take advantage.
            Whatever progress we have made, it is simply to distributing these books. So go on, and do not
            divert
            your mind for a moment from this. I have full confidence in you. (Letter to: Ramesvara: - Mayapur 11
            October, 1974)
        </blockquote>
        <hr>
        <blockquote>Whenever there is any publication in any language, it enlivens me 100 times. (Letter to:
            Hamsaduta: -
            Mayapur 19 October, 1974)
        </blockquote>
        <hr>
        <blockquote>Prabhupada: You consult among you. So I want to see simply distribution of books in any
            language. That I
            want. (Room Conversation - July 27-28, 1977, Vrndavana)
        </blockquote>
        <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span>
        </div>

        <h6 class="wow fadeInUp text-center" data-wow-delay="0.4s">
            We are a 501 (C) (3) non-profit charity organization. Our IRS tax id is <strong>34-1225-440</strong> and all donations
            made to 'ISKCON Columbus' are tax deductible.
        </h6>
        <?php
        include('footer.php');
        ?>
</body>
</html>
