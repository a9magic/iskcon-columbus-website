<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="keywords" content="Columbus ISKCON festivals, Ekadashi dates in Columbus, Ekadashi in ISKCON Columbus, Ekadashi Ohio">

    <link rel="profile" href="https://gmpg.org/xfn/11">

    <title>Festivals in 2020</title>
    <?php
    include('meta_links.php');
    ?>
</head>

<body class="events-template-default single single-events postid-74 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">

    <?php
    include('header.php');
    ?>
    <!-- Blog Heading -->
    <section id="blog-heading">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text">
                    <h1 class="blog-title text-center">Festivals</h1>
                </div><!-- /.heading-text -->
            </div><!-- /.container -->
        </div><!-- /.heading-fullwidth -->
    </section><!-- /#blog-heading -->
    <!-- Blog Heading end -->

    <!-- header image -->
    <div class="full-width">
        <div class="vc_row-fluid">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">


                    <div class="wow fadeIn" data-wow-delay="0.4s"
                         style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp; width: 100%;"><img
                            src="images/festivals-header.jpg"
                            alt=""
                            title="" style="height: 80%; width: 100%; margin-top: 0px; margin-left: -0.5px;">
                    </div>

                </div>
            </div>
        </div>
    </div>

    <?php include "festival-schedule.php";?>

    <?php
    include('footer.php');
    ?>


</body>
</html>
