<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <title>Upcoming Events</title>
    <?php
    include('meta_links.php');
    ?>
</head>

<body class="events-template-default single single-events postid-74 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">

    <?php
    include('header.php');
    ?>
    <!-- Blog Heading -->
    <section id="blog-heading">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text">
                    <h1 class="blog-title text-center">Upcoming Events</h1>
                </div><!-- /.heading-text -->
            </div><!-- /.container -->
        </div><!-- /.heading-fullwidth -->
    </section><!-- /#blog-heading -->
    <!-- Blog Heading end -->
    <style type="text/css">
        .panel-heading {
            cursor: pointer;
        }

        .panel-title:after {
            /* symbol for "opening" panels */
            font-family: 'Glyphicons Halflings'; /* essential for enabling glyphicon */
            content: "\e113"; /* adjust as needed, taken from bootstrap.css */
            float: right; /* adjust as needed */
            color: grey; /* adjust as needed */
        }

        .panel-title.collapsed:after {
            /* symbol for "collapsed" panels */
            content: "\e114"; /* adjust as needed, taken from bootstrap.css */
        }
    </style>
    <script type="javascript">
        $('#all').click(function () {
            if ($(this).text().indexOf('Show') >= 0) {
                $('.panel-collapse').collapse('show');
                $(this).text('Hide All');
            } else {
                $('.panel-collapse').collapse('hide');
                $(this).text('Show All');
            }
        });
    </script>
    <!-- Blog Page Container -->
    <section id="blog-page-container" style="padding-top:40px; background-image:url(images/black-wood-bg.jpg)">
        <div class="blog-page-wrapper">
            <div class="container">
                <div class="row">


                    <div class="container">

                        <!-- March 2018 Events -->
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-toggle="collapse" data-target='#collapse5'>
                                    <h3 class="collapsed text-center widget-title" data-toggle="collapse"
                                        data-target='#collapse5'>
                                        Click to see Events in March 2018
                                    </h3>

                                </div>
                                <div class="panel-collapse collapse" id="collapse5">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="all-event">
                                                <div class="event-post">

                                                    <h2 class="uppercase"><a
                                                                href=""
                                                                target="_blank"
                                                                title="Sri Gaura
                                                            Purnima celebrations on Thursday March 1st!"> Sri Gaura
                                                            Purnima celebrations on Thursday March 1st!</a></h2>
                                                    <div class="event-period">
									<span class="period-session">
										<i class="fa fa-clock-o"></i> 3.5 hours <span class="separator"></span> <i
                                                class="fa fa-map-marker"></i> 379 West 8th Ave Columbus, OH 43201</span><span
                                                                class="separator"></span>
                                                        <?php include('socialmedia-share.php'); ?><!-- /.comments-share -->
                                                    </div><!-- /.event-period -->
                                                    <div class="feature-img">
                                                        <img width="800" height="550"
                                                             src="images/events/gaura-purnima.jpg"
                                                             class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                             srcset="images/events/gaura-purnima.jpg 800w, images/events/gaura-purnima.jpg 300w"
                                                             sizes="(max-width: 800px) 100vw, 800px"/></div>
                                                    <div class="event-description">
                                                        <div class="media">
                                                            Please join us with your loved ones on Thursday 03/01 to celebrate the most auspicious appearance of Lord Sri Chaitanya Mahaprabhu. Below are the details of the festivities. We humbly request all of you to participate in the festivities and get eternal blessings of Lord Chaitanya. <br/>

                                                            <div class="media-left"><i
                                                                        class="fa fa-music base-color"></i></div>
                                                            <div class="media-body">Abhishek of Sri Gaura Nitai deities<br></div>
                                                        </div>
                                                        <div class="media">
                                                            <div class="media-left"><i
                                                                        class="fa fa-bullhorn base-color"></i></div>
                                                            <div class="media-body">Lively kirtan (call-and-response
                                                                chanting)<br>
                                                            </div>
                                                        </div>
                                                        <div class="media">
                                                            <div class="media-left"><i
                                                                        class="fa fa-cutlery base-color"></i></div>
                                                            <div class="media-body">Delicious Prasadam<br></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br/>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="event-location">
                                                <h2 class="widget-title">View Location</h2>
                                                <div class="eventMap">
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3056.7323308791683!2d-83.01912548461696!3d39.99208667941676!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88388eea31eb70cd%3A0xc3081bd6d7b6ddff!2s379+W+8th+Ave%2C+Columbus%2C+OH+43201!5e0!3m2!1sen!2sus!4v1502756333422"
                                                            width="100%" height="100%" frameborder="0" style="border:0"
                                                            allowfullscreen></iframe>
                                                </div>


                                            </div>

                                            <div class="event-detail">
                                                <h2 class="widget-title">Event Details</h2>
                                                <div class="address">
                                                    <div class="media">
                                                        <div class="media-left"><i
                                                                    class="fa fa-map-marker base-color"></i></div>
                                                        <div class="media-body"><strong>Where:</strong> ISKCON Columbus - Krishna
                                                            House <br/>
                                                            379 W 8th Ave, Columbus, Ohio 43201
                                                            <br></div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i
                                                                    class="fa fa-calendar-o base-color"></i></div>
                                                        <div class="media-body"><strong>When:</strong> <br/>
                                                            March 1st, 2018: <strong>6:00 PM to 9:15 PM
                                                                </strong><br/>
                                                        </div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i class="fa fa-clock-o base-color"></i>
                                                        </div>
                                                        <div class="media-body"><strong>Who:</strong> Everyone is
                                                            invited to attend<br>
                                                        </div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i class="fa fa-money base-color"></i>
                                                        </div>
                                                        <div class="media-body"><strong>Cost:</strong> FREE <br></div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i class="fa fa-user base-color"></i>
                                                        </div>
                                                        <div class="media-body"><strong>What to wear:</strong>
                                                            Casual<br></div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <form action="https://maps.google.com/maps" method="get"
                                                          target="_blank">
                                                        <input type="text" placeholder="Enter Your Location"
                                                               name="saddr"/>
                                                        <input type="hidden" name="daddr"
                                                               value="379 W 8th Ave, Columbus, Ohio 43201"/>
                                                        <input type="submit" class="btn custom-btn"
                                                               style="color: white; border-radius:20px;"
                                                               value="Get directions"/>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>

                                        <!-- Evennt Schedule -->
                                        <div class="vc_col-sm-12 ">
                                            <section class="shedule"
                                                     style="background: url(images/Untitled-1-1.jpg);">
                                                <div class="container" style="width: 100%">
                                                    <div class="row">

                                                            <div class="shedule-list col-md-12 wow fadeInUp"
                                                                 data-wow-delay="0.1s">
                                                                <h2>Gaura Purnima Schedule</h2>
                                                                <p></p>

                                                                <div class="tab-content">
                                                                    <div id="daily"
                                                                         class="tab-pane fade in active">
                                                                        <table class="festivals table thead-inverse table-hover table-responsive">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    Time
                                                                                </th>
                                                                                <th>
                                                                                    Event
                                                                                </th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                    MORNING
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    5:00 AM
                                                                                </td>
                                                                                <td>
                                                                                    Mangal Aarti
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    6:00 AM
                                                                                </td>
                                                                                <td>
                                                                                    Japa Meditation (darsan closed for deity dressing)
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    8:15 AM
                                                                                </td>
                                                                                <td>
                                                                                    Guru Puja
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    8:30 AM
                                                                                </td>
                                                                                <td>
                                                                                    Special Sringar darsan of Deities in new outfits
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    EVENING
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    6:00 PM
                                                                                </td>
                                                                                <td>
                                                                                    Bhajans
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    6:15 PM
                                                                                </td>
                                                                                <td>
                                                                                    "Dhule Dhule Gora Chanda" by Krsnaya
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    6:30 PM to 7:15 PM
                                                                                </td>
                                                                                <td>
                                                                                    Abhishek of Sri Gaura Nitai deities
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    7:15 PM - 8:15 PM
                                                                                </td>
                                                                                <td>
                                                                                    "Glories of Lord Chaitanya" by HH Bhaktimarg Swami
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    8:20 PM
                                                                                </td>
                                                                                <td>
                                                                                    Performance by Sunday school children
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    8:25 PM to 9:00 PM
                                                                                </td>
                                                                                <td>
                                                                                    Gaura Aarti & Dancing Kirtan
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    9:00 PM
                                                                                </td>
                                                                                <td>
                                                                                    Lord Narsimha prayers and announcements
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    9:15 PM
                                                                                </td>
                                                                                <td>
                                                                                    Prasadam serve out (Vegetarian food offered to the Lord)
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                    </div><!-- /.row -->
                                                </div><!-- /.container -->
                                            </section>

                                        </div>


                                        <div class="col-md-12 wow fadeInUp text-center" data-wow-delay="0.4s"
                                             style="padding: 15px;">
                                            <strong>To RSVP</strong> please call / text us on <i
                                                    class="fa fa-phone-square"></i> <a
                                                    href="tel:727-804-9835"> 727-804-9835 </a> / <a
                                                    href="tel:727-804-9836">
                                                727-804-9836</a> or email us at
                                            <a style="color: #0077b3"
                                               href="mailto:spiritualyoga108@gmail.com?Subject=Maha%20Satsang"
                                               target="_top">spiritualyoga108@gmail.com</a>
                                            to let us know so that we can prepare dinner prasad
                                            accordingly. We sure hope that you can attend and please invite your
                                            FAMILIES, FRIENDS
                                            AND COLLEAGUES TOO!
                                            <br/><br/>
                                            Please inform all your friends who understands Hindi and invite everyone to
                                            this wonderful Bhagavat Katha event.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>

                        <!-- January 2018 Events -->
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-toggle="collapse" data-target='#collapse4'>
                                    <h3 class="collapsed text-center widget-title" data-toggle="collapse"
                                        data-target='#collapse4'>
                                        Click to see Events in January 2018
                                    </h3>

                                </div>
                                <div class="panel-collapse collapse" id="collapse4">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="all-event">
                                                <div class="event-post">

                                                    <h2 class="uppercase"><a
                                                                href=""
                                                                target="_blank"
                                                                title="Welcoming New Year at New Temple Land"> Welcoming
                                                            New Year at ISKCON Krishna House</a></h2>
                                                    <div class="event-period">
									<span class="period-session">
										<i class="fa fa-clock-o"></i> 3 hours <span class="separator"></span> <i
                                                class="fa fa-map-marker"></i> 379 West 8th Ave Columbus, OH 43201</span><span
                                                                class="separator"></span>
                                                        <?php include('socialmedia-share.php'); ?><!-- /.comments-share -->
                                                    </div><!-- /.event-period -->
                                                    <div class="feature-img">
                                                        <img width="800" height="550"
                                                             src="images/events/NewYear.jpg"
                                                             class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                             srcset="images/events/NewYear.jpg 800w, images/events/NewYear.jpg 300w"
                                                             sizes="(max-width: 800px) 100vw, 800px"/></div>
                                                    <div class="event-description">
                                                        <div class="media">
                                                            <div class="media-left"><i
                                                                        class="fa fa-music base-color"></i></div>
                                                            <div class="media-body">Pushpa Abhishek of Radha Krishna
                                                                Deities<br></div>
                                                        </div>
                                                        <div class="media">
                                                            <div class="media-left"><i
                                                                        class="fa fa-bullhorn base-color"></i></div>
                                                            <div class="media-body">Lively kirtan (call-and-response
                                                                chanting)<br>
                                                            </div>
                                                        </div>
                                                        <div class="media">
                                                            <div class="media-left"><i
                                                                        class="fa fa-cutlery base-color"></i></div>
                                                            <div class="media-body">Delicious Prasadam<br></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br/>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="event-location">
                                                <h2 class="widget-title">View Location</h2>
                                                <div class="eventMap">
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3056.7323308791683!2d-83.01912548461696!3d39.99208667941676!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88388eea31eb70cd%3A0xc3081bd6d7b6ddff!2s379+W+8th+Ave%2C+Columbus%2C+OH+43201!5e0!3m2!1sen!2sus!4v1502756333422"
                                                            width="100%" height="100%" frameborder="0" style="border:0"
                                                            allowfullscreen></iframe>
                                                </div>


                                            </div>

                                            <div class="event-detail">
                                                <h2 class="widget-title">Event Details</h2>
                                                <div class="address">
                                                    <div class="media">
                                                        <div class="media-left"><i
                                                                    class="fa fa-map-marker base-color"></i></div>
                                                        <div class="media-body"><strong>Where:</strong> ISKCON Krishna
                                                            House <br/>
                                                            379 W 8th Ave, Columbus, Ohio 43201
                                                            <br></div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i
                                                                    class="fa fa-calendar-o base-color"></i></div>
                                                        <div class="media-body"><strong>When:</strong> <br/>
                                                            January 1st, 2018: <strong>11:00 am to 2:00
                                                                pm </strong><br/>
                                                        </div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i class="fa fa-clock-o base-color"></i>
                                                        </div>
                                                        <div class="media-body"><strong>Who:</strong> Everyone is
                                                            invited to attend<br>
                                                        </div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i class="fa fa-money base-color"></i>
                                                        </div>
                                                        <div class="media-body"><strong>Cost:</strong> FREE <br></div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i class="fa fa-user base-color"></i>
                                                        </div>
                                                        <div class="media-body"><strong>What to wear:</strong>
                                                            Casual<br></div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <form action="https://maps.google.com/maps" method="get"
                                                          target="_blank">
                                                        <input type="text" placeholder="Enter Your Location"
                                                               name="saddr"/>
                                                        <input type="hidden" name="daddr"
                                                               value="379 W 8th Ave, Columbus, Ohio 43201"/>
                                                        <input type="submit" class="btn custom-btn"
                                                               style="color: white; border-radius:20px;"
                                                               value="Get directions"/>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="col-md-12 wow fadeInUp text-center" data-wow-delay="0.4s"
                                             style="padding: 15px;">
                                            <strong>To RSVP</strong> please call / text us on <i
                                                    class="fa fa-phone-square"></i> <a
                                                    href="tel:727-804-9835"> 727-804-9835 </a> / <a
                                                    href="tel:727-804-9836">
                                                727-804-9836</a> or email us at
                                            <a style="color: #0077b3"
                                               href="mailto:spiritualyoga108@gmail.com?Subject=Maha%20Satsang"
                                               target="_top">spiritualyoga108@gmail.com</a>
                                            to let us know so that we can prepare dinner prasad
                                            accordingly. We sure hope that you can attend and please invite your
                                            FAMILIES, FRIENDS
                                            AND COLLEAGUES TOO!
                                            <br/><br/>
                                            Please inform all your friends who understands Hindi and invite everyone to
                                            this wonderful Bhagavat Katha event.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>

                        <!-- October Events -->
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-toggle="collapse" data-target='#collapse3'>
                                    <h3 class="collapsed text-center widget-title" data-toggle="collapse"
                                        data-target='#collapse3'>
                                        Click to see Events in October 2017
                                    </h3>
                                </div>
                                <div class="panel-collapse collapse" id="collapse3">
                                    <div class="panel-body">

                                        <!-- Diwali Dinner -->
                                        <div class="portal item" data-wow-delay="0.1s"
                                             style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp; background: #f7f7f7;">
                                            <div class="col-md-6">
                                                <a href="https://www.eventbrite.com/e/diwali-dinner-and-kartik-damodar-pooja-tickets-36826156024?aff=es2#tickets"
                                                   target="_blank">
                                                    <img style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"
                                                         height="600"
                                                         src="images/diwali-dinner.jpg"
                                                         class="attachment-thumbnail size-thumbnail wp-post-image"
                                                         alt=""
                                                         srcset="images/diwali-dinner.jpg 180w, images/diwali-dinner.jpg 300w"
                                                         sizes="(max-width: 300px) 100vw, 150px">
                                                </a>
                                            </div>

                                            <div class="col-md-6 text-center">
                                                <h2 style="font-family:Satisfy;font-size:2em;position:relative">Purchase
                                                    tickets and <u>more details</u> on <br/></h2>
                                                <a href="https://www.eventbrite.com/e/diwali-dinner-and-kartik-damodar-pooja-tickets-36826156024?aff=es2#tickets"
                                                   target="_blank">
                                                    <img src="images/eventbrite.jpg" style="width: 25%; height: auto;">
                                                </a>
                                                <br/><br/>
                                                <h2 style="font-family:Satisfy;font-size:2em;position:relative">View
                                                    Location</h2>
                                                <div class="eventMap eventMap1"></div>
                                                <script>
                                                    jQuery(document).ready(function () {
                                                        var teamID = jQuery(".eventMap1");
                                                        if (teamID.length) {
                                                            function isMobile() {
                                                                return ('ontouchstart' in document.documentElement);
                                                            }

                                                            function init_gmap() {
                                                                if (typeof google == 'undefined') return;
                                                                var options = {
                                                                    center: [40.1071128, -83.1409204],
                                                                    zoom: 15,
                                                                    mapTypeControl: true,
                                                                    mapTypeControlOptions: {
                                                                        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                                                                    },
                                                                    navigationControl: true,
                                                                    scrollwheel: false,
                                                                    streetViewControl: true
                                                                }

                                                                if (isMobile()) {
                                                                    options.draggable = false;
                                                                }

                                                                jQuery('.eventMap1').gmap3({
                                                                    map: {
                                                                        options: options
                                                                    },
                                                                    marker: {
                                                                        latLng: [40.1071128, -83.1409204],
                                                                        options: {icon: ""}
                                                                    }
                                                                });
                                                            }

                                                            init_gmap();
                                                        }

                                                    });

                                                </script>
                                                <div>
                                                    <form action="//maps.google.com/maps" method="get" target="_blank">
                                                        <input type="text" placeholder="Enter Your Location"
                                                               name="saddr"/>
                                                        <input type="hidden" name="daddr" value="5600 Post Road
Dublin, OH 43017"/>
                                                        <input type="submit" class="btn custom-btn"
                                                               style="color: white; border-radius:20px;"
                                                               value="Get directions"/>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                        <br/>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>

                        <!-- August Events -->
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-toggle="collapse" data-target='#collapse2'>
                                    <h3 class="collapsed text-center widget-title" data-toggle="collapse"
                                        data-target='#collapse2'>
                                        Click to see Events in August 2017
                                    </h3>

                                </div>
                                <div class="panel-collapse collapse" id="collapse2">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="all-event">
                                                <div class="event-post">

                                                    <h2 class="uppercase"><a
                                                                href="https://www.facebook.com/events/156637048223885"
                                                                target="_blank"
                                                                title="Sri Krishna Janmasthami "> Sri Krishna
                                                            Janmasthami </a></h2> <h4><a
                                                                href="https://www.facebook.com/events/156637048223885"
                                                                target="_blank">Facebook Event</a></h4>
                                                    <div class="event-period">
									<span class="period-session">
										<i class="fa fa-clock-o"></i> 5 AM - 12 AM <span class="separator"></span> <i
                                                class="fa fa-map-marker"></i> 379 W 8th Ave, Columbus, Ohio 43201</span><span
                                                                class="separator"></span>
                                                        <?php include('socialmedia-share.php'); ?><!-- /.comments-share -->
                                                        <br/>
                                                        We would like to invite you and your loved ones to celebrate the
                                                        most auspicious occasion of Sri Krishna Janmastami, the divine
                                                        appearance of Lord Krishna on Tuesday 8/15.<br/>
                                                        Please Come to as many of the possible events-
                                                    </div><!-- /.event-period -->
                                                    <div class="feature-img">
                                                        <img width="800" height="550"
                                                             src="images/janmasthami1.jpg"
                                                             class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                             srcset="images/janmasthami1.jpg 800w, images/janmasthami1.jpg 300w"
                                                             sizes="(max-width: 800px) 100vw, 800px"/></div>
                                                </div>
                                                <br/>


                                                <h3 class="wow fadeInUp" data-wow-delay="0.4s">Parking Details</h3><br/>
                                                <div class="feature-img">
                                                    <img width="800" height="550"
                                                         src="images/janmasthami-parking.jpg"
                                                         class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                         srcset="images/janmasthami-parking.jpg 800w, images/janmasthami-parking.jpg 300w"
                                                         sizes="(max-width: 800px) 100vw, 800px"/></div>


                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="event-location">
                                                <h2 class="widget-title">View Location</h2>
                                                <div class="eventMap">
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3056.7323308791683!2d-83.01912548461696!3d39.99208667941676!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88388eea31eb70cd%3A0xc3081bd6d7b6ddff!2s379+W+8th+Ave%2C+Columbus%2C+OH+43201!5e0!3m2!1sen!2sus!4v1502756333422"
                                                            width="100%" height="100%" frameborder="0" style="border:0"
                                                            allowfullscreen></iframe>
                                                </div>


                                            </div>

                                            <div class="event-detail">
                                                <h2 class="widget-title">Event Details</h2>
                                                <div class="address">
                                                    <div class="media">
                                                        <div class="media-left"><i
                                                                    class="fa fa-map-marker base-color"></i></div>
                                                        <div class="media-body"><strong>Where:</strong> ISKCON Columbus
                                                            <br/>
                                                            379 W 8th Ave, Columbus, Ohio 43201
                                                            <br></div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i
                                                                    class="fa fa-calendar-o base-color"></i></div>
                                                        <div class="media-body"><strong>When:</strong> <br/>

                                                            <i class="fa fa-clock-o"></i><strong>Morning
                                                                programs:</strong><br/>
                                                            <strong> 05:00 AM - 05:45 AM: </strong> Mangal aarti<br/>
                                                            <strong> 05:45 AM - 07:15 AM: </strong> Group Japa<br/>
                                                            <strong> 07:15 AM - 07:30 AM: </strong> Guru puja<br/>
                                                            <strong> 07:30 AM - 07:45 AM: </strong> Sringar darsan<br/>

                                                            <br/>
                                                            <i class="fa fa-clock-o"></i><strong>Evening
                                                                programs:</strong><br/><br/>

                                                            Temple room:<br/>

                                                            <strong> 06:30 PM - 07:30 PM: </strong> Bhajans<br/>
                                                            <strong> 07:30 PM - 09:30 PM: </strong> Abhishekam
                                                            ceremony<br/>
                                                            <strong> 09:30 PM - 12:00 AM: </strong> Bhajans (darshan
                                                            closed during this time)<br/>
                                                            <strong> 12:00 AM - 12:30 AM: </strong> Special darshan,
                                                            Aarti and Kirtan<br/><br/>

                                                            Temple outside:<br/>
                                                            <strong>06:30 pm – 06:45 pm:</strong> Hare Krishna bhajans
                                                            by Prema Sindhu das<br/>
                                                            <strong>06:45 pm – 06:50 pm:</strong> Sloka recitation by
                                                            Ujwala Nitai das<br/>
                                                            <strong>06.50 pm – 07.00 pm:</strong> “Dasavatara”
                                                            recitation by Pariksith Pothamsetty<br/>
                                                            <strong>07.00 pm – 07.05 pm:</strong> Classical dance
                                                            performance by
                                                            Anaya Jaiswal<br/>
                                                            <strong>07:00 pm – 07:15 pm:</strong> Classical dance
                                                            performance
                                                            by Kaustavi<br/>
                                                            <strong>07:15 pm – 07:25 pm:</strong> Flute by Torin<br/>
                                                            <strong>07:30 pm – 08:00 pm:</strong> “Pastimes of Lord
                                                            Krishna” by Mother Krishna Nandini<br/>
                                                            <strong>08:00 pm – 08:45 pm:</strong> Yakshagana
                                                            Performance<br/>
                                                            <strong>08:45 pm – 09:00 pm:</strong> “Krishna
                                                            Lila Stava” performance by Sunday
                                                            school kids<br/>
                                                            <strong>09:10 pm – 09:20 pm:</strong>
                                                            Classical dance performance by
                                                            Ishika Jaryal<br/>
                                                            <strong>09:20 pm – 09:50 pm:</strong>
                                                            “Pastimes of Lord Krishna”
                                                            by Mother Malati<br/>
                                                            <strong>09:50 pm – 10:05 pm:</strong>
                                                            Hare Krishna! Film
                                                            trailer<br/>
                                                            <strong>10:10 pm – 10:20 pm:</strong>
                                                            Classical dance
                                                            performance by Roshni
                                                            Datta<br/>
                                                            <strong>10:20 pm – 10:50 pm:</strong>
                                                            Hare Krishna bhajans<br/>
                                                            <strong>10:50 pm – 11:30 pm:</strong>
                                                            “Lord Krishna’s
                                                            appearance” by Prem
                                                            Vilas das<br/>
                                                            <strong>12.30 AM -</strong> Special
                                                            maha-prasadam serve out<br/><br/>


                                                        </div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i class="fa fa-clock-o base-color"></i>
                                                        </div>
                                                        <div class="media-body"><strong>Who:</strong> Everyone is
                                                            invited to attend<br>
                                                        </div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i class="fa fa-money base-color"></i>
                                                        </div>
                                                        <div class="media-body"><strong>Cost:</strong> FREE <br></div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i class="fa fa-user base-color"></i>
                                                        </div>
                                                        <div class="media-body"><strong>What to wear:</strong>
                                                            Casual<br></div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <form action="https://maps.google.com/maps" method="get"
                                                          target="_blank">
                                                        <input type="text" placeholder="Enter Your Location"
                                                               name="saddr"/>
                                                        <input type="hidden" name="daddr"
                                                               value="379 W 8th Ave, Columbus, Ohio 43201"/>
                                                        <input type="submit" class="btn custom-btn"
                                                               style="color: white; border-radius:20px;"
                                                               value="Get directions"/>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="col-md-12 wow fadeInUp text-center" data-wow-delay="0.4s"
                                             style="padding: 15px;">
                                            <strong>To RSVP</strong> please call / text us on <i
                                                    class="fa fa-phone-square"></i> <a
                                                    href="tel:727-804-9835"> 727-804-9835 </a> / <a
                                                    href="tel:727-804-9836">
                                                727-804-9836</a> or email us at
                                            <a style="color: #0077b3"
                                               href="mailto:spiritualyoga108@gmail.com?Subject=Maha%20Satsang"
                                               target="_top">iskconcolumbus@gmail.com</a>. We sure hope that you can
                                            attend and please invite your
                                            FAMILIES, FRIENDS
                                            AND COLLEAGUES TOO!
                                            <br/>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <br/>
                        <!-- July Events -->
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-toggle="collapse" data-target='#collapse1'>
                                    <h3 class="collapsed text-center widget-title" data-toggle="collapse"
                                        data-target='#collapse1'>
                                        Click to see Events in July 2017
                                    </h3>

                                </div>
                                <div class="panel-collapse collapse" id="collapse1">
                                    <div class="panel-body">
                                        <div class="col-md-8">
                                            <div class="all-event">
                                                <div class="event-post">

                                                    <h2 class="uppercase"><a
                                                                href="https://www.facebook.com/events/1965671043669344"
                                                                target="_blank"
                                                                title="Hindi Bhagavat Katha by Lal Govind Prabhu"> Hindi
                                                            Bhagavat
                                                            Katha by Lal Govind Prabhu</a></h2>
                                                    <div class="event-period">
									<span class="period-session">
										<i class="fa fa-clock-o"></i> 3 days <span class="separator"></span> <i
                                                class="fa fa-map-marker"></i> 3520 Walker Road Hilliard, Ohio-43026</span><span
                                                                class="separator"></span>
                                                        <?php include('socialmedia-share.php'); ?><!-- /.comments-share -->
                                                    </div><!-- /.event-period -->
                                                    <div class="feature-img">
                                                        <img width="800" height="550"
                                                             src="images/bhagwatSapta.jpg"
                                                             class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                             srcset="images/bhagwatSapta.jpg 800w, images/bhagwatSapta.jpg 300w"
                                                             sizes="(max-width: 800px) 100vw, 800px"/></div>
                                                    <div class="event-description">
                                                        <div class="media">
                                                            <div class="media-left"><i
                                                                        class="fa fa-music base-color"></i></div>
                                                            <div class="media-body">Lively kirtan (call-and-response
                                                                chanting)<br></div>
                                                        </div>
                                                        <div class="media">
                                                            <div class="media-left"><i
                                                                        class="fa fa-bullhorn base-color"></i></div>
                                                            <div class="media-body">Thought-provoking discussion on the
                                                                Srimad Bhagavatam<br>
                                                            </div>
                                                        </div>
                                                        <div class="media">
                                                            <div class="media-left"><i
                                                                        class="fa fa-cutlery base-color"></i></div>
                                                            <div class="media-body">Dinner Prasadam<br></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br/>


                                                <h4 class="wow fadeInUp" data-wow-delay="0.4s">Hare Krishna Dear
                                                    Friends,<br/><br/>

                                                    It is with great pleasure that we remind you that <strong>His Grace
                                                        Lal
                                                        Govind Prabhu</strong> will be visiting Columbus and will be
                                                    speaking on
                                                    <strong>Srimad Bhagavatam in HINDI</strong> from JULY 28 to JULY 30
                                                    ( Friday to
                                                    Sunday). <br/><br/>
                                                    He is a wonderful speaker on the Srimad Bhagavatam which is
                                                    interspersed with his melodious singing.

                                                    <h2>Prasadam will be served on all 3 days.</h2></h4>


                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="event-location">
                                                <h2 class="widget-title">View Location</h2>
                                                <div class="eventMap">
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3055.3991917482817!2d-83.22158968461603!3d40.02186597941305!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x883896708e160dbf%3A0xaef76c9ffe609cca!2s3520+Walker+Rd%2C+Hilliard%2C+OH+43026!5e0!3m2!1sen!2sus!4v1500565487543"
                                                            width="100%" height="100%" frameborder="0" style="border:0"
                                                            allowfullscreen></iframe>
                                                </div>


                                            </div>

                                            <div class="event-detail">
                                                <h2 class="widget-title">Event Details</h2>
                                                <div class="address">
                                                    <div class="media">
                                                        <div class="media-left"><i
                                                                    class="fa fa-map-marker base-color"></i></div>
                                                        <div class="media-body"><strong>Where:</strong> Hare Krishna
                                                            Land ( Property on New ISKCON Temple Land) <br/>
                                                            3520 Walker Road, Hilliard, Ohio-43026
                                                            <br></div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i
                                                                    class="fa fa-calendar-o base-color"></i></div>
                                                        <div class="media-body"><strong>When:</strong> <br/>
                                                            July 28 Friday: <strong>6:30 pm to 9:30 pm</strong> <br/>
                                                            July 29 Saturday: <strong>11:00 am to 7:00 pm </strong><br/>
                                                            July 30 Sunday: <strong>11:00 am to 4:00 pm</strong><br/>
                                                        </div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i class="fa fa-clock-o base-color"></i>
                                                        </div>
                                                        <div class="media-body"><strong>Who:</strong> Everyone is
                                                            invited to attend<br>
                                                        </div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i class="fa fa-money base-color"></i>
                                                        </div>
                                                        <div class="media-body"><strong>Cost:</strong> FREE <br></div>
                                                    </div>
                                                    <div class="media">
                                                        <div class="media-left"><i class="fa fa-user base-color"></i>
                                                        </div>
                                                        <div class="media-body"><strong>What to wear:</strong>
                                                            Casual<br></div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <form action="https://maps.google.com/maps" method="get"
                                                          target="_blank">
                                                        <input type="text" placeholder="Enter Your Location"
                                                               name="saddr"/>
                                                        <input type="hidden" name="daddr"
                                                               value="3520 Walker Road Hilliard, Ohio-43026"/>
                                                        <input type="submit" class="btn custom-btn"
                                                               style="color: white; border-radius:20px;"
                                                               value="Get directions"/>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="col-md-12 wow fadeInUp text-center" data-wow-delay="0.4s"
                                             style="padding: 15px;">
                                            <strong>To RSVP</strong> please call / text us on <i
                                                    class="fa fa-phone-square"></i> <a
                                                    href="tel:727-804-9835"> 727-804-9835 </a> / <a
                                                    href="tel:727-804-9836">
                                                727-804-9836</a> or email us at
                                            <a style="color: #0077b3"
                                               href="mailto:spiritualyoga108@gmail.com?Subject=Maha%20Satsang"
                                               target="_top">spiritualyoga108@gmail.com</a>
                                            to let us know so that we can prepare dinner prasad
                                            accordingly. We sure hope that you can attend and please invite your
                                            FAMILIES, FRIENDS
                                            AND COLLEAGUES TOO!
                                            <br/><br/>
                                            Please inform all your friends who understands Hindi and invite everyone to
                                            this wonderful Bhagavat Katha event.
                                        </div>

                                        <div class="col-md-4 hidden-xs">
                                            <div class="all-event">
                                                <div class="event-post">

                                                    <h2 class="uppercase"><a
                                                                href=""
                                                                title="Hindi Bhagavat Katha by Lal Govind Prabhu"></a>
                                                    </h2>

                                                    <div class="feature-img">
                                                        <img width="800" height="550"
                                                             src="images/unnamed.png"
                                                             class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                             srcset="images/unnamed.png 800w, images/unnamed.png 300w"
                                                             sizes="(max-width: 800px) 100vw, 800px"/></div>
                                                </div>
                                                <br/>
                                            </div>
                                        </div>
                                        <div class="col-md-8 hidden-xs">
                                            <div class="event-location">
                                                <h2 class="widget-title">Bio
                                                    and photo of Lal Govind Prabhu </h2>
                                                <div class="eventMap">
                                                    He hails from a Vaisnav family in Gujarat and he grew up in Mumbai.
                                                    Since childhood he has been reading Bhagavad Gita and Srimad
                                                    Bhagavatam and singing bhajans and doing kirtan in ISKCON Juhu in
                                                    Mumbai. He is a disciple of HH Radha Govind Swami. His lectures on
                                                    Bhagavat Katha wonderfully blend references from Srimad Bhagavatam,
                                                    Bhagavad Gita, Ramayan, Mahabharat, Chaitanya Charitamrita, Puranas
                                                    and other Vedic scriptures making lectures more real with
                                                    surrealistic musical effects. He has inspired thousands of people in
                                                    Krishna Consciousness. He travels widely and has delivered lectures
                                                    in countries in Asia, America, Australia, Europe and Africa.His
                                                    lectures hold special attention and interest on internet with a
                                                    startling attendance of over 2.1 million viewers on Youtube etc. He
                                                    delivers lectures in Gujarati and Hindi with equal ease and
                                                    dexterity.
                                                    <br/><br/>
                                                    You can also know more about him and his lectures by going to
                                                    lalgovinddas.com
                                                </div>


                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                    </div>

                </div><!-- row -->
            </div> <!-- #container -->
        </div><!-- #blog-page-wrapper -->
    </section>

    <?php
    include('footer.php');
    ?>


</body>
</html>
