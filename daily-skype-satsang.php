<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <title>Skype Satsang</title>
    <?php
    $pageDescription = 'Join our Daily Skype satsang to discuss Vedic Scriptures!';
    $pageKeywords = 'ISKCON Columbus,  Columbus Krishna House, Columbus temple, Hindu Temple in Columbus, Radha Krishna, ISKCON Ohio, Radha Natabara, Skype Satsang';
    $pageCanonical = 'https://www.iskconcolumbus.com/daily-skype-satsang.php';
    include('meta_links.php');
    ?>
</head>

<body class="events-template-default single single-events postid-74 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">

    <?php
    include('header.php');
    ?>
    <!-- Blog Heading -->
    <section id="blog-heading">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text">
                    <h1 class="blog-title text-center"><i class="fa fa-skype"></i> Daily Skype Satsang!</h1>
                </div><!-- /.heading-text -->
            </div><!-- /.container -->
        </div><!-- /.heading-fullwidth -->
    </section><!-- /#blog-heading -->
    <!-- Blog Heading end -->

    <!-- Blog Page Container -->
    <section id="blog-page-container">
        <div class="blog-page-wrapper">
            <div class="container">
                <div class="row">


                    <div class="col-md-12 wow fadeInLeft" data-wow-delay="0.4s">
                        <div class="all-event">
                            <div class="">

                                <div class="feature-img">
                                    <img style="position:relative;overflow:hidden;-webkit-transition:opacity 1s, -webkit-transform 1s;transition:opacity 1s, transform 1s;-webkit-backface-visibility:hidden;backface-visibility:hidden;width:100%;" width="800" height="550"
                                         src="images/daily-skype-header.jpg"
                                         class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                         srcset="images/daily-skype-header.jpg 800w, images/daily-skype-header.jpg 300w"
                                         sizes="(max-width: 800px) 100vw, 800px"/></div>
                                <!--                                <div class="event-description">-->
                                <!--                                    <div class="media">-->
                                <!--                                        <div class="media-left"><i class="fa fa-align-center base-color"></i></div>-->
                                <!--                                        <div class="media-body">Tour of New Temple Land at 4:30 pm<br></div>-->
                                <!--                                    </div>-->
                                <!--                                    <div class="media">-->
                                <!--                                        <div class="media-left"><i class="fa fa-music base-color"></i></div>-->
                                <!--                                        <div class="media-body">Melodious and ecstatic kirtan<br></div>-->
                                <!--                                    </div>-->
                                <!--                                    <div class="media">-->
                                <!--                                        <div class="media-left"><i class="fa fa-bullhorn base-color"></i></div>-->
                                <!--                                        <div class="media-body">Thought-provoking talk by His Grace Prem Vilas das<br>-->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                    <div class="media">-->
                                <!--                                        <div class="media-left"><i class="fa fa-users base-color"></i></div>-->
                                <!--                                        <div class="media-body">Engaging children’s activities.<br></div>-->
                                <!--                                    </div>-->
                                <!--                                    <div class="media">-->
                                <!--                                        <div class="media-left"><i class="fa fa-heart base-color"></i></div>-->
                                <!--                                        <div class="media-body">Mantra meditation<br></div>-->
                                <!--                                    </div>-->
                                <!--                                    <div class="media">-->
                                <!--                                        <div class="media-left"><i class="fa fa-newspaper-o base-color"></i></div>-->
                                <!--                                        <div class="media-body">Cultural program<br></div>-->
                                <!--                                    </div>-->
                                <!--                                    <div class="media">-->
                                <!--                                        <div class="media-left"><i class="fa fa-group base-color"></i></div>-->
                                <!--                                        <div class="media-body">Stand up aarti and kirtan<br></div>-->
                                <!--                                    </div>-->
                                <!--                                    <div class="media">-->
                                <!--                                        <div class="media-left"><i class="fa fa-cutlery base-color"></i></div>-->
                                <!--                                        <div class="media-body">Delicious vegetarian dinner prasad<br></div>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                            </div>
                            <br/>

                        </div>
                    </div>


                </div><!-- row -->
            </div> <!-- #container -->
        </div><!-- #blog-page-wrapper -->
    </section>



    <?php
    include('footer.php');
    ?>


</body>
</html>