<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="//gmpg.org/xfn/11">

    <title>Amazon Smile</title>
    <?php

    $pageDescription = 'Amazon will donate 0.5% of the price of your eligible AmazonSmile purchases to ISKCON of Columbus whenever you shop on AmazonSmile. ';
    $pageKeywords = 'ISKCON Columbus,  Columbus Krishna House, Columbus temple, Hindu Temple in Columbus, Radha Krishna, ISKCON Ohio, Radha Natabara, Amazon smile';
    $pageCanonical = 'https://www.iskconcolumbus.com/amazon-smile.php';

    include("meta_links.php");
    ?>
</head>

<body class="events-template-default single single-events postid-74 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">

    <?php
    include('header.php');
    ?>
    <!-- Blog Heading -->
    <section id="blog-heading">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text">
                    <h1 class="blog-title text-center">Amazon Smile</h1>
                </div><!-- /.heading-text -->
            </div><!-- /.container -->
        </div><!-- /.heading-fullwidth -->
    </section><!-- /#blog-heading -->
    <!-- Blog Heading end -->

    <div class="vc_row-fluid">
        <div class="vc_col-sm-12 wpb_column vc_column_container ">
            <div class="wpb_wrapper">

                <section class="shedule section-padding" style="background: url(images/amazon-header.jpg);">
                    <div class="container" style="padding:15px; background-color: rgba(17, 17, 17, 0.9)">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="wow fadeInLeft" data-wow-delay="0.6s">
                                    <img width="100%" height="100%" src="images/sponsor-festivals-header.jpg"
                                         class="attachment-shop_single size-shop_single wp-post-image"
                                         alt="Radha Natabara" title="Radha Natabara"/>
                                    <div class="thumbnails columns-3">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 wow fadeInRight" data-wow-delay="0.6s">
                                <h2 class="section-title">You Shop. <strong>Amazon Donates.</strong></h2>
                                <a href="https://smile.amazon.com/ch/34-1225440"><h3><i class="fa fa-amazon"></i> Amazon
                                        Smile :)
                                    </h3></a>
                                <div class="section-detail" style="color: white">
                                    <br/><i class="fa fa-hand-o-right"></i> Amazon will donate 0.5% of the price of your
                                    eligible AmazonSmile
                                    purchases to ISKCON of Columbus whenever you shop on AmazonSmile.
                                    <br/><br/><i class="fa fa-hand-o-right"></i>
                                    AmazonSmile is the same Amazon you know. Same products, same prices, same service.
                                    <br/><br/><i class="fa fa-hand-o-right"></i>
                                    Support your charitable organization by starting your shopping at
                                    <strong> smile.amazon.com</strong>
                                    <br/>
                                    <form action="https://smile.amazon.com/ch/34-1225440" target="_blank">
                                        <button type="submit" class="btn custom-btn"
                                                style="color: white; width:60%;border-radius:20px;"
                                                value="Amazon Smile">
                                            Visit Amazon to learn more
                                        </button>
                                    </form>
                                </div>

                            </div>


                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </section>

            </div>
        </div>
    </div>
    <?php
    include('footer.php');
    ?>


</body>
</html>