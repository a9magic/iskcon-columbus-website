<?php include_once("analyticstracking.php") ?>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '553155428389175',
            xfbml      : true,
            version    : 'v2.12'
        });
        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<header id="top-section" style="background: #ffffff">
    <div class="container" style="width: 90%;">
        <div class="col-sm-3 hidden-xs address" style="font-size: 10px">
            <div class="pull-left">
                <ul>
                    <a href="tel:+16144211661">
                        <li><i class="fa fa-phone"></i>+1(614)421-1661</li>
                    </a>
                    <a href="mailto:iskconcolumbus@gmail.com" target="_top">
                        <li><i class="fa fa-envelope-o"></i>iskconcolumbus@gmail.com</li>
                    </a>
                    <a href="https://www.google.com/maps?saddr&daddr=379+West+8th+Ave+Columbus" target="_blank">
                        <li><i class="fa fa-map-marker"></i>379 West 8th Ave Columbus, OH 43201</li>
                    </a>
                </ul>
            </div>
        </div><!-- /.address -->

        <div class="hidden-xs col-sm-6 col-xs-12">
            <div class="logo text-center">
                <a href="/" alt="ISKCON Columbus"/>
                <img src="images/logo-new.jpg">
                </a>
            </div><!-- /.logo -->
        </div>

        <div class=" hidden-xs col-sm-3 social-search pull-right">
            <ul>
                <li><a href="https://www.youtube.com/channel/UCbhrTLxLFZuH-n3Gz1HhJ-w?sub_alertation=1"
                       rel="nofollow" target="_blank"><i class="fa fa-youtube fa-2x"></i><span></span></a></li>
                <li><a href="https://www.facebook.com/KrishnaHouseColumbus" rel="nofollow"
                       target="_blank"><i class="fa fa-facebook fa-2x"></i><span></span></a></li>
                <li><a href="https://www.instagram.com/iskconcolumbus/" rel="nofollow"
                       target="_blank"><i class="fa fa-instagram fa-2x"></i><span></span></a></li>
                <li><a href="https://smile.amazon.com/ch/34-1225440" rel="nofollow"
                       target="_blank"><i class="fa fa-amazon fa-2x"></i><span></span></a></li>
            </ul>
        </div><!-- /.social -->


    </div><!-- /.container -->

    <div class="menu-slider">
        <nav id="peace-menu">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <div style="font-family: 'Garamond'; font-size: 20px;"
                         class="col-xs-9 hidden-lg hidden-md hidden-sm">
                        <a href="/" alt="ISKCON Columbus"/>
                        <img style="height: auto; width:100%;" src="images/logo-new.jpg">
                        <!--                        <strong style="color:#7b0415;">ISKCON Columbus</strong>-->
                        </a>
                    </div><!-- /.logo -->
                    <div class="col-xs-3">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#responsive-icon" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="responsive-icon">
                    <div class="menu-menu-1-container">
                        <ul id="menu-menu-1" class="nav navbar-nav text-center nav navbar-nav text-center"
                            style="text-align: center">

                            <li id="menu-item-105"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-has-children menu-item-105 dropdown">
                                <a title="Home" href="/" class="dropdown-toggle" aria-haspopup="true">Home</a>
                            </li>

                            <li id="menu-item-500"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-500 dropdown">
                                <a title="ISKCON Greater Columbus" href="https://www.iskcongreatercolumbus.com"
                                   target="_blank">New Temple </a>
                            </li>

                            <li id="menu-item-390"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-390 dropdown">
                                <a title="Events" alt="Events" href="events.php" class="dropdown-toggle"
                                   aria-haspopup="true">Events<span> <i
                                                class="fa fa-angle-down hidden-xs hidden-sm"></i></span></a>
                                <ul role="menu" class=" dropdown-menu hidden-xs">
<!--                                    <li id="menu-item-400"-->
<!--                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-406">-->
<!--                                        <a title="Janmashtami 2019" href="janmashtami.php">Janmashtami 2019</a></li>-->
<!--                                    <li id="menu-item-400"-->
<!--                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-406">-->
<!--                                        <a title="Upcoming Events" href="upcoming-events.php">Upcoming Events</a></li>-->
                                    <li id="menu-item-401"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-406">
                                        <a title="Ekadashis in 2020" href="ekadashi.php">Ekadashis in 2020</a></li>
                                    <li id="menu-item-401"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-406">
                                        <a title="Sunday Feast" href="sunday-feast.php">Sunday Feast</a></li>
                                    <li id="menu-item-402"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-406">
                                        <a title="Thursday Program" href="thursday-feast.php">Thursday Program</a></li>
<!--                                    <li id="menu-item-403"-->
<!--                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-406">-->
<!--                                        <a title="Friday Night Harinaam" href="harinaam.php">Friday Night Harinaam</a>-->
<!--                                    </li>-->

                                    <li id="menu-item-405"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-405">
                                        <a title="Home Gathering/Satsang" href="home_satsang.php">Home
                                            Gathering/Satsang</a></li>
                                    <li id="menu-item-406"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-404">
                                        <a title="Maha Satsang" href="maha-satsang.php">Maha Satsang</a></li>
                                    <li id="menu-item-407"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-404">
                                        <a title="Bhakti Yoga" href="https://www.facebook.com/ColumbusBhaktiYoga/"
                                           onclick="alert('You\'ll be redirected to our official Facebook page - Columbus Bhakti Yoga.');"
                                           target="_blank"><i
                                                    class="fa fa-facebook-square"></i>Columbus Bhakti Yoga</a></li>
                                    <li id="menu-item-401"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-406">
                                        <a title="Festivals in 2020" href="festivals.php">Festivals in 2020</a></li>
                                </ul>
                            </li>

                            <li id="menu-item-551"

                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-551 dropdown">
                                <a title="Krishna Catering" href="krishna-catering.php">Krishna Catering</i></a>

                            <li id="menu-item-431"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-431 dropdown">
                                <a title="Services" href="services.php" class="dropdown-toggle" aria-haspopup="true">Services
                                    <i class="fa fa-angle-down hidden-xs hidden-sm "></i></a>
                                <ul role="menu" class=" dropdown-menu hidden-xs">
                                    <li id="menu-item-420"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-428">
                                        <a title="Sunday School" href="sunday-school.php">Sunday School</a></li>
                                    <li id="menu-item-420"
                                        class="menu-item ">
                                        <a title="Daily Skype Satsang" href="daily-skype-satsang.php"><i
                                                    class="fa fa-skype"></i> Daily
                                            Skype Satsang</a></li>
                                    <li id="menu-item-404"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-407"><a
                                                title="University Outreach" href="university-outreach.php">University
                                            Outreach</a>
                                    </li>
                                    <!--                                    <li id="menu-item-421"-->
                                    <!--                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-428">-->
                                    <!--                                        <a title="Govinda's catering" href="govindas_catering.php">Govinda's-->
                                    <!--                                            catering</a>-->
                                    <!--                                    </li>-->
                                    <li id="menu-item-422"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-428">
                                        <a title="Govinda's Bakery"
                                           onclick="alert('You\'ll be redirected to our official Facebook page - 108 Delectable Delights.');"
                                           href="https://www.facebook.com/108delectabledelights/" target="_blank"> <i
                                                    class="fa fa-facebook-square"></i> Govinda's
                                            Bakery</a>
                                    </li>
                                    <li id="menu-item-424"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-429">
                                        <a title="Book Distribution" href="book-distribution.php">Book Distribution</a>
                                    </li>
                                </ul>
                            </li>


                            <li id="menu-item-551"

                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-551 dropdown">
                                <a title="Contact" href="contact.php">Contact</i></a>

                            <li id="menu-item-431"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-431 dropdown">
                                <a title="Donate" class="" href="donate.php" class="dropdown-toggle"
                                   aria-haspopup="true"> Donate <i class="fa fa-angle-down hidden-xs hidden-sm"></i></a>
                                <ul role="menu" class=" dropdown-menu hidden-xs">
                                    <li id="menu-item-434"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-429">
                                        <a title="Donate For New temple"
                                           href="https://www.iskcongreatercolumbus.com/donate.php"
                                           target="_blank">Donate For New temple</a>
                                    </li>
                                    <li id="menu-item-430"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-430">
                                        <a title="Tithe - Support Monthly" href="tithe.php">Tithe - Support Monthly</a>
                                    </li>
                                    <li id="menu-item-431"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-428">
                                        <a title="Sponsor Sunday Feast" href="sponsor-sunday-feast.php">Sponsor Sunday
                                            Feast</a>
                                    </li>
                                    <li id="menu-item-431"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-428">
                                        <a title="Sponsor Festivals" href="sponsor-festivals.php">Sponsor Festivals</a>
                                    </li>
                                    <li id="menu-item-432"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-429">
                                        <a title="Sponsor Book Distribution" href="sponsor-book-distribution.php">Sponsor
                                            Book Distribution</a></li>
                                    <li id="menu-item-433"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-429">
                                        <a title="One Time Donation" href="general-donation.php">One Time Donation</a>
                                    </li>
                                    <li id="menu-item-434"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-429">
                                        <a title="Amazon Smile" href="amazon-smile.php"><i class="fa fa-amazon"></i>
                                            Amazon Smile</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="menu-item-551"

                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-551 dropdown hidden-lg hidden-md hidden-sm">
                                <a href="tel:+16144211661">
                                    <i class="fa fa-phone"></i> Call us +1(614)421-1661
                                </a>
                            </li>
                        </ul>
                    </div>
                </div><!-- /.navbar-collapse -->
            </div><!-- /#peace-menu -->
        </nav>
    </div><!-- /.menu-slider -->
</header>
