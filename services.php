<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="//gmpg.org/xfn/11">
    <title>Services
    </title>
    <?php
    include('meta_links.php');
    ?>
</head>
<body class="page-template page-template-page-template page-template-event-page page-template-page-templateevent-page-php page page-id-399 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- /#preloader -->
<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php
    include('header.php');
    ?>
    <!-- Blog Heading -->
    <section id="blog-heading" style="height:auto;border-bottom:2px solid #e1e1e1;">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text pull-left" style="width:99%;padding-top:41px;">
                    <h1 class="blog-title" style="text-align:center;font-size:31px;">Services
                    </h1>
                    <div class="event-description col-6 text-center" style="padding:11px;">
                        The best way to begin devotional service is to try to offer everything you do, or some portion
                        of everything you do, to Krishna. Devotional service is dependent on the mercy of Krishna and
                        His Devotees.
                    </div>
                </div>
                <!-- /.heading-text -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.heading-fullwidth -->
    </section>
    <!-- /#blog-heading -->
    <!-- Blog Heading end -->
    <section id="blog-page-container">
        <div class="blog-page-wrapper">
            <div id="primary" class="container">
                <main id="main" class="main-content" role="main">

                    <!-- Sunday School -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">18
                        </span>
                                        <br>
                                        <span class="event-month">June
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="sunday-school.php"
                                           title="Sunday School">Sunday School
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-clock-o">
                          </i> 1 Hour
                          <span class="separator">
                          </span>
                          <i class="fa fa-map-marker">
                          </i> 379 West 8th Ave Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="sunday-school.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/sunday-school-thumbnail.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/sunday-school-thumbnail.jpg 570w, images/sunday-school-thumbnail.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Daily Skype Satsang -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                                        <span class="event-date">Daily </span>
                                        <br>
                                        <span class="event-month"> </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="daily-skype-satsang.php"
                                           title="Book Distribution">Daily Skype Satsang
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-skype">
                          </i> Skype id - prashantp2004
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="daily-skype-satsang.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/daily-skype-thumbnail.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/daily-skype-thumbnail.jpg 570w, images/daily-skype-thumbnail.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- University Outreach -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">June
                        </span>
                                        <br>
                                        <span class="event-month">
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="university-outreach.php"
                                           title="University Outreach">University Outreach
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-clock-o">
                          </i> 2 Hours
                          <span class="separator">
                          </span>
                          <i class="fa fa-map-marker">
                          </i> Ohio State University, Columbus
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="university-outreach.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/college-outreach.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/college-outreach.jpg 570w, images/college-outreach.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                        <br/><br/>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Book Distribution -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">June
                        </span>
                                        <br>
                                        <span class="event-month">
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="book-distribution.php"
                                           title="Book Distribution">Book Distribution
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-map-marker">
                          </i> Across Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="book-distribution.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/book-distribution.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/book-distribution.jpg 570w, images/book-distribution.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Govinda's Bakery -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">30
                        </span>
                                        <br>
                                        <span class="event-month">June
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a onclick="alert('You\'ll be redirected to our official Facebook page - 108 Delectable Delights.');" href="https://www.facebook.com/108delectabledelights" target="_blank"
                                           title="Govinda's Bakery">Govinda's Bakery
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-clock-o">
                          </i> 9 am - 5 pm
                          <span class="separator">
                          </span>
                          <i class="fa fa-map-marker">
                          </i> Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a onclick="alert('You\'ll be redirected to our official Facebook page - 108 Delectable Delights.');" href="https://www.facebook.com/108delectabledelights" target="_blank">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/govindas-bakery.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/govindas-bakery.jpg 570w, images/govindas-bakery.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    include('footer.php');
                    ?>

            </div>
            <!-- /.peace layout end -->

