<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <title>Maha Satsang Program
    </title>
    <?php
    $pageDescription = 'Maha Satsang program which happens once a month to lift you spiritually using kirtan, thought provoking talks and japa meditation!';
    $pageKeywords = 'ISKCON Columbus,  Columbus Krishna House, Columbus temple, Hindu Temple in Columbus, Radha Krishna, ISKCON Ohio, Radha Natabara, Maha Satsang';
    $pageCanonical = 'https://www.iskconcolumbus.com/maha-satsang.php';
    include('meta_links.php');
    ?>
</head>
<body class="product-template-default single single-product postid-247 _masterslider _msp_version_3.0.4 woocommerce woocommerce-page wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- /#preloader -->
<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php include('header.php'); ?>

    <!-- Blog Heading -->
    <section id="blog-heading">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text">
                    <h1 class="blog-title text-center"><i class="fa fa-group"></i> Maha <strong>Sat</strong>sang!</h1>
                </div><!-- /.heading-text -->
            </div><!-- /.container -->
        </div><!-- /.heading-fullwidth -->
    </section><!-- /#blog-heading -->
    <!-- Blog Heading end -->

    <!-- Blog Page Container -->
    <section id="blog-page-container">
        <div class="blog-page-wrapper">
            <div class="container">
                <div class="row">


                    <div class="col-md-8 wow fadeInLeft" data-wow-delay="0.4s">
                        <div class="all-event">
                            <div class="event-post">
                                <h2 class="uppercase"><a
                                            href="https://www.facebook.com/events/230774371012535/"
                                            title="ISKCON Columbus - Maha Satsang"> Mind-Friend or Enemy? </a></h2>
                                <div class="event-period">
									<span class="period-session">
										<i class="fa fa-clock-o"></i> 2.5 Hours <span class="separator"></span> <i
                                                class="fa fa-map-marker"></i> 5985 Cara Rd, Dublin, OH 43016 </span><span
                                            class="separator"></span>
                                    <?php include('socialmedia-share.php'); ?><!-- /.comments-share -->
                                </div><!-- /.event-period -->
                                <br/>
                                <div class="feature-img">
                                    <img width="800" height="550"
                                         src="images/mahasatsang.jpg"
                                         class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                         srcset="images/mahasatsang.jpg 800w, images/mahasatsang.jpg 300w"
                                         sizes="(max-width: 800px) 100vw, 800px"/></div>
                                <div class="event-description">
                                    <!--                                    <div class="media">-->
                                    <!--                                        <div class="media-left"><i class="fa fa-align-center base-color"></i></div>-->
                                    <!--                                        <div class="media-body">Tour of New Temple Land at 4:30 pm<br></div>-->
                                    <!--                                    </div>-->
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-music base-color"></i></div>
                                        <div class="media-body">Melodious and ecstatic kirtan<br></div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-bullhorn base-color"></i></div>
                                        <div class="media-body">Topic: Mind-Friend or Enemy? - by His Grace Prem Sindhu
                                            das <br>
                                        </div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-users base-color"></i></div>
                                        <div class="media-body">Engaging children’s activities.<br></div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-heart base-color"></i></div>
                                        <div class="media-body">Mantra meditation<br></div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-newspaper-o base-color"></i></div>
                                        <div class="media-body">Cultural program & Bharat Natyam dance<br></div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-group base-color"></i></div>
                                        <div class="media-body">Stand up aarti and kirtan<br></div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-cutlery base-color"></i></div>
                                        <div class="media-body">Delicious vegetarian dinner prasad<br></div>
                                    </div>
                                </div>
                            </div>
                            <br/>

                        </div>
                    </div>
                    <div class="col-md-4 wow fadeInRight" data-wow-delay="0.4s">
                        <div class="event-location">
                            <h2 class="widget-title">View Location</h2>
                            <div class="eventMap">
                                <!-- ISKCON Greater Columbus -->
<!--                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3055.4031813851793!2d-83.22139708432684!3d40.021776887488!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x883896708f9c2193%3A0xa6e8737ac158414f!2s3520+Walker+Rd%2C+Hilliard%2C+OH+43026!5e0!3m2!1sen!2sus!4v1531426721609"-->
<!--                                        width="100%" height="100%" frameborder="0" style="border:0"-->
<!--                                        allowfullscreen></iframe>-->

                                <!-- Kaltenbach Park Community Hall -->
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3052.916169748859!2d-83.15722468461439!3d40.077281979406024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x883893655ee5b371%3A0xb90507ac85b529fc!2s5985+Cara+Rd%2C+Dublin%2C+OH+43016!5e0!3m2!1sen!2sus!4v1545947202911"
                                        width="100%" height="100%" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>

                            </div>


                        </div>

                        <div class="event-detail">
                            <h2 class="widget-title">Event Details</h2>
                            <div class="address">
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-map-marker base-color"></i></div>
                                    <div class="media-body"><strong>Where:</strong> 35985 Cara Rd, Dublin, OH 43016
                                        <br></div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-calendar-o base-color"></i></div>
                                    <div class="media-body"><strong>When:</strong> January 12, 2019 <strong>5:00
                                            PM</strong><br></div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-clock-o base-color"></i></div>
                                    <div class="media-body"><strong>Who:</strong> Everyone is invited to attend<br>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-money base-color"></i></div>
                                    <div class="media-body"><strong>Cost:</strong> FREE <br></div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-user base-color"></i></div>
                                    <div class="media-body"><strong>What to wear:</strong> Casual<br></div>
                                </div>
                            </div>
                            <div>
                                <form action="https://maps.google.com/maps" method="get" target="_blank">
                                    <input type="text" placeholder="Enter Your Location" name="saddr"/>
                                    <input type="hidden" name="daddr" value="5985 Cara Rd, Dublin, OH 43016"/>
                                    <input type="submit" class="btn custom-btn"
                                           style="color: orange; border-radius:20px;" value="Get directions"/>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!--                    <div class="col-md-12 hidden-xs">-->
                    <!--                        <div class="col-md-4 hidden-xs">-->
                    <!--                            <div class="all-event">-->
                    <!--                                <div class="event-post">-->
                    <!---->
                    <!--                                    <h2 class="uppercase"><a-->
                    <!--                                                href=""-->
                    <!--                                                title="HG Amerandra Das"></a>-->
                    <!--                                    </h2>-->
                    <!---->
                    <!--                                    <div class="feature-img">-->
                    <!--                                        <img width="800" height="550"-->
                    <!--                                             src="images/amerandra.jpg"-->
                    <!--                                             class="attachment-post-thumbnail size-post-thumbnail wp-post-image"-->
                    <!--                                             srcset="images/amerandra.jpg 800w, images/amerandra.jpg 300w"-->
                    <!--                                             sizes="(max-width: 800px) 100vw, 800px"/></div>-->
                    <!--                                </div>-->
                    <!--                                <br/>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div class="col-md-8 hidden-xs">-->
                    <!--                            <div class="event-location">-->
                    <!--                                <h2 class="widget-title">Biography and picture of Amarendra Das </h2>-->
                    <!--                                <div class="eventMap">-->
                    <!--                                    Amarendra prabhu is a diksha disciple of His Holiness Radha Govinda Dasa Goswami-->
                    <!--                                    Maharaj, who is a-->
                    <!--                                    senior disciple of Srila Prabhupada, very well known for his classes on the Srimad-->
                    <!--                                    Bhagavatam. As a-->
                    <!--                                    disciple, Amarendra prabhu aspires to practice and preach the message of the-->
                    <!--                                    scriptures far and wide as-->
                    <!--                                    much as possible and in this endeavour has travelled to many cities around the world-->
                    <!--                                    spreading Krsna-->
                    <!--                                    consciousness in Universities, temple Congregations and social platforms such as-->
                    <!--                                    Youtube, Whatsapp and-->
                    <!--                                    facebook-->
                    <!--                                    As part of his student preaching endeavours, he has also been part of many student-->
                    <!--                                    conferences and has-->
                    <!--                                    contributed in areas such as Interfaith harmony and Science & spiritual synthesis.-->
                    <!--                                    Academically,-->
                    <!--                                    Amarendra Prabhu is a Masters in Electrical Engineering from the University of-->
                    <!--                                    Massachusetts and-->
                    <!--                                    spiritually holds a Bhakti Shastri degree from Mayapur.-->
                    <!---->
                    <!--                                    <br/><br/>-->
                    <!--                                    Amarendra Dasa's website contains many audio and video recordings, articles etc. <a-->
                    <!--                                            href="https://amarendradasa.com">https://amarendradasa.com</a>-->
                    <!---->
                    <!--                                    <br/><br/>-->
                    <!--                                    Amarendra Dasa can be contacted at <a href="mailto:Arvindbsds@gmail.com">Arvindbsds@gmail.com</a>-->
                    <!--                                    for scheduling lectures and seminars-->
                    <!---->
                    <!--                                </div>-->
                    <!---->
                    <!---->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                </div><!-- row -->
            </div> <!-- #container -->
        </div><!-- #blog-page-wrapper -->

    </section>

    <div class="col-md-12 wow fadeInUp text-center" data-wow-delay="0.4s"
         style="padding: 15px;">
        <strong>To RSVP</strong> please call / text us on <i class="fa fa-phone-square"></i> <a
                href="tel:727-804-9835"> 727-804-9835 </a> / <a href="tel:727-804-9836">
            727-804-9836</a> or email us at
        <a style="color: #0077b3"
           href="mailto:spiritualyoga108@gmail.com?Subject=Maha%20Satsang" target="_top">spiritualyoga108@gmail.com</a>
        to let us know so that we can prepare dinner prasad
        accordingly. We sure hope that you can attend and please invite your FAMILIES, FRIENDS
        AND COLLEAGUES TOO!
    </div>

</div>
<?php
include('footer.php');
?>
</body>
</html>
