<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Krishna Catering</title>
    <meta name="description" content="Krishna catering - Pure vegetarian (without onion & garlic) food prepared by devotees of, and offered to, Sri Sri Radha-Krishna at ISKCON Columbus">
    <meta name="keywords" content="Krishna Catering, Vegetarian food, Jain Dishes, Best Indian Food, ISKCON prashad, ISKCON Columbus">
    <title>Krishna Catering</title>
    <?php
    include('meta_links.php');
    ?>
</head>

<body class="events-template-default single single-events postid-74 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">

    <?php
    include('header.php');
    ?>
    <!-- Blog Heading -->
    <?php
    include('krishna-catering.html');
    ?>
    <!-- Blog Heading end -->

    <?php
    include('footer.php');
    ?>


</body>
</html>