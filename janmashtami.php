<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <title>Janmashtami 2019
    </title>
    <?php
    $pageDescription = 'Sri Krishna Janmastami 2019, the divine appearance of Lord Krishna on Friday August 23rd 2019. 
                        There will be a block party starting 6 PM, Abhishek, Krishna katha, Krishna dance, Midnight darshan';
    $pageKeywords = 'Janmashtami, Janmashtami 2019, Krishna Janmashtami, Lord Krishna appearance day, Gods birth, ISKCON Columbus,  
                    Columbus Krishna House, Columbus temple, Hindu Temple in Columbus, Radha Krishna, ISKCON Ohio, Radha Natabara';
    $pageCanonical = 'https://www.iskconcolumbus.com/maha-satsang.php';
    include('meta_links.php');
    ?>
</head>
<body class="product-template-default single single-product postid-247 _masterslider _msp_version_3.0.4 woocommerce woocommerce-page wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- /#preloader -->
<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php include('header.php'); ?>

    <br/>
    <!-- Blog Heading -->
    <section id="blog-heading">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text">
                    <h1 class="blog-title text-center"><i class="fa fa-group"></i> <strong>Janmashtami</strong></h1>
                </div><!-- /.heading-text -->
            </div><!-- /.container -->
        </div><!-- /.heading-fullwidth -->
    </section><!-- /#blog-heading -->
    <!-- Blog Heading end -->

    <!-- Blog Page Container -->
    <section id="blog-page-container">
        <div class="blog-page-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="all-event">
                            <div class="event-post">

                                <div class="full-width">
                                    <div class="vc_row-fluid">
                                        <div class="vc_col-sm-12 wpb_column vc_column_container ">
                                            <div class="wpb_wrapper">


                                                <div class="wow fadeIn" data-wow-delay="0.4s"
                                                     style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp width: 100%;">
                                                    <img
                                                            src="images/janmashtami/janmasthami.jpg"
                                                            alt=""
                                                            title=""
                                                            style="height: 80%; width: 100%; margin-top: 0px; margin-left: -0.5px;">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <br/>

                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="all-event">
                            <div style="text-align: center" class="event-post">

                                <a href="https://www.facebook.com/events/466821900547774"
                                   target="_blank"
                                   title="Sri Krishna Janmasthami ">
                                    <img style="width: 30%;" src="images/fb-event.png">
                                </a>
                                <br/> <br/>

                                <h2 class="widget-title">The Divine Appearance Day Anniversary of Lord Krishna</h2>
                                <hr>
                                <div style="text-align: left; font-size: larger">
                                    <h4>
                                        It is that time of year again when the ringing bells of ISKCON Columbus announce
                                        the
                                        divine birthday celebration of Lord Krishna. Follow the sound to the grandest
                                        2019
                                        Sri Krishna Janmashtami festival!
                                    </h4> <br/>
                                    <h4>

                                        It is a time to reflect on Krishna’s love and mercy and deepen your relationship
                                        with Him. Join us for days of reflection, prayer, worship, spiritual
                                        discussions,
                                        chanting of Krishna’s names, yoga, fun activities, and A BLOCK PARTY that hosts
                                        other
                                        engagements which will connect you with the Lord and celebrate His appearance in
                                        our
                                        lives.
                                    </h4>
                                    <br/>
                                    <strong>Friday - August 23rd, 2019</strong>
                                    <br/>
                                    <h4>
                                        The actual appearance day anniversary of the Lord brings you an experience you
                                        will
                                        never forget. Bring your family and friends to enjoy enacted pastimes of Lord
                                        Krishna around the lake (the Janmashtami Journey), benefit from chanting
                                        Krishna’s holy names, and relish a sumptuous evening dinner. Be enamoured by the
                                        Lords Sri Sri Radha Natabara at
                                        <br>
                                        <i class="fa fa-clock-o base-color"></i>
                                        a special midnight arati in their splendid new outfit.
                                        <br/>
                                        <i class="fa fa-music base-color"></i>
                                        Participate in ecstatic kirtans and bhajans (musical mantra meditation) to
                                        invoke auspiciousness and Lord Krishna’s sublime blessings
                                        <br/>
                                        <i class="fa fa-adjust base-color"></i>
                                        a Kalash Abhishek,
                                        bathing
                                        the
                                        Deities with sacred liquids.
                                    </h4>
                                </div>
                            </div>
                            <br/>
                            <hr>


                            <!-- Janmashtami sponsorship GIF-->
                            <div class="feature-img">
                                <img width="800" height="550"
                                     src="images/janmashtami/janmashtami-sponsorship.gif"
                                     class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                     srcset="images/janmashtami/janmashtami-sponsorship.gif 800w, images/janmashtami/janmashtami-sponsorship.gif 300w"
                                     sizes="(max-width: 800px) 100vw, 800px"/></div>

                            <!-- Sponsorship details-->
                            <div class="col-md-12">
                                <h2 style="text-align: center">
                                    Serve Sri Sri Radha Natabar
                                    <br/>
                                    Jagannath-Baladev-Subhadra
                                    <br/>
                                    Sri Sri Gaura Nitai
                                    <br/>
                                </h2>

                                <br/>
                                <h3 style="text-align: center">Event level sponsorship</h3>
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td style="color: #960011;">Sponsor full Janmashtami event
                                        </td>
                                        <td style="color: #12009a;">$20,001 USD</td>
                                    </tr>
                                    <tr>
                                        <td style="color: #960011;">Sponsor Deity Services and Decorations
                                        </td>
                                        <td style="color: #12009a;">$7,501 USD</td>
                                    </tr>
                                    <tr>
                                        <td style="color: #960011;">Sponsor Block party and all booths</td>
                                        <td style="color: #12009a;">$7,501 USD</td>
                                    </tr>
                                    <tr>
                                        <td style="color: #960011;">Sponsor Maha Prasadam</td>
                                        <td style="color: #12009a;">$3,001 USD</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!-- JANMASHTAMI EVENT LEVEL SPONSORSHIP PAYPAL BUTTONS-->
                                <div style="text-align: center">
                                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
                                        <input type="hidden" name="cmd" value="_s-xclick">
                                        <input type="hidden" name="hosted_button_id" value="ZR3V7AMF8ZJ8U">
                                        <table>
                                            <tr><td><input type="hidden" name="on0" value="Janmashtami">Janmashtami</td></tr><tr><td><select name="os0">
                                                        <option value="Sponsor entire Janmashtami event">Sponsor entire Janmashtami event $20,001.00 USD</option>
                                                        <option value="Sponsor Deity Services and Decorations">Sponsor Deity Services and Decorations $7,501.00 USD</option>
                                                        <option value="Sponsor Block party and all booths">Sponsor Block party and all booths $7,501.00 USD</option>
                                                        <option value="Sponsor Maha Prasadam">Sponsor Maha Prasadam $3,001.00 USD</option>
                                                    </select> </td></tr>
                                            <tr><td><input type="hidden" name="on1" value="Name & phone number">Name & phone number</td></tr><tr><td><input type="text" name="os1" maxlength="200"></td></tr>
                                        </table>
                                        <input type="hidden" name="currency_code" value="USD">
                                        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                    </form>
                                </div>

                                <br/>
                                <h3 style="text-align: center">Individual item sponsorship</h3>
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td style="color: #960011;">Maha Prasadam
                                        </td>
                                        <td style="color: #12009a;">$3,001 USD</td>
                                    </tr>
                                    <tr>
                                        <td style="color: #960011;">New outfits and jewelry for The Deities
                                        </td>
                                        <td style="color: #12009a;">$2,501 USD</td>
                                    </tr>
                                    <tr>
                                        <td style="color: #960011;">Kalash Maha-Abhishek Paraphernalia
                                        </td>
                                        <td style="color: #12009a;">$2,501 USD</td>
                                    </tr>
                                    <tr>
                                        <td style="color: #960011;">Altar decorations</td>
                                        <td style="color: #12009a;">$2,001 USD</td>
                                    </tr>
                                    <tr>
                                        <td style="color: #960011;">Temple decorations</td>
                                        <td style="color: #12009a;">$1,008 USD</td>
                                    </tr>
                                    <tr>
                                        <td style="color: #960011;">Block party setup</td>
                                        <td style="color: #12009a;">$7,501 USD</td>
                                    </tr>
                                    <tr>
                                        <td style="color: #960011;">Flower Garlands for The Deities</td>
                                        <td style="color: #12009a;">$501 USD</td>
                                    </tr>
                                    <tr>
                                        <td style="color: #960011;">Flower vases</td>
                                        <td style="color: #12009a;">$501 USD</td>
                                    </tr>
                                    <tr>
                                        <td style="color: #960011;">Kids camp and bounce house</td>
                                        <td style="color: #12009a;">$750 USD</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!-- JANMASHTAMI Individual LEVEL SPONSORSHIP PAYPAL BUTTONS-->
                                <div style="text-align: center">
                                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
                                        <input type="hidden" name="cmd" value="_s-xclick">
                                        <input type="hidden" name="hosted_button_id" value="N9PZ4UWB5P5KQ">
                                        <table>
                                            <tr><td><input type="hidden" name="on0" value="Janmashtami-individual">Janmashtami-individual</td></tr><tr><td><select name="os0">
                                                        <option value="Maha Prasadam">Maha Prasadam $3,001.00 USD</option>
                                                        <option value="New outfits and jewelry for The Deities">New outfits and jewelry for The Deities $2,501.00 USD</option>
                                                        <option value="Kalash Maha-Abhishek Paraphernalia">Kalash Maha-Abhishek Paraphernalia $2,501.00 USD</option>
                                                        <option value="Altar decorations">Altar decorations $2,001.00 USD</option>
                                                        <option value="Temple decorations">Temple decorations $1,008.00 USD</option>
                                                        <option value="Block party setup">Block party setup $7,501.00 USD</option>
                                                        <option value="Flower Garlands for The Deities">Flower Garlands for The Deities $501.00 USD</option>
                                                        <option value="Flower vases">Flower vases $501.00 USD</option>
                                                        <option value="Kids camp and bounce house">Kids camp and bounce house $750.00 USD</option>
                                                    </select> </td></tr>
                                            <tr><td><input type="hidden" name="on1" value="Name & phone number">Name & phone number</td></tr><tr><td><input type="text" name="os1" maxlength="200"></td></tr>
                                        </table>
                                        <input type="hidden" name="currency_code" value="USD">
                                        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                    </form>
                                </div>



                            </div>




                        </div>

                        <!-- Janmashtami 2017 album-->
                        <div class='embedsocial-album' data-ref="2ffd9554151a7d3eeaa9bb5fabd8824fac1d8198"></div>
                        <script>(function (d, s, id) {
                                var js;
                                if (d.getElementById(id)) {
                                    return;
                                }
                                js = d.createElement(s);
                                js.id = id;
                                js.src = "https://embedsocial.com/embedscript/ei.js";
                                d.getElementsByTagName("head")[0].appendChild(js);
                            }(document, "script", "EmbedSocialScript"));</script>

                    </div>
                    <div class="col-md-4">
                        <div class="event-location">
                            <h2 class="widget-title">View Location</h2>
                            <div class="eventMap">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3056.7323308791683!2d-83.01912548461696!3d39.99208667941676!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88388eea31eb70cd%3A0xc3081bd6d7b6ddff!2s379+W+8th+Ave%2C+Columbus%2C+OH+43201!5e0!3m2!1sen!2sus!4v1502756333422"
                                        width="100%" height="100%" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>
                            </div>


                        </div>
                        <div>
                            <form action="https://maps.google.com/maps" method="get"
                                  target="_blank">
                                <input type="text" placeholder="Enter Your Location"
                                       name="saddr"/>
                                <input type="hidden" name="daddr"
                                       value="379 W 8th Ave, Columbus, Ohio 43201"/>
                                <input type="submit" class="btn custom-btn"
                                       style="color: #ff7600; border-radius:20px;"
                                       value="Get directions"/>
                            </form>
                        </div>
                        <h2 class="widget-title">Parking Details</h2>
                        <div class="feature-img">
                            <img width="800" height="550"
                                 src="images/janmasthami-parking.jpg"
                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                 srcset="images/janmasthami-parking.jpg 800w, images/janmasthami-parking.jpg 300w"
                                 sizes="(max-width: 800px) 100vw, 800px"/></div>
                        <div class="event-detail">
                            <h2 class="widget-title">Event Details</h2>
                            <div class="address">
                                <div class="media">
                                    <div class="media-left"><i
                                                class="fa fa-map-marker base-color"></i></div>
                                    <div class="media-body"><strong>Where:</strong> ISKCON Columbus
                                        <br/>
                                        379 W 8th Ave, Columbus, Ohio 43201
                                        <br></div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-clock-o base-color"></i>
                                    </div>
                                    <div class="media-body"><strong>Who:</strong> Everyone is
                                        invited to attend<br>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-money base-color"></i>
                                    </div>
                                    <div class="media-body"><strong>Cost:</strong> FREE <br></div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i
                                                class="fa fa-calendar-o base-color"></i></div>
                                    <div class="media-body"><strong>When:</strong> <br/>

                                        <i class="fa fa-clock-o"></i><strong> Morning
                                            programs:</strong><br/>
                                        <strong> 05:00 AM - 05:45 AM: </strong> Mangal aarti<br/>
                                        <strong> 05:45 AM - 07:15 AM: </strong> Group Japa<br/>
                                        <strong> 07:15 AM - 07:30 AM: </strong> Guru puja<br/>
                                        <strong> 07:30 AM - 07:45 AM: </strong> Sringar darsan<br/>

                                        <br/>
                                        <i class="fa fa-clock-o"></i><strong> Evening
                                            programs:</strong><br/><br/>

                                        Temple room:<br/>

                                        <strong> 06:30 PM - 07:30 PM: </strong> Bhajans<br/>
                                        <strong> 07:30 PM - 09:30 PM: </strong> Abhishek
                                        ceremony<br/>
                                        <strong> 09:30 PM - 12:00 AM: </strong> Bhajans (darshan
                                        closed during this time)<br/>
                                        <strong> 12:00 AM - 12:30 AM: </strong> Special darshan,
                                        Aarti and Kirtan<br/><br/>

                                        Outsie Temple - Block party:<br/>
                                        <strong>06:30 pm – 06:45 pm:</strong> Hare Krishna bhajans
                                        by ISKCON Columbus youth<br/>
                                        <strong>06:45 pm – 06:50 pm:</strong> Sloka recitation by
                                        Ujwala Nitai das<br/>
                                        <strong>06.50 pm – 07 pm:</strong> “Dasavatara”
                                        recitation by Pariksith Pothamsetty<br/>
                                        <strong>07 pm – 07.05 pm:</strong> Classical dance
                                        performance by Anaya Jaiswal<br/>
                                        <strong>07:00 pm – 07:15 pm:</strong> Classical dance
                                        performance
                                        by Kaustavi<br/>
                                        <strong>07:15 pm – 07:25 pm:</strong> Flute by Torin<br/>
                                        <strong>07:30 pm – 08:00 pm:</strong> “Pastimes of Lord
                                        Krishna” by Mother Krishna Nandini<br/>
                                        <strong>08:00 pm – 08:45 pm:</strong> Yakshagana
                                        Performance<br/>
                                        <strong>08:45 pm – 09:00 pm:</strong> “Krishna
                                        Lila Stava” performance by Sunday
                                        school kids<br/>
                                        <strong>09:10 pm – 09:20 pm:</strong>
                                        Classical dance performance by
                                        Ishika Jaryal<br/>
                                        <strong>09:20 pm – 09:50 pm:</strong>
                                        “Pastimes of Lord Krishna”
                                        by Mother Malati<br/>
                                        <strong>09:50 pm – 10:05 pm:</strong>
                                        Hare Krishna! Film
                                        trailer<br/>
                                        <strong>10:10 pm – 10:20 pm:</strong>
                                        Classical dance
                                        performance by Roshni
                                        Datta<br/>
                                        <strong>10:20 pm – 10:50 pm:</strong>
                                        Hare Krishna bhajans<br/>
                                        <strong>10:50 pm – 11:30 pm:</strong>
                                        “Lord Krishna’s
                                        appearance” by Prem
                                        Vilas das<br/>
                                        <strong>12.30 AM -</strong> Special
                                        maha-prasadam serve out<br/><br/>


                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <br/>

                </div><!-- row -->
            </div> <!-- #container -->
        </div><!-- #blog-page-wrapper -->

    </section>

    <div class="col-md-12 wow fadeInUp text-center" data-wow-delay="0.4s"
         style="padding: 15px;">
        <strong>For any inquiries</strong> please call / text us on <i class="fa fa-phone-square"></i> <a
                href="tel:727-804-9835"> 727-804-9835 </a> / <a href="tel:727-804-9836">
            727-804-9836</a> or email us at
        <a style="color: #0077b3"
           href="mailto:iskconcolumbus@gmail.com?Subject=ISKCON%20Columbus%20Janmashtami%202019" target="_top">iskconcolumbus@gmail.com</a>
        . We sure hope that you can attend and please invite your FAMILIES, FRIENDS
        AND COLLEAGUES TOO!
    </div>

</div>
<?php
include('footer.php');
?>
</body>
</html>
