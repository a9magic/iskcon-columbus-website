<?php include"includes/header.php"; ?>

<div class="donate-container container">	
	<h1>Donate</h1>
	<hr>
	<div class="col-sm-12">
		<h4>Be Apart of History</h4>
		<br>
		<div class="">
			<b>Make a Pledge</b><br><br>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
			<br>
		</div>
	</div>
	<div class="col-sm-6 ">
		<div class="text-center rightside">
			<b>Make a Pledge</b><br><br><p>Dharma Rakshak</p><br><br>
			<h1>$15000</h1>
			<hr>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
			<br>
			<button class="btn btn-sm donate-btn" data-toggle="modal" data-target="#firstform">Donation Pledge Form</button>
			<button class="btn btn-sm donate-btn" data-toggle="modal" data-target="#secondform">Direct Debit Collection Form</button>
		</div>
	</div>
	<div class="col-sm-6 table-responsive donation-table" data-toggle="modal" data-target="#firstform" style="cursor:pointer">
		<h3 class="text-center makewhite">Donation plans</h3><br>
		<p class="text-center makewhite">What you can do to help</p><br><br>
		<table class="table">
			<tr class="table-head">
				<th>Donation Level</th>
				<th>Donation Amount</th>
			</tr>
			<tr onclick="selectbox('kesava')">
				<td>Kesava</td>
				<td>$500,000</td>
			</tr>
			<tr onclick="selectbox('narayana')">
				<td>Narayana</td>
				<td>$250,000</td>
			</tr>
			<tr onclick="selectbox('madhava')">
				<td>Madhava</td>
				<td>$100,000</td>
			</tr>
			<tr onclick="selectbox('govinda')">
				<td>Govinda</td>
				<td>$40,000</td>
			</tr>
			<tr onclick="selectbox('vishnu')">
				<td>Vishnu</td>
				<td>$20,000</td>
			</tr>
			<tr onclick="selectbox('madhusudana')">
				<td>Madhusudana</td>
				<td>$10,000</td>
			</tr>
			<tr onclick="selectbox('trivikrama')">
				<td>Trivikrama</td>
				<td>$5,000</td>
			</tr>
			<tr onclick="selectbox('vamana')">
				<td>Vamana</td>
				<td>$2,500</td>
			</tr>
			<tr onclick="selectbox('sridhara')">
				<td>Sridhara</td>
				<td>$1,008</td>
			</tr>
			<tr onclick="selectbox('hrslkesa')">
				<td>Hrslkesa</td>
				<td>$500 [2 sq ft]</td>
			</tr>
			<tr onclick="selectbox('padmanabha')">
				<td>Padmanabha</td>
				<td>$250 [1 sq ft]</td>
			</tr>
		</table>
	</div>
	<div id="main" class="container">
		<div class="donate">
			<h1>Donate Now</h1>
			<hr>
			<!-- 1st charity container -->
	        <div class="col-sm-4 col-sm-offset-1 charity">
	            <a href=""><img src="images/ISKCON1.png"></a>
	            <!--<form method="post" action="a/process.php?paypal=checkout">
					<input type="hidden" name="itemname" value="Donation" /> 
					<input type="hidden" name="itemnumber" value="10000" /> 
				    <input type="hidden" name="itemdesc" value="Donate amount to iskcon temple" /> 
					<input type="hidden" name="itemprice" value="1008" />
					<input type="hidden" name="itemQty" value="1" />
				    <input type="submit" name="submitbutt" value="Donate $1008" />
			    </form>-->
				<form class="paypal" action="#" method="post" id="paypal_form">
					
					<input type="submit" name="submit" value="Donate $1008"/>
				</form>
	            <p>Or give <a href="#" onclick="show('<?php echo base64_encode(1); ?>');" class="show2">any amount</a>.</p>
	        </div>
	        <!-- 2nd charity container -->
	         <div class="col-sm-2 ">
	         </div>
	        <div class="col-sm-4 charity">
	            <a href=""><img src="images/ISKCON1.png"></a>
	           <!--  <form action="process.php" method="POST">
	                <input type="hidden" name="id" value="<?php echo base64_encode(2); ?>">
	                <input type="submit" value="Donate $15000" name="submit">
	            </form> -->
	            <!--<form method="post" action="a/process.php?paypal=checkout">
					<input type="hidden" name="itemname" value="Donation" /> 
					<input type="hidden" name="itemnumber" value="10000" /> 
				    <input type="hidden" name="itemdesc" value="Donate amount to iskcon temple" /> 
					<input type="hidden" name="itemprice" value="15000" />
					<input type="hidden" name="itemQty" value="1" />
				    <input type="submit" name="submitbutt" value="Donate $15000" />
			    </form>-->
				<form class="paypal" action="#" method="post" id="paypal_form">
					
					<input type="submit" name="submit" value="Donate $15000"/>
				</form>
	            <p>Or give <a href="#" onclick="show('<?php echo base64_encode(2); ?>');">any amount</a>.</p>
	        </div>
	    </div>

	    <div id="pop2" class="simplePopup">
	        <h3>Donate and start helping today!</h3>
	        <img src="images/donate.jpg">
                <br/>
	        <form class="paypal" action="#" method="post" id="paypal_form">
				<input type="text" name="itemprice" value="" />
				<input type="submit" name="submit" class="btn btn-sm donate-btn" value="Submit Payment"/>
			</form>
	    </div>
	</div>
</div>
</div>

<?php include"includes/footer.php"; ?>
<script src="js/jquery.simplePopup.js" type="text/javascript"></script>
        <!-- call popup -->
        <script type="text/javascript">
            function show(id) {
                $('#charity_id').val(id);
                $('.box-shadow-preview').css("opacity", "0.1");
                $('#pop2').simplePopup();
            }
		</script>
		
<div class="modal fade" id="firstform" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Donation Pledge Form</h4>
			</div>
			<div class="modal-body">
				<form method="post" action="donate.php">
					<div class="alert alert-danger" id="err" style="display:none">
					  Please fill all required filds
					</div>
				  <div class="form-group">
					<label for="exampleInputEmail1">Donor Name <span class="red">*</span></label>
					<input type="text" class="form-control" id="name" name="name" placeholder="Your Answer">
				  </div>
				  <div class="form-group">
					<label>Donation Opportunities <span class="red">*</span></label>
					<p>View Spiritual Benefits in below image</p>
					<select class="form-control" name="opportunities" id="opportunities">
						<option value="">Choose</option>
						<option value="kesava" >Kesava : $500,000 ($13,888.00 USD monthly for 36 months)</option>
						<option value="narayana">Narayana : $6,944.00 USD - monthly</option>
						<option value="madhava">Madhava : $2,777.00 USD - monthly</option>
						<option value="govinda">Govinda : $1,111.00 USD - monthly</option>
						<option value="vishnu">Vishnu : $566.00 USD - monthly</option>
						<option value="madhusudana">Madhusudana : $278.00 USD - monthly</option>
						<option value="trivikrama"> Trivikrama : $139.00 USD - monthly</option>
						<option value="vamana">Vamana : $70.00 USD - monthly</option>
						<option value="sridhara">Sridhara : $28.00 USD - monthly</option>
						<option value="hrisikesa">Hrisikesa : $500 USD two sq ft</option>
						<option value="padmanabha">Padmanabha : $250 USD one time donation</option>
						<option value="other">other</option>
					</select>
				  </div>
				  <div class="form-group">
					<label>Other</label>
					<p>Enter amount</p>
					<input type="text" class="form-control" id="other" name="other" placeholder="Your Answer">
				  </div>
				  <div class="form-group">
					<label>Number of Payments <span class="red">*</span></label>
					<p>You can select an appropriate option to opt to pay once or in 3 years or in 2 year. Refer to the details below to see the payment amounts based on your selection.</p>
					<select class="form-control" name="noOfpayment" id="noOfpayment">
						<option valuee="">Choose</option>
						<option valuee="1">One time payment</option>	
						<option valuee="12">12 monthly</option>
						<option valuee="24">24 monthly</option>
						<option valuee="36">36 monthly</option>
					</select>
				  </div>
				  <div class="form-group">
					<label>Bank Account Holder Name <span class="red">*</span> </label>
					<input type="text" class="form-control" placeholder="Your Answer" name="holdername" id="holdername">
				  </div>
				  <div class="form-group">
					<label>Bank Routing Number <span class="red">*</span></label>
					<p>ABA routing number (Refer to sample image of check below to identify your Bank Routing number.)</p>
					<input type="text" class="form-control" placeholder="Your Answer" name="routingnumber" id="routingnumber">
				  </div>
				  <div class="form-group">
					<label>Bank Account Number <span class="red">*</span></label>
					<p>Refer to sample image of check below to identify your Bank Account number.</p>
					<input type="text" class="form-control" placeholder="Your Answer" id="accountnumber" name="accountnumber">
				  </div>
				  <div class="form-group">
					<label>Where do I get Routing Number, Account Number details ? </label>
					<img src="images/check.gif" style="height:180px">
				  </div>
				  <div class="form-group">
					<label>Start date <span class="red">*</span></label>
					<p>The date to start ACH debit</p>
					<input type="date" class="form-control" placeholder="Your Answer" id="startdate" name="startdate">
				  </div>
				  <div class="form-group">
					<label>Phone number <span class="red">*</span></label>
					<input type="text" class="form-control" placeholder="Your Answer" id="phone" name="phone">
				  </div>
				  <div class="form-group">
					<label>Email <span class="red">*</span></label>
					<input type="text" class="form-control" placeholder="Your Answer" id="email" name="email">
				  </div>
				  <div class="form-group">
					<label>Mailing Address <span class="red">*</span></label>
					<input type="text" class="form-control" placeholder="Your Answer" id="address" name="address">
				  </div>
				  <div class="form-group">
					<label>Donor Signature. I hereby authorize India Heritage Foundation to ACH debit my donation pledge amount in installments from my bank account as per my selection above. <span class="red">*</span></label>
					<p>Please enter your full legal name. The entry below will act as your Digital Signature.</p>
					<input type="text" class="form-control" placeholder="Your Answer" id="sign" name="sign">
				  </div>
				  <button type="submit" name="form1" class="btn btn-success" onclick="return val(); " >Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="secondform" role="dialog" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Direct Debit Collection Form</h4>
			</div>
			<div class="modal-body">
				<p>This form authorizes ISKCON Columbus (iskconcolumbus.com) to withdraw an amount specified by you from your checking or savings account, on a periodic basis. Using ACH mode to deduct money from your Checking a/c will save 2.5% fees charged by financial institutions like Paypal/Credit Cards/Square. You can override previous authorizations any time, by submitting a new entry form, with your modifications specified below.</p>
				<br>
				<form method="post">
					<div class="alert alert-danger" id="err2" style="display:none">
					  Please fill all required filds
					</div>
				  <div class="form-group">
					<label for="exampleInputEmail1">First Name <span class="red">*</span></label>
					<p>Exactly as in your bank account</p>
					<input type="text" class="form-control" id="firstname" name="firstname" placeholder="Your Answer">
				  </div>
				  <div class="form-group">
					<label>Last Name </label>
					<p>Exactly as in your bank account</p>
					<input type="text" class="form-control" id="lastname" name="lastname" placeholder="Your Answer">
				  </div>
				  <div class="form-group">
					<label>Amount <span class="red">*</span></label>
					<p>Enter amount of dollars to be deducted every period.</p>
					<input type="text" class="form-control" placeholder="Your Answer" id="amounts" name="amounts">
				  </div>
				  <div class="form-group">
					<label>Donation Towards </label>
					<p>Please specify which department you would like to apply your donation.</p>
					<ul style="list-style:none">
						<li><input type="checkbox" name="donationtowards[]" value="columbus"> Sri Sri Radha Natabara, ISKCON Columbus</li>
						<li><input type="checkbox" name="donationtowards[]" value="monthly"> Monthly Tithing</li>
						<li><input type="checkbox" name="donationtowards[]" value="sunday"> Sunday Feast</li>
						<li><input type="checkbox" name="donationtowards[]" value="other"> Other</li>
					</ul>
				  </div>
				  <div class="form-group">
					<label>Where do I get Routing Number, Account Number details ? </label>
					<img src="images/code.jpg" style="height:180px">
				  </div>
				  <div class="form-group">
					<label>Bank Routing Number <span class="red">*</span></label>
					<p>ABA routing number (the first 9 digits on your bottom line of your check)</p>
					<input type="text" class="form-control" name="bankroutingnumber" id="bankroutingnumber" placeholder="Your Answer">
				  </div>
				  <div class="form-group">
					<label>Bank Account Number <span class="red">*</span></label>
					<p>The digits following the routing number.</p>
					<input type="text" class="form-control" placeholder="Your Answer" name="bankaccountnumber" id="bankaccountnumber">
				  </div>
				  
				  <div class="form-group">
					<label>Start date <span class="red">*</span></label>
					<p>Default is immediately start on submission.</p>
					<input type="date" class="form-control" placeholder="Your Answer" name="startdate" id="startdate2">
				  </div>
				  <div class="form-group">
					<label>Frequency of donation <span class="red">*</span></label>
					<p>Default is monthly deduction from your account. Please select one of below option if you don't prefer monthly deductions.</p>
					<select class="form-control" name="frequencyofdonation" id="frequencyofdonation">
						<option>Choose</option>
					</select>
				  </div>
				  <div class="form-group">
					<label>Start date <span class="red">*</span></label>
					<p>Please stop my deduction effective date....(leave it empty if you are not sure now, you can send email to iskconcolumbus@gmail anytime)</p>
					<input type="date" class="form-control" id="start" name="start" placeholder="Your Answer">
				  </div>
				  <div class="form-group">
					<label>Email <span class="red">*</span></label>
					<input type="text" class="form-control" name="email" id="email2" placeholder="Your Answer">
				  </div>
				  <div class="form-group">
					<label>Street Address <span class="red">*</span></label>
					<p>Address 1</p>
					<input type="text" class="form-control" name="streetaddress" id="streetaddress" placeholder="Your Answer">
				  </div>
				  <div class="form-group">
					<label>Apt/Unit Number <span class="red">*</span></label>
					<p>Address 2</p>
					<input type="text" class="form-control" name="apt" id="apt" placeholder="Your Answer">
				  </div>
				  <div class="form-group">
					<label>City <span class="red">*</span></label>
					<input type="text" class="form-control" name="city" id="city" placeholder="Your Answer">
				  </div>
				  <div class="form-group">
					<label>State <span class="red">*</span></label>
					<input type="text" class="form-control" name="state" id="state" placeholder="Your Answer">
				  </div>
				  <div class="form-group">
					<label>Zip <span class="red">*</span></label>
					<input type="text" class="form-control" name="zip" id="zip2" placeholder="Your Answer">
				  </div>
				  <div class="form-group">
					<label>Phone number <span class="red">*</span></label>
					<input type="text" class="form-control" name="phone" id="phone2" placeholder="Your Answer">
				  </div>
				  <div class="form-group">
					<label>I hearby authorize ISKCON Columbus, 379 W 8th Ave, Columbus, OH 43201 to initiate a debit entry to my checking or savings account at the financial institution name above. I acknowledge that the origination of ACH transactions to my account must comply with the provisions of U.S.law. I understand I can cancel this authorization any time, by sending an email to iskconcolumbus@gmail.com <span class="red">*</span></label>
					<p>Please enter your full legal name. The entry below will act as your Digital Signature.</p>
					<input type="text" class="form-control" name="sign" id="sign2" placeholder="Your Answer">
				  </div>
				  <h3>Hare Krishna Hare Krishna Krishna Krishna Hare Hare || Hare Rama Hare Rama Rama Rama Hare Hare</h3>
				  <br>
				  <button type="submit" class="btn center-block btn-success" onclick="return val2();" >Submit</button>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" role="dialog" id="thank">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<h2>Thank you!</h2>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
.modal-content{
	border-radius: 0px !important;
}
.modal-header{
	background: #FFBC01;
	color: #fff;
	border-color: #FFBC01;
	font-weight: bold !important;
}
.modal-body{
	padding: 40px;
}
.modal-body .form-group{
	margin-bottom: 25px;
}
</style>

<script type="text/javascript">
	function val()
	{
		flag = true;
		var name = $('#name').val();
		var opportunities = $('#opportunities').val();
		var noOfpayment = $('#noOfpayment').val();
		var holdername = $('#holdername').val();
		var routingnumber = $('#routingnumber').val();
		var accountnumber = $('#accountnumber').val();
		var startdate = $('#startdate').val();
		var email = $('#email').val();
		var address = $('#address').val();
		var sign = $('#sign').val();

		if(name == '')
		{
			$('#err').show();
			flag = false; 
		}
		if(opportunities == '')
		{
			$('#err').show();
			flag = false;
		}
		if(noOfpayment == '')
		{
			$('#err').show();
			flag = false;
		}
		if(holdername == '')
		{
			$('#err').show();
			flag = false;
		}
		if(routingnumber == '')
		{
			$('#err').show();
			flag = false;
		}
		if(accountnumber == '')
		{
			$('#err').show();
			flag = false;
		}
		if(startdate == '')
		{
			$('#err').show();
			flag = false;
		}
		if(email == '')
		{
			$('#err').show();
			flag = false;
		}
		if(address == '')
		{
			$('#err').show();
			flag = false;
		}
		if(sign == '')
		{
			$('#err').show();
			flag = false;
		}

		return flag;
	}

	function val2()
	{
		flag = true;
		var firstname = $('#firstname').val();
		var amount = $('#amounts').val();
		var bankroutingnumber = $('#bankroutingnumber').val();
		var bankaccountnumber = $('#bankaccountnumber').val();
		var startdate = $('#startdate2').val();
		var frequencyofdonation = $('#frequencyofdonation').val();
		var start = $('#start').val();
		var email = $('#email2').val();
		var streetaddress = $('#streetaddress').val();
		var apt = $('#apt').val();
		var city = $('#city').val();
		var state = $('#state').val();
		var zip = $('#zip2').val();
		var phone = $('#phone2').val();
		var sign = $('#sign2').val();

		if(firstname == '')
		{
			alert('a');
			$('#err2').show();
			flag = false; 
		}
		if(amount == '')
		{
			alert('b');
			$('#err2').show();
			flag = false;
		}
		
		if(bankroutingnumber == '')
		{
			alert('c');
			$('#err2').show();
			flag = false;
		}
		if(bankaccountnumber == '')
		{
			alert('d');
			$('#err2').show();
			flag = false;
		}
		if(startdate == '')
		{
			alert('e');
			$('#err2').show();
			flag = false;
		}
		if(frequencyofdonation == '')
		{
			alert('f');
			$('#err2').show();
			flag = false;
		}
		if(email == '')
		{
			alert('g');
			$('#err2').show();
			flag = false;
		}
		if(streetaddress == '')
		{
			alert('h');
			$('#err2').show();
			flag = false;
		}
		if(apt == '')
		{
			alert('i');
			$('#err2').show();
			flag = false;
		}
		if(city == '')
		{
			alert('j');
			$('#err2').show();
			flag = false;
		}
		if(state == '')
		{
			alert('k');
			$('#err2').show();
			flag = false;
		}
		if(zip == '')
		{
			alert('l');
			$('#err2').show();
			flag = false;
		}
		if(phone == '')
		{
			alert('m');
			$('#err2').show();
			flag = false;
		}
		if(sign == '')
		{
			alert('n');
			$('#err2').show();
			flag = false;
		}

		return flag;
	}

	function selectbox(val)
	{
		$('#opportunities').val(val);
	}

</script>

<?php
$con  = mysql_connect("bkjha1.arvixevps.com","geek","geek1");
		mysql_select_db("iskcon");
/*$con  = mysql_connect("localhost","root","");
mysql_select_db("iskcon");*/

if(isset($_POST['form1']))
{
	extract($_POST);

	$query = "insert into pledge values('','$name', '$opportunities', '$other', '$noOfpayment', '$holdername', '$routingnumber', '$accountnumber', '$startdate', '$phone', '$email', '$address', '$sign')";
	$result = mysql_query($query);
	?>
	<script type="text/javascript">
		$('#thank').modal('show'); 
	</script>
	<?php
}

if(isset($_POST['form2']))
{
	extract($_POST);
	$donationtowards = implode (", ", $donationtowards	);
	$query = "insert into directDebit values('','$firstname', '$lastname', '$amount', '$donationtowards', '$bankroutingnumber', '$bankaccountnumber', '$startdate', '$frequencyofdonation', '$start', '$email', '$streetaddress', '$apt', '$city', '$state', '$zip', '$phone', '$sign')";
	$result = mysql_query($query);
	?>
	<script type="text/javascript">
		$('#thank').modal('show'); 
	</script>
	<?php
}
?>