﻿<?php include"includes/header.php"; ?>

		<!-- Slider -->
		<div class="slider">
			<ul class="rslides" id="slider">
				<li>
					<img src="images/IMG_1612.jpg" alt="iskcon temple">
				</li>
				<li>
					<img src="images/IMG_1471.jpg" alt="iskcon temple">
				</li>
				<li>
					<img src="images/IMG_1466.jpg" alt="iskcon temple">
				</li>
				<li>
					<img src="images/2.jpg" alt="iskcon temple">
				</li>
				

			</ul>
		</div>
		<!-- //Slider -->

	</div>
	<!-- //Header -->

	<!-- About -->
	<div class="about" id="about">
		<div class="container">
		<h1 class=" wow fadeInDown" data-wow-delay=".25s" >OUR MISSION</h1>
		<div class="heading-underline" ></div>

		<p data-wow-delay=".25s" class="wow fadeInRightBig">It is the desire of ISKCON Columbus and associates to be a place where all are welcomed, connected to Krishna, encouraged to grow in devotion to Him, enabled to practice Krishna consciousness based on the teachings of His Divine Grace Srila AC Bhaktivedanta Swami Prabhupada, and equipped to propagate Krishna consciousness.</p>
		<br><br>
		<h3 class=" wow fadeInDown" data-wow-delay=".25s">Process of Advancement in Stages of Devotion and Commitment to the Mission</h3>
		<br>
		<ul data-wow-delay=".25s" class="wow fadeInRightBig">
			<li>Members of Society at Large become welcomed to Krishna through outreach programs and become Guests</li>
			<li>Guests connect to Krishna by starting to regularly attend the Sunday Feast and become Congregational Devotees</li>
			<li>Congregational Devotees will grow in devotion by experiencing community life in Krishna consciousness by joining a small group and become Congregational Practitioners</li>
			<li>Congregational Practitioners will practice Krishna consciousness by committing to follow, develop knowledge about, and receive training in applying the teachings of Srila Prabhupada through Vaishnava education and training programs and commit to contribute to Srila Prabhupada’s Mission. They will become Temple Servants/Preachers.</li>
			<li>Temple Servants/ Preachers will propagate Krishna consciousness by placement and training in a Temple Department and become effective preachers and leaders in temple service.</li>
		</ul>
		</div>
	
		<!-- <div class="container">

			<div class="about-info">
				<div class="col-md-6 about-info-grid">
					<div class="about-info-list1">
						<h5>Consectetur</h5>
						<ul>
							<li><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> Lorem Ipsum Dolor Sit Amet</li>
							<li><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> Sed ut perspiciatis unde</li>
							<li><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> Neque porro quisquam est</li>
							<li><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> Quis autem vel eum iure</li>
							<li><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> Ut enim ad minima veniam</li>
						</ul>
					</div>
					<div class="about-info-image">
						<img src="images/about-1.jpg" alt="Corsa Racer">
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="col-md-6 about-info-grid">
					<div class="about-info-list2">
						<h5>Perspiciatis</h5>
						<ul>
							<li><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> Neque porro quisquam est</li>
							<li><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> Lorem Ipsum Dolor Sit Amet</li>
							<li><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> Ut enim ad minima veniam</li>
							<li><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> Neque porro quisquam est</li>
							<li><span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span> Sed ut perspiciatis unde</li>
						</ul>
					</div>
					<div class="about-info-image">
						<img src="images/about-2.jpg" alt="Corsa Racer">
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div> -->

	</div>
	<div class="clearfix"></div>
	<!-- //About -->

	<!-- Features -->
	<div class="features" id="features">
		<div class="container">

			<h2>SERVICES</h2>
			<div class="heading-underline"></div>

			<div class="feature-grid">
				<div class="col-md-4 col-sm-4 col-xs-12 feature1 wow bounceInUp wHighlight animated" data-wow-delay=".35s" >
					<div class="row features-item sans-shadow text-center">
						<!-- <div class="features-icon">
							<img src="images/graphics.png" alt="Corsa Racer">
						</div> -->
						<div class="features-info">
							<h4>College Outreach</h4>
							<p>ISKCON volunteers are running a club at Ohio State University under ISKCON Yoga Circle by partnering with current OSU students. We conduct weekly vegetarian cooking workshops at campus where more than 80 students attend. Most of them are western students. We demonstrate different vegetarian dishes every week and discuss about the ingredients, different spices that we use, their health benefits and also share recipes with the participants.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 feature1  wow bounceInUp wHighlight animated" data-wow-delay=".35s">
					<div class="row features-item sans-shadow text-center">
						<!-- <div class="features-icon">
							<img src="images/world.png" alt="Corsa Racer">
						</div> -->
						<div class="features-info">
							<h4>Sunday School</h4>
							<p>ISKCON Columbus conducts two Sunday school classes every Sunday for children with ages 4 to 14. Children are taught Vedic knowledge in a very easy, practical and fun way in a safe environment. We follow a very nice curriculum developed by very experienced and talented devotes based on vedic scriptures such as Bhagavad Gita and Srimad Bhagavatam. Children also learn sanskrit slokas, stories, participate in dramas and cultural activities.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 feature1 wow bounceInUp wHighlight animated" data-wow-delay=".35s">
					<div class="row features-item sans-shadow text-center">
						<!-- <div class="features-icon">
							<img src="images/engine.png" alt="Corsa Racer">
						</div> -->
						<div class="features-info">
							<h4>Festivals</h4>
							<p>Festivals are very integral part of Vedic culture. The past times of Lord and his devotees and their life styles, important milestones like appearance and disappearance days, inspirational incidents from the scriptures are enacted as dramas to understand the essence of them. All the Vaishnava festivals such as Sri Krishna Janmastami, Sri Rama Navami, Appearance of Lord Narsimha dev, Gaura Purnima etc are celebrated at the temple.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 feature1 wow bounceInUp wHighlight animated" data-wow-delay=".35s">
					<div class="row features-item sans-shadow text-center">
						<!-- <div class="features-icon">
							<img src="images/car.png" alt="Corsa Racer">
						</div>  -->
						<div class="features-info">
							<h4>Harinams</h4>
							<p>Harinam, short for Harinama-sankirtana (the congregational chanting of the holy names of the Lord) may be most familiar from seeing the devotees out in the streets, dancing and chanting the maha-mantra Hare Krishna, Hare Krishna, Krishna Krishna, Hare Hare/ Hare Rama, Hare Rama, Rama Rama, Hare Hare accompanied by mridangas (two headed drums) and karatalas (hand cymbals).</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 feature1 wow bounceInUp wHighlight animated"   data-wow-delay=".35s">
					<div class="row features-item sans-shadow text-center">
						<!-- <div class="features-icon">
							<img src="images/track.png" alt="Corsa Racer">
						</div> -->
						<div class="features-info">
							<h4>Book Distribution</h4>
							<p>ISKCON Columbus congregation performs book distribution of vedic literature on weekends, special occasions and during festivals by going out on streets, door to door and at different public places. The books such as Bhagavad-Gita and Srimad Bhagavatam are translated by ISKCON Founder Acarya Srila A.C Bhakti Vendanta Swami Srila Prabhupada.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 feature1 wow bounceInUp wHighlight animated" data-wow-delay=".35s">
					<div class="row features-item sans-shadow text-center">
						<!-- <div class="features-icon">
							<img src="images/multiplayer.png" alt="Corsa Racer">
						</div> -->
						<div class="features-info">
							<h4>Lorem ipsum</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>

		</div>
	</div>
	<!-- //Features -->

	<!-- Real -->
	<!-- <div class="real">

		<div class="container">
			<div class="col-md-4 col-sm-4 real-grid">
				<img src="images/cars.png" alt="Corsa Racer">
				<h3>Real Cars</h3>
				<p>More than 750 real, licensed perfectly designed and handling cars to choose from.</p>
			</div>
			<div class="col-md-4 col-sm-4 real-grid">
				<img src="images/tracks.png" alt="Corsa Racer">
				<h3>Real Tracks</h3>
				<p>Drive through mofre than 2000 licensed race tracks, accurate street circuits, dirt trials.</p>
			</div>
			<div class="col-md-4 col-sm-4 real-grid">
				<img src="images/players.png" alt="Corsa Racer">
				<h3>Real Players</h3>
				<p>Connect and compete against real people and players in the multiplayer races.</p>
			</div>
			<div class="clearfix"></div>
		</div>

	</div> -->
	<!-- //Real -->

	<!-- New -->
	<!-- <div class="new">
		<h3>UPDATES</h3>
		<div class="heading-underline"></div>

		<div class="container">
			<div class="col-md-6 col-sm-6 new-grid">
				<img src="images/1-Aerial-CU.jpg" alt="Corsa Racer">
			</div>
			<div class="col-md-6 col-sm-6 new-grid">
				<h4>Heading</h4>
				<ul>
					<li>Lorem Ipsum is simply dummy text</li>
					<li>Lorem Ipsum is simply dummy text</li>
					<li>Lorem Ipsum is simply dummy text</li>
					<li>Lorem Ipsum is simply dummy text</li>
					<li>Lorem Ipsum is simply dummy text</li>
				</ul>
				<br>
				<p>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</p><br
				<p>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</p>
			</div>
			<div class="clearfix"></div>
		</div>

	</div> -->
	<!-- //New -->

	<!-- Formats -->
	<div class="formats" id="gallery">

		<h3>GALLERY</h3>
		<div class="heading-underline"></div>
		
		<!-- Owl-Carousel -->
		<div id="owl-demo" class="owl-carousel text-center">
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog15">
				<img class="lazyOwl" data-src="images/1.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple view outside</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog1">
				<img class="lazyOwl" data-src="images/2.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple front view</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog2">
				<img class="lazyOwl" data-src="images/interior/i-Temple-2.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple interior</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog3">
				<img class="lazyOwl" data-src="images/3.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple side look</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog4">
				<img class="lazyOwl" data-src="images/4.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple side look</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog5">
				<img class="lazyOwl" data-src="images/interior/i-Lobby-2.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple lobby </span>
				</div>
			</div>
			
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog6">
				<img class="lazyOwl" data-src="images/6.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple front view</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog7">
				<img class="lazyOwl" data-src="images/interior/int-Courtyard.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple courtyard</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog8">
				<img class="lazyOwl" data-src="images/interior/int-Yoga.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple yoga place</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog9">
				<img class="lazyOwl" data-src="images/2.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple front </span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog10">
				<img class="lazyOwl" data-src="images/interior/i-Lobby.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple lobby view</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog11">
				<img class="lazyOwl" data-src="images/3.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple side view</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog12">
				<img class="lazyOwl" data-src="images/interior/i-iso-Gift-Shop.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple gift shop</span>
				</div>
			</div>
			
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog13">
				<img class="lazyOwl" data-src="images/interior/i-Temple.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple interior</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog14">
				<img class="lazyOwl" data-src="images/5.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple outside view</span>
				</div>
			</div>
		</div>
		<!-- //Owl-Carousel -->

		<!-- Magnific-Popup -->
		<div class="caption-popup">
			<div id="small-dialog" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/1.jpg" title="postname" />
				<p>Temple view outside</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog1" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/2.jpg" title="postname" />
				<p>Temple front view</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog2" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/interior/i-Temple-2.jpg" title="postname" />
				<p>Temple interior</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog3" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/3.jpg" title="postname" />
				<p>Temple side look</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog4" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/4.jpg" title="postname" />
				<p>Temple side look</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog5" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/interior/i-Lobby-2.jpg" title="postname" />
				<p>Temple lobby</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog6" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/6.jpg" title="postname" />
				<p>Temple front look</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog7" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/interior/int-Courtyard.jpg" title="postname" />
				<p>Temple courtyard</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog8" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/interior/int-Yoga.jpg" title="postname" />
				<p>Temple yoga place</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog9" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/2.jpg" title="postname" />
				<p>Temple front</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog10" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/interior/i-Lobby.jpg" title="postname" />
				<p>Temple lobby view</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog11" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/3.jpg" title="postname" />
				<p>Temple side view</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog12" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/interior/i-iso-Gift-Shop.jpg" title="postname" />
				<p>Temple gift shop</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog13" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/interior/i-Temple.jpg" title="postname" />
				<p>Temple interior</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog14" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/5.jpg" title="postname" />
				<p>Temple outside view</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog15" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/1.jpg" title="postname" />
				<p>Temple outside view</p>
			</div>
		</div>

		<!-- //Magnific-Popup -->

	</div>
	<!-- //Formats -->

	<!-- Progressive-Effects -->
	<div class="progressive-effects" id="skills">
		<div class="container">

			<!-- Skills -->
			<?php
			/*$con  = mysql_connect("bkjha1.arvixevps.com","geek","geek1");
					mysql_select_db("iskcon");
					
			$query = "SELECT * from totaltarget";
			$result = mysql_query($query);

			while($row = mysql_fetch_assoc($result))
			{
				$total = $row['amount'];
			}*/

			/*$query = "insert into totaltarget values('','50000000')";
			$result = mysql_query($query);*/

			/*$query = "SELECT sum(amount) as s from donation";
			$result = mysql_query($query);

			while($row = mysql_fetch_assoc($result))
			{
				echo $donation = $row['s'];
			}

			$percent = ($donation/$total)*100;*/


			?>
			<div id="about-us" class="parallax">
				<h3>Funds Raised so far</h3>
				<div class="heading-underline"></div>

				<div class="row">
					<div class="our-skills wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
						<div class="single-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
							<!-- <p class="lead">Funds Raised so far</p> -->
							<div class="progress">
								<div class="progress-bar progress-bar-primary six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="<?php echo "60";//echo intval($percent); ?>"><?php echo "60";//intval($percent); ?>%</div>
							</div>
						</div>
					</div>
					<br>
					<a href="donate.php"><button class="btn btn-lg center-block btn-primary" >Donate Now</button></a>
				</div>
			</div>
			<!-- //Skills -->

			<!-- Stats -->
			<div class="stats">

				<h3>SOME FUN FACTS</h3>
				<div class="heading-underline"></div>

				<div class="stats-info">
					<div class="col-md-3 col-sm-3 stats-grid">
						<!-- <div class="stats-img">
							<img src="images/s2.png" alt="Corsa Racer">
						</div> -->
						<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='81548' data-delay='.5' data-increment="1000">81548</div>
						<p>VISITORS ON JANMASHTAMI</p>
					</div>
					<div class="col-md-3 col-sm-3 stats-grid">
						<!-- <div class="stats-img">
							<img src="images/s3.png" alt="Corsa Racer">
						</div> -->
						<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='315655' data-delay='8' data-increment="1000">315655</div>
						<p>SQ METERS OF SPACE AT BHAKTIVEDANTA MANOR</p>
					</div>
					<div class="col-md-3 col-sm-3 stats-grid">
						<!-- <div class="stats-img">
							<img src="images/s4.png" alt="Corsa Racer">
						</div> -->
						<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='516' data-delay='.5' data-increment="1">516</div>
						<p>BRICKS DONATED ON JANMASHTAMI</p>
					</div>
					<div class="col-md-3 col-sm-3 stats-grid">
						<!-- <div class="stats-img">
							<img src="images/s5.png" alt="Corsa Racer">
						</div> -->
						<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='1024' data-delay='3' data-increment="100">1024</div>
						<p>FREE MEALS EVERYDAY FOR THE HOMELESS</p>
					</div>
					<div class="clearfix"></div>
				</div>

			</div>
			<!-- //Stats -->

		</div>
	</div>
	<!-- Progressive-Effects -->

	<!-- Team -->
	<!-- <div class="team" id="team">
		<h3>TEAM</h3>
		<div class="heading-underline"></div>

		<div class="container">
			<div class="grid">
				<figure class="effect-dexter">
					<img src="images/team-1.jpg" alt="Corsa Racer"/>
					<figcaption>
						<h4>Bruce <span> Wayne</span></h4>
						<ul class="social">
							<li><a href="#" class="facebook" title="Go to Our Facebook Page"></a></li>
							<li><a href="#" class="twitter" title="Go to Our Twitter Account"></a></li>
							<li><a href="#" class="googleplus" title="Go to Our Google Plus Account"></a></li>
						</ul>
					</figcaption>
				</figure>
				<figure class="effect-dexter">
					<img src="images/team-2.jpg" alt="Corsa Racer"/>
					<figcaption>
						<h4>Clark <span> Kent</span></h4>
						<ul class="social">
							<li><a href="#" class="facebook" title="Go to Our Facebook Page"></a></li>
							<li><a href="#" class="twitter" title="Go to Our Twitter Account"></a></li>
							<li><a href="#" class="googleplus" title="Go to Our Google Plus Account"></a></li>
						</ul>
					</figcaption>
				</figure>
				<figure class="effect-dexter">
					<img src="images/team-3.jpg" alt="Corsa Racer"/>
					<figcaption>
						<h4>Diana <span> Prince</span></h4>
						<ul class="social">
							<li><a href="#" class="facebook" title="Go to Our Facebook Page"></a></li>
							<li><a href="#" class="twitter" title="Go to Our Twitter Account"></a></li>
							<li><a href="#" class="googleplus" title="Go to Our Google Plus Account"></a></li>
						</ul>
					</figcaption>
				</figure>
				<figure class="effect-dexter">
					<img src="images/team-4.jpg" alt="Corsa Racer"/>
					<figcaption>
						<h4>Lex <span> Luthor</span></h4>
						<ul class="social">
							<li><a href="#" class="facebook" title="Go to Our Facebook Page"></a></li>
							<li><a href="#" class="twitter" title="Go to Our Twitter Account"></a></li>
							<li><a href="#" class="googleplus" title="Go to Our Google Plus Account"></a></li>
						</ul>
					</figcaption>
				</figure>
			</div>
		</div>
	</div> -->
	<!-- //Team -->

	<!-- Map-iFrame -->
	<div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3055.399191752806!2d-83.221595049342!3d40.02186597931202!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x883896708e160dbf%3A0xaef76c9ffe609cca!2s3520+Walker+Rd%2C+Hilliard%2C+OH+43026%2C+USA!5e0!3m2!1sen!2sin!4v1494045755021" allowfullscreen></iframe>
	</div>
	<!-- //Map-iFrame -->

	<!-- Contact -->
	<div class="contact" id="contact">
		<div class="container">

			<h3>Contact</h3>
			<div class="heading-underline"></div>

			<form class="contact_form" method="post">

				<div class="message">
					<div class="col-md-6 col-sm-6 grid_6 c1">
						<input type="text" class="text" placeholder="Name" name="name" required="" >
						<input type="text" class="text" placeholder="Email" name="email" required="" >
						<input type="text" class="text" placeholder="Phone" name="phone" required="" >
					</div>

					<div class="col-md-6 col-sm-6 grid_6 c1">
						<textarea placeholder="Message" name="message" required=""></textarea>
					</div>
					<div class="clearfix"></div>

					<input type="submit" name="sub" class="more_btn" value="Send Message">
				</div>
			</form>

		</div>
	</div>
	<!-- //Contact -->
<?php include"includes/footer.php"; ?>

<script type="text/javascript"> 
new WOW().init();
</script>

<?php
if(isset($_POST['sub']))
{
$name = $_POST['name'];
$email_address = $_POST['email'];
$message = $_POST['message'];
$phone = $_POST['phone'];

// The message
//$message = "Line 1\r\nLine 2\r\nLine 3";
$email_subject = "Website Contact Form:".$name;
// In case any of our lines are larger than 70 characters, we should use wordwrap()
//$message = wordwrap($message, 70, "\r\n");
$message = "You have received a new message from your website contact form.\n\n"."Here are the details:\n\nName: $name\n\nEmail: $email_address\n\n\n\nPhone: $phone\n\nMessage:\n$message";

$ToEmail = 'abhiraj.vishwakarma37@gmail.com'; 
$EmailSubject = 'Site contact form'; 
$mailheader = "From: ".$_POST["email"]."\r\n"; 
$mailheader .= "Reply-To: ".$_POST["email"]."\r\n"; 
$mailheader .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
$MESSAGE_BODY = "You have received a new message from your website contact form.\n\n"; 
$MESSAGE_BODY .= "Name: ".$_POST["name"].""; 
$MESSAGE_BODY .= "Email: ".$_POST["email"]."";
$MESSAGE_BODY .= "Phone: ".$_POST["phone"].""; 
$MESSAGE_BODY .= "Comment: ".nl2br($_POST["message"]).""; 
mail($ToEmail, $EmailSubject, $MESSAGE_BODY, $mailheader) or die ("Failure");

// Send
/*if(mail('abhi87700@gmail.com', $email_subject, $message))
{*/
	?>
	<script type="text/javascript">
	window.location.href = 'index.php';
	</script>
	<?php
/*}*/
}
?>
