<!DOCTYPE html>
<html>
<head>

<title>ISKCON</title>

<!-- For-Mobile-Apps -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //For-Mobile-Apps -->

<!-- Custom-Stylesheet-Links -->
	<!-- Bootstrap-Core-CSS --> <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="all"/>
	<!-- Index-Page-Styling --> <link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<!-- Owl-Carousel-CSS --> <link rel="stylesheet" href="css/owl.carousel.css" type="text/css" media="all"/>
	<!-- Popup-Box-CSS --> <link rel="stylesheet" href="css/popuo-box.css" type="text/css" media="all"/>
<!-- //Custom-Stylesheet-Links -->

<!-- Web-Fonts -->
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Racing+Sans+One' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<!-- //Web-Fonts -->


</head>
<body>
	<!-- Header -->
	<div class="header" >

		<!-- Navbar -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php"><img style="float:left" src="images/iskcon.png" ><h2 style="float:left;padding:10px;color:#000">ISKCON Columbus</h2></a>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="hover-effect"><a href="index.php">Home</a></li>
						<li class="hover-effect"><a href="index.php#about">About</a></li>
						<li class="hover-effect"><a href="index.php#features">Updates</a></li>
						<li class="hover-effect"><a href="index.php#about-us">Donate</a></li>
						<li class="hover-effect"><a href="index.php#gallery">Gallery</a></li>
						<li class="hover-effect"><a href="index.php#contact">Contact</a></li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- //Navbar -->