﻿<?php include"includes/header.php"; ?>

		<!-- Slider -->
		<div class="slider">
			<ul class="rslides" id="slider">
				
				<li>
					<img src="images/1.jpg" alt="iskcon temple">
				</li>
				
				

			</ul>
		</div>
		<!-- //Slider -->

	</div>
	<!-- //Header -->

	<!-- About -->
	<div class="about" id="about">
		<div class="container">
		<h1 class=" wow fadeInDown" data-wow-delay=".25s" >LET'S BUILD THE TEMPLE</h1>
		<div class="heading-underline"></div>
			
		<ul data-wow-delay=".25s" class="wow fadeInRightBig">
			<li>ISKCON of Columbus, Ohio, USA, has purchased fifty three acres of prime real estate fifteen minutes’ drive from downtown Columbus.</li>
			<li>Devotees plan to develop a Krishna conscious community that offers the best of both urban and rural worlds.</li>
<li>The local congregation raised $500,000 and bought the new property outright. Located in a fast-developing suburb called Hilliard, it’s flanked by upcoming million-dollar homes and neighboring land owned by banks.

<li>While all amenities are close by, ISKCON Columbus is planning a haven of simple living and high thinking. Within six months to a year, devotees expect to break ground and begin construction on a gorgeous temple with a 500-person capacity temple room, a gift shop, accommodation for celibate students, and a children’s school.</li>

<li>

The landscaped grounds will include walking trails, retreat facilities, cow protection facilities, homes for devotees, and farmland.

“We also plan to have a farm-to-table restaurant where we’ll serve visitors fresh vegetables and fruits from our organic garden, and a bakery too,” says Prem Vilas Das, who is on the board of directors along with Prem Sindhu das, Naveen Krishna das and Ram Tirtha das.

“Then we’d like to have a Community Supported Agriculture (CSA) program where we can teach devotees and local people how to do their own farming,” he adds. “We’ll assign a couple of acres, and people can come and grow their own vegetables, shrubs and flowers.”

Also planned is a vegetarian culinary institute that will teach vegetarian cooking to the general public.

Columbus has always been an exciting place for Krishna conscious outreach. In May 1969, Srila Prabhupada stayed there for a week, inaugurated a temple, initiated devotees and held a program at Ohio State University with Allen Ginsberg, where 2,000 students chanted and danced. The temple, located on the OSU campus along with a Govinda’s restaurant, was a hit.</li>
		</ul>
		</div>
	
	
	</div>
	<div class="clearfix"></div>
	<!-- //About -->

<!-- Features -->
	<div class="features" id="features">
		<div class="container">

			<h2>UPDATES</h2>
			<div class="heading-underline"></div>

			<div class="feature-grid">
				<div class="col-md-4 col-sm-4 col-xs-12 feature1 wow bounceInUp wHighlight animated" data-wow-delay=".35s" >
					<div class="row features-item sans-shadow text-center">
						<!-- <div class="features-icon">
							<img src="images/graphics.png" alt="Corsa Racer">
						</div> -->
						<div class="features-info">
							<h4>Feb 2016</h4>
							<p>53 Acres purchased in Hilliard, ohio at 3508 Walker Road.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 feature1  wow bounceInUp wHighlight animated" data-wow-delay=".35s">
					<div class="row features-item sans-shadow text-center">
						<!-- <div class="features-icon">
							<img src="images/world.png" alt="Corsa Racer">
						</div> -->
						<div class="features-info">
							<h4>June 2016</h4>
							<p>2500 SF Ranch purchased in front of the land.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 feature1 wow bounceInUp wHighlight animated" data-wow-delay=".35s">
					<div class="row features-item sans-shadow text-center">
						<!-- <div class="features-icon">
							<img src="images/engine.png" alt="Corsa Racer">
						</div> -->
						<div class="features-info">
							<h4>Apr 2017</h4>
							<p>Schematic design completed.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 feature1 wow bounceInUp wHighlight animated" data-wow-delay=".35s">
					<div class="row features-item sans-shadow text-center">
						<!-- <div class="features-icon">
							<img src="images/car.png" alt="Corsa Racer">
						</div>  -->
						<div class="features-info">
							<h4>2017-2018</h4>
							<p>Fund Raising (Target $10 Million).</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 feature1 wow bounceInUp wHighlight animated"   data-wow-delay=".35s">
					<div class="row features-item sans-shadow text-center">
						<!-- <div class="features-icon">
							<img src="images/track.png" alt="Corsa Racer">
						</div> -->
						<div class="features-info">
							<h4>2019</h4>
							<p>Construction Drawing.</p>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 feature1 wow bounceInUp wHighlight animated" data-wow-delay=".35s">
					<div class="row features-item sans-shadow text-center">
						<!-- <div class="features-icon">
							<img src="images/multiplayer.png" alt="Corsa Racer">
						</div> -->
						<div class="features-info">
							<h4>2020</h4>
							<p>Temple construction bidding.</p>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>

		</div>
	</div>
	<!-- //Features -->

	

	<!-- Formats -->
	<div class="formats" id="gallery">

		<h3>GALLERY</h3>
		<div class="heading-underline"></div>
		
		<!-- Owl-Carousel -->
		<div id="owl-demo" class="owl-carousel text-center">
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog15">
				<img class="lazyOwl" data-src="images/1.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple view outside</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog1">
				<img class="lazyOwl" data-src="images/2.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple front view</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog2">
				<img class="lazyOwl" data-src="images/interior/i-Temple-2.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple interior</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog3">
				<img class="lazyOwl" data-src="images/3.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple side look</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog4">
				<img class="lazyOwl" data-src="images/4.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple side look</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog5">
				<img class="lazyOwl" data-src="images/interior/i-Lobby-2.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple lobby </span>
				</div>
			</div>
			
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog6">
				<img class="lazyOwl" data-src="images/6.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple front view</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog7">
				<img class="lazyOwl" data-src="images/interior/int-Courtyard.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple courtyard</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog8">
				<img class="lazyOwl" data-src="images/interior/int-Yoga.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple yoga place</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog9">
				<img class="lazyOwl" data-src="images/2.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple front </span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog10">
				<img class="lazyOwl" data-src="images/interior/i-Lobby.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple lobby view</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog11">
				<img class="lazyOwl" data-src="images/3.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple side view</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog12">
				<img class="lazyOwl" data-src="images/interior/i-iso-Gift-Shop.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple gift shop</span>
				</div>
			</div>
			
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog13">
				<img class="lazyOwl" data-src="images/interior/i-Temple.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple interior</span>
				</div>
			</div>
			<div class="item g1 popup-with-zoom-anim" href="#small-dialog14">
				<img class="lazyOwl" data-src="images/5.jpg" alt="ISKCON">
				<div class="caption">
					<h4>ISKCON</h4>
					<span>Temple outside view</span>
				</div>
			</div>
		</div>
		<!-- //Owl-Carousel -->

		<!-- Magnific-Popup -->
		<div class="caption-popup">
			<div id="small-dialog" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/1.jpg" title="postname" />
				<p>Temple view outside</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog1" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/2.jpg" title="postname" />
				<p>Temple front view</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog2" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/interior/i-Temple-2.jpg" title="postname" />
				<p>Temple interior</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog3" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/3.jpg" title="postname" />
				<p>Temple side look</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog4" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/4.jpg" title="postname" />
				<p>Temple side look</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog5" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/interior/i-Lobby-2.jpg" title="postname" />
				<p>Temple lobby</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog6" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/6.jpg" title="postname" />
				<p>Temple front look</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog7" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/interior/int-Courtyard.jpg" title="postname" />
				<p>Temple courtyard</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog8" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/interior/int-Yoga.jpg" title="postname" />
				<p>Temple yoga place</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog9" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/2.jpg" title="postname" />
				<p>Temple front</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog10" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/interior/i-Lobby.jpg" title="postname" />
				<p>Temple lobby view</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog11" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/3.jpg" title="postname" />
				<p>Temple side view</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog12" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/interior/i-iso-Gift-Shop.jpg" title="postname" />
				<p>Temple gift shop</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog13" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/interior/i-Temple.jpg" title="postname" />
				<p>Temple interior</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog14" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/5.jpg" title="postname" />
				<p>Temple outside view</p>
			</div>
		</div>
		<div class="caption-popup">
			<div id="small-dialog15" class="mfp-hide innercontent">
				<h3>Image Gallery - ISKCON</h3>
				<img class="img-responsive" src="images/1.jpg" title="postname" />
				<p>Temple outside view</p>
			</div>
		</div>

		<!-- //Magnific-Popup -->

	</div>
	<!-- //Formats -->

	<!-- Progressive-Effects -->
	<div class="progressive-effects" id="skills">
		<div class="container">

			<!-- Skills -->
			<?php
			/*$con  = mysql_connect("bkjha1.arvixevps.com","geek","geek1");
					mysql_select_db("iskcon");
					
			$query = "SELECT * from totaltarget";
			$result = mysql_query($query);

			while($row = mysql_fetch_assoc($result))
			{
				$total = $row['amount'];
			}*/

			/*$query = "insert into totaltarget values('','50000000')";
			$result = mysql_query($query);*/

			/*$query = "SELECT sum(amount) as s from donation";
			$result = mysql_query($query);

			while($row = mysql_fetch_assoc($result))
			{
				echo $donation = $row['s'];
			}

			$percent = ($donation/$total)*100;*/


			?>
			<div id="about-us" class="parallax">
				<h3>Funds Raised so far</h3>
				<div class="heading-underline"></div>

				<div class="row">
					<div class="our-skills wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
						<div class="single-skill wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
							<!-- <p class="lead">Funds Raised so far</p> -->
							<div class="progress">
								<div class="progress-bar progress-bar-primary six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="<?php echo "12";//echo intval($percent); ?>"><?php echo "12";//intval($percent); ?>%</div>
							</div>
						</div>
					</div>
					<br>
					<a href="donate.php"><button class="btn btn-lg center-block btn-primary" >Donate Now</button></a>
				</div>
			</div>
			<!-- //Skills -->

			<!-- Stats -->
			<div class="stats">

				<h3>SOME FUN FACTS</h3>
				<div class="heading-underline"></div>

				<div class="stats-info">
					<div class="col-md-3 col-sm-3 stats-grid">
						<!-- <div class="stats-img">
							<img src="images/s2.png" alt="Corsa Racer">
						</div> -->
						<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='81548' data-delay='.5' data-increment="1000">81548</div>
						<p>VISITORS ON JANMASHTAMI</p>
					</div>
					<div class="col-md-3 col-sm-3 stats-grid">
						<!-- <div class="stats-img">
							<img src="images/s3.png" alt="Corsa Racer">
						</div> -->
						<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='315655' data-delay='8' data-increment="1000">315655</div>
						<p>SQ METERS OF SPACE AT BHAKTIVEDANTA MANOR</p>
					</div>
					<div class="col-md-3 col-sm-3 stats-grid">
						<!-- <div class="stats-img">
							<img src="images/s4.png" alt="Corsa Racer">
						</div> -->
						<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='516' data-delay='.5' data-increment="1">516</div>
						<p>BRICKS DONATED ON JANMASHTAMI</p>
					</div>
					<div class="col-md-3 col-sm-3 stats-grid">
						<!-- <div class="stats-img">
							<img src="images/s5.png" alt="Corsa Racer">
						</div> -->
						<div class='numscroller numscroller-big-bottom' data-slno='1' data-min='0' data-max='1024' data-delay='3' data-increment="100">1024</div>
						<p>FREE MEALS EVERYDAY FOR THE HOMELESS</p>
					</div>
					<div class="clearfix"></div>
				</div>

			</div>
			<!-- //Stats -->

		</div>
	</div>
	<!-- Progressive-Effects -->

	<!-- Team -->
	<!-- <div class="team" id="team">
		<h3>TEAM</h3>
		<div class="heading-underline"></div>

		<div class="container">
			<div class="grid">
				<figure class="effect-dexter">
					<img src="images/team-1.jpg" alt="Corsa Racer"/>
					<figcaption>
						<h4>Bruce <span> Wayne</span></h4>
						<ul class="social">
							<li><a href="#" class="facebook" title="Go to Our Facebook Page"></a></li>
							<li><a href="#" class="twitter" title="Go to Our Twitter Account"></a></li>
							<li><a href="#" class="googleplus" title="Go to Our Google Plus Account"></a></li>
						</ul>
					</figcaption>
				</figure>
				<figure class="effect-dexter">
					<img src="images/team-2.jpg" alt="Corsa Racer"/>
					<figcaption>
						<h4>Clark <span> Kent</span></h4>
						<ul class="social">
							<li><a href="#" class="facebook" title="Go to Our Facebook Page"></a></li>
							<li><a href="#" class="twitter" title="Go to Our Twitter Account"></a></li>
							<li><a href="#" class="googleplus" title="Go to Our Google Plus Account"></a></li>
						</ul>
					</figcaption>
				</figure>
				<figure class="effect-dexter">
					<img src="images/team-3.jpg" alt="Corsa Racer"/>
					<figcaption>
						<h4>Diana <span> Prince</span></h4>
						<ul class="social">
							<li><a href="#" class="facebook" title="Go to Our Facebook Page"></a></li>
							<li><a href="#" class="twitter" title="Go to Our Twitter Account"></a></li>
							<li><a href="#" class="googleplus" title="Go to Our Google Plus Account"></a></li>
						</ul>
					</figcaption>
				</figure>
				<figure class="effect-dexter">
					<img src="images/team-4.jpg" alt="Corsa Racer"/>
					<figcaption>
						<h4>Lex <span> Luthor</span></h4>
						<ul class="social">
							<li><a href="#" class="facebook" title="Go to Our Facebook Page"></a></li>
							<li><a href="#" class="twitter" title="Go to Our Twitter Account"></a></li>
							<li><a href="#" class="googleplus" title="Go to Our Google Plus Account"></a></li>
						</ul>
					</figcaption>
				</figure>
			</div>
		</div>
	</div> -->
	<!-- //Team -->

	<!-- Map-iFrame -->
	<div class="map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3055.399191752806!2d-83.221595049342!3d40.02186597931202!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x883896708e160dbf%3A0xaef76c9ffe609cca!2s3520+Walker+Rd%2C+Hilliard%2C+OH+43026%2C+USA!5e0!3m2!1sen!2sin!4v149404575502z" allowfullscreen></iframe>
	</div>
	<!-- //Map-iFrame -->

	<!-- Contact -->
	<div class="contact" id="contact">
		<div class="container">

			<h3>Contact</h3>
			<div class="heading-underline"></div>

			<form class="contact_form" method="post">

				<div class="message">
					<div class="col-md-6 col-sm-6 grid_6 c1">
						<input type="text" class="text" placeholder="Name" name="name" required="" >
						<input type="text" class="text" placeholder="Email" name="email" required="" >
						<input type="text" class="text" placeholder="Phone" name="phone" required="" >
					</div>

					<div class="col-md-6 col-sm-6 grid_6 c1">
						<textarea placeholder="Message" name="message" required=""></textarea>
					</div>
					<div class="clearfix"></div>

					<input type="submit" name="sub" class="more_btn" value="Send Message">
				</div>
			</form>

		</div>
	</div>
	<!-- //Contact -->
<?php include"includes/footer.php"; ?>

<script type="text/javascript"> 
new WOW().init();
</script>

<?php
if(isset($_POST['sub']))
{
$name = $_POST['name'];
$email_address = $_POST['email'];
$message = $_POST['message'];
$phone = $_POST['phone'];

// The message
//$message = "Line 1\r\nLine 2\r\nLine 3";
$email_subject = "Website Contact Form:".$name;
// In case any of our lines are larger than 70 characters, we should use wordwrap()
//$message = wordwrap($message, 70, "\r\n");
$message = "You have received a new message from your website contact form.\n\n"."Here are the details:\n\nName: $name\n\nEmail: $email_address\n\n\n\nPhone: $phone\n\nMessage:\n$message";

$ToEmail = 'abhiraj.vishwakarma37@gmail.com'; 
$EmailSubject = 'Site contact form'; 
$mailheader = "From: ".$_POST["email"]."\r\n"; 
$mailheader .= "Reply-To: ".$_POST["email"]."\r\n"; 
$mailheader .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
$MESSAGE_BODY = "You have received a new message from your website contact form.\n\n"; 
$MESSAGE_BODY .= "Name: ".$_POST["name"].""; 
$MESSAGE_BODY .= "Email: ".$_POST["email"]."";
$MESSAGE_BODY .= "Phone: ".$_POST["phone"].""; 
$MESSAGE_BODY .= "Comment: ".nl2br($_POST["message"]).""; 
mail($ToEmail, $EmailSubject, $MESSAGE_BODY, $mailheader) or die ("Failure");

// Send
/*if(mail('abhi87700@gmail.com', $email_subject, $message))
{*/
	?>
	<script type="text/javascript">
	window.location.href = 'index.php';
	</script>
	<?php
/*}*/
}
?>
