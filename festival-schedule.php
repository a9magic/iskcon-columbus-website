<div class="vc_row-fluid">
    <div class="vc_col-sm-12 wpb_column vc_column_container ">
        <div class="wpb_wrapper">
            <section class="shedule section-padding"
                     style="background: url(images/Untitled-1-1.jpg);">
                <div class="container">
                    <div class="row">
                        <h2 class="section-title text-center wow fadeInUp"
                            data-wow-delay="0.4s"><i
                                    class="fa fa-bullhorn fa-2x "></i> Ekadashis & Other Festivals
                            in 2020
                        </h2>
                        <!-- /.section-title -->

                        <div class="col-md-12">
                            <div class="shedule-list col-md-12 wow fadeInUp"
                                 data-wow-delay="0.6s">

                                <table class="festivals table thead-inverse table-hover table-responsive">
                                    <thead>
                                    <tr>
                                        <th>
                                            Festival
                                        </th>
                                        <th>
                                            Date
                                        </th>
                                        <th>
                                            Day
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            3 hour kirtan on New Year 2020 10 AM to 1 PM
                                        </td>
                                        <td>
                                            01/01/2020
                                        </td>
                                        <td>
                                            Wednesday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Appearance of Sri Advaita Acharya
                                        </td>
                                        <td>
                                            01/31/2020
                                        </td>
                                        <td>
                                            Friday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Appearance of Lord Nityananda Prabhu
                                        </td>
                                        <td>
                                            02/07/2020
                                        </td>
                                        <td>
                                            Friday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Appearance of Srila Bhaktisiddhanta Saraswati Thakur
                                        </td>
                                        <td>
                                            02/13/2020
                                        </td>
                                        <td>
                                            Thursday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Gaura Purnima
                                        </td>
                                        <td>
                                            03/09/2020
                                        </td>
                                        <td>
                                            Monday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Sri Rama Navami - Appearance of Lord Ramachandra
                                        </td>
                                        <td>
                                            04/02/2020
                                        </td>
                                        <td>
                                            Thursday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Appearance of Lord Narsimhadeva
                                        </td>
                                        <td>
                                            05/06/2020
                                        </td>
                                        <td>
                                            Wednesday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Appearance of Lord Balaram
                                        </td>
                                        <td>
                                            08/03/2020
                                        </td>
                                        <td>
                                            Monday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Sri Krishna Janmastami
                                        </td>
                                        <td>
                                            08/11/2020
                                        </td>
                                        <td>
                                            Tuesday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Srila Prabhupada Appearance day
                                        </td>
                                        <td>
                                            08/12/2020
                                        </td>
                                        <td>
                                            Wednesday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Radhastami
                                        </td>
                                        <td>
                                            08/25/2020
                                        </td>
                                        <td>
                                            Tuesday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Appearance of Lord Vamanadeva
                                        </td>
                                        <td>
                                            08/29/2020
                                        </td>
                                        <td>
                                            Saturday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Govardhana Puja/Diwali celebration
                                        </td>
                                        <td>
                                            11/15/2020
                                        </td>
                                        <td>
                                            Sunday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Disappearance of Srila Prabhupada
                                        </td>
                                        <td>
                                            11/18/2020
                                        </td>
                                        <td>
                                            Wednesday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Thanksgiving - Appreciating Devotees Festival
                                        </td>
                                        <td>
                                            11/26/2020
                                        </td>
                                        <td>
                                            Thursday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Advent of Bhagavad Gita
                                        </td>
                                        <td>
                                            12/25/2020
                                        </td>
                                        <td>
                                            Friday
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            6-hour Kirtan on New Year's Eve
                                        </td>
                                        <td>
                                            12/31/2020
                                        </td>
                                        <td>
                                            Thursday
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section>

        </div>
    </div>
</div>
