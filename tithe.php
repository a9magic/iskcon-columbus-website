<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <title>Tithe - Support Monthly
    </title>
    <?php
    include('meta_links.php');
    ?>
</head>
<body class="product-template-default single single-product postid-247 _masterslider _msp_version_3.0.4 woocommerce woocommerce-page wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- /#preloader -->
<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php include('header.php'); ?>
    <section id="blog-heading" style="height:auto;">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text pull-left" style="width:99%;padding-top:25px;">
                    <h1 class="blog-title" style="text-align:center;font-size:31px;">Tithe - Monthly Support
                    </h1>
                </div>
                <!-- /.heading-text -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.heading-fullwidth -->
    </section>

    <section id="shop" style="padding:0 !important;">
        <h3 class="text-center" style="padding-top:15px;"> Join our family </h3>

        <div class="vc_row-fluid">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">
                    <section class="shedule section-padding" style="background: url(images/Untitled-1-1.jpg);">
                        <div class="container">
                            <div class="row">

                                <div class="col-md-6 wow fadeInLeft" data-wow-delay="0.6s">
                                    <h3><i class="fa fa-heart"></i> Partner With Us & Make A Monthly Temple Tithe
                                    </h3>
                                    <br/>
                                    <h2 class="section-title">About Tithing</h2>
                                    <div class="section-detail">
                                        <br/><br/>A temple needs the support of its congregation to remain solvent and
                                        stable and to provide services for the community on a regular basis. Tithing,
                                        the regular commitment of a monthly donation to support the functioning of our
                                        temple, defines your relationship with Krishna. Many devotees in our community
                                        does not
                                        have time on a daily basis to offer physical service at the temple. But you can
                                        still be a direct part of the seva here by supporting these activities through
                                        financial contributions.
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="wow fadeInRight" data-wow-delay="0.6s">
                                        <img width="100%" height="100%" src="images/IMG_1468.jpg"
                                             class="attachment-shop_single size-shop_single wp-post-image"
                                             alt="Radha Natabara" title="Radha Natabara"/>
                                        <div class="thumbnails columns-3">
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>

                </div>
            </div>
        </div>

        <div class="container">
            <!-- festival -->
            <div class="full-width">
                <div class="vc_row-fluid">
                    <div class="vc_col-sm-12 wpb_column vc_column_container ">
                        <div class="wpb_wrapper">

                            <h2 class="section-title wow fadeInUp" style="padding: 15px 0 15px 0;"
                                data-wow-delay="0.4s"><i
                                        class="fa fa-cogs fa-2x"></i> How to setup - Tithe </h2>
                            <!-- /.section-title -->

                            <div class="sermons">

                                <div class="col-md-8 col-sm-8 text-center">
                                    <h1 class="wow fadeInUp" data-wow-delay="0.4s" style="padding: 10px;"><u>Use
                                            Credit/Debit Card</u></h1>
                                    <div class="col-md-6 col-sm-6 text-center">
                                        <div class="vc_box_shadow_border wow fadeInLeft text-center"
                                             data-wow-delay="0.6s"
                                             style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post"
                                                  target="_blank">
                                                <input type="hidden" name="cmd" value="_s-xclick">
                                                <input type="hidden" name="hosted_button_id" value="7JES623MWRDFJ">
                                                <table>
                                                    <tr>
                                                        <td><input type="hidden" name="on0"
                                                                   value="My Tithing Pledge is">
                                                            <h3> My Tithing Pledge is </h3>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><select name="os0">
                                                                <option value="Option 1"> $25.00 USD - monthly
                                                                </option>
                                                                <option value="Option 2"> $50.00 USD - monthly
                                                                </option>
                                                                <option value="Option 3"> $75.00 USD - monthly
                                                                </option>
                                                                <option value="Option 4"> $100.00 USD - monthly
                                                                </option>
                                                                <option value="Option 5"> $150.00 USD - monthly
                                                                </option>
                                                                <option value="Option 6"> $250.00 USD - monthly
                                                                </option>
                                                                <option value="Option 7"> $500.00 USD - monthly
                                                                </option>
                                                            </select></td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="hidden" name="on1" value="Please add memo">
                                                            <div class="vc_empty_space" style="padding-top: 15px"><span
                                                                        class="vc_empty_space_inner"></span>
                                                                <h3> Please add memo </h3>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="text" name="os1" maxlength="200"></td>
                                                    </tr>
                                                </table>
                                                <input type="hidden" name="currency_code" value="USD">
                                                <input align="center" type="image"
                                                       src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribeCC_LG.gif"
                                                       border="0" name="submit"
                                                       alt="PayPal - The safer, easier way to pay online!">
                                            </form>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6 text-center">
                                        <div class="vc_box_shadow_border wow fadeInLeft text-center"
                                             data-wow-delay="0.6s"
                                             style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                            <h2 style="padding: 10px;">One Time Donation</h2>
                                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post"
                                                  target="_blank">
                                                <input type="hidden" name="cmd" value="_s-xclick">
                                                <input type="hidden" name="hosted_button_id" value="K94FLQYGZG3W2">
                                                <input type="image"
                                                       src="https://www.paypalobjects.com/WEBSCR-640-20110429-1/en_US/i/btn/btn_donateCC_LG.gif"
                                                       border="0" name="submit"
                                                       alt="PayPal - The safer, easier way to pay online!">
                                                <img alt="" border="0"
                                                     src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif"
                                                     width="1" height="1">
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="vc_box_shadow_border wow fadeInRight text-center" data-wow-delay="0.6s"
                                         style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                        <h1 style="padding: 10px;"><u>ACH Debit Form</u></h1>
                                        <?php include('ach-form.php') ?>
                                    </div>
                                </div>

                                <div class="coll-md-12 col-sm-12 text-center wow fadeInUp" data-wow-delay="0.6s"
                                     style="padding: 30px;">
                                    If setting up on your own: Payee name: ISKCON Columbus, Address: 379 West 8th Ave
                                    Columbus, OH 43201. If you have any questions, contact <strong>Ram Tirtha
                                        Das </strong> at 614-404-8570 or stop by in the temple.
                                </div>
                            </div>

                            <div class="col-md-12 text-center wow fadeInUp" data-wow-delay="0.4s"
                                 style="padding: 30px;">
                                <strong>Receive Lord Krishna's blessings by sharing a portion of your income. No amount
                                    is insignificant, everything helps.</strong>
                            </div>

                            <h2 class="section-title wow fadeInUp" style="padding: 15px 0 15px 0;"
                                data-wow-delay="0.4s"><i
                                        class="fa fa-pagelines fa-2x"></i> Principle Reasons to Tithe: </h2>
                            <!-- /.section-title -->

                            <div class="col-md-12 wow fadeInUp" data-wow-delay="0.4s" style="padding: 15px;">
                                <ul class="list-group">
                                    <li class="list-group-item list-group-item-success">
                                        Support Temple services - prasadam distribution, Deity worship, book
                                        distribution, classes, and university outreach
                                    </li>
                                    <li class="list-group-item list-group-item-info">
                                        Assist the Temple to reach out to our congregation of devotees.
                                    </li>
                                    <li class="list-group-item list-group-item-warning">
                                        Take a step towards selfless service by practicing generosity.
                                    </li>
                                    <li class="list-group-item list-group-item-danger">
                                        Set a good example to your children and fellow devotees.
                                    </li>
                                    <li class="list-group-item list-group-item-success">
                                        Know you are pleasing Sri Sri Radha Natabara, Sri Sri Gaura Nitai, Sri Sri
                                        Jagannatha Baladev and Subhadra, and Srila Prabhupada
                                    </li>
                                </ul>
                            </div>

                            <div class="col-md-12 wow fadeInUp" data-wow-delay="0.4s" style="padding: 15px;">
                                <h3>Remember that 365 days a year our ISKCON Temple maintains </h3>

                                <ul class="list-unstyled" style="margin-left: 2%;">
                                    <ul>
                                        <li>Breathtaking Daily viewings (darshans) of Sri Sri Radha Natabara, Sri Sri
                                            Gaura Nitai, Sri Sri Jagannatha Baladev and Subhadra including gorgeous
                                            dresses and jewelry.
                                        </li>
                                        <li>Daily Śrīmad-Bhāgavatam classes.</li>
                                        <li>Daily breakfast and lunch prasadam.</li>
                                        <li>Prasadam distribution at OSU Campus and Sunday Feasts.</li>
                                        <li>Maintenence of Srimati Tulasi devi for the pleasure of Sri Krishna.</li>
                                        <li>Many annual festivals, Monthly Maha-santsang, weekly Harinaam Sankirtan and
                                            transcedental book distribution.
                                        </li>
                                        <li>AND MUCH MORE...</li>
                                    </ul>

                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end festivals -->
        </div>
        <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span>
        </div>
        <div class="modal fade" role="dialog" id="thank">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h2>Thank you for your donation!</h2>
                    </div>
                </div>
            </div>
        </div>


        <h6 class="wow fadeInUp text-center" data-wow-delay="0.4s">
            We are a 501 (C) (3) non-profit charity organization. Our IRS tax id is <strong>34-1225-440</strong> and all
            donations
            made to 'ISKCON Columbus' are tax deductible.
        </h6>
        <?php
        include('footer.php');
        ?>
</body>
</html>
