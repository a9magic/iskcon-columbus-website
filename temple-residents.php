<div class="full-width">
    <div class="vc_row-fluid vc_custom_1475847937518">
        <div class="vc_col-sm-12 wpb_column vc_column_container ">
            <div class="wpb_wrapper">

                <h2 class="section-title wow fadeInUp" data-wow-delay="0.4s"> Temple Priests </h2>
                <!-- /.section - title -->
            </div><!-- /.section - detail -->

            <div class="sermons">

                <div class="col-md-3 col-sm-3">
                    <div class="temple-resident wow fadeInLeft text-center" data-wow-delay="0.6s"
                         style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp; ">
                        <div class="content"><a>Chanakya Pandit Das</a></div>
                        <img
                                src="images/unknown-thumbnail.jpg">
                    </div>
                </div>

                <div class="col-md-3 col-sm-3">
                    <div class="temple-resident wow fadeInLeft text-center" data-wow-delay="0.6s"
                         style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp; ">
                        <div class="content"><a>Nitai Chand Das</a></div>
                        <img
                                src="images/unknown-thumbnail.jpg">
                    </div>
                </div>

                <div class="col-md-3 col-sm-3">
                    <div class="temple-resident wow fadeInLeft text-center" data-wow-delay="0.6s"
                         style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp; ">
                        <div class="content"><a>Gurudayal Das</a></div>
                        <img
                                src="images/unknown-thumbnail.jpg">
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="temple-resident wow fadeInLeft text-center" data-wow-delay="0.6s"
                         style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp; ">
                        <div class="content"><a>Punya Thirtha Das</a></div>
                        <img
                                src="images/unknown-thumbnail.jpg">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
