<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Ekadashi Dates and Time in Columbus, OH">
    <meta name="keywords"
          content="Columbus ISKCON festivals, Ekadashi dates in Columbus, Ekadashi in ISKCON Columbus, Ekadashi Ohio">

    <link rel="profile" href="https://gmpg.org/xfn/11">

    <style type="text/css">
        a.disabled {
            pointer-events: none;
            cursor: default;
        }
    </style>
    <title>2020 Ekadashi Dates and Time in Columbus, OH</title>
    <?php
    include('meta_links.php');
    ?>
</head>

<body class="events-template-default single single-events postid-74 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">

    <?php
    include('header.php');
    ?>
    <!-- Blog Heading -->
    <section id="blog-heading">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text">
                    <h1 class="blog-title text-center">2020 Ekadashi dates in Columbus, OH</h1>
                </div><!-- /.heading-text -->
            </div><!-- /.container -->
        </div><!-- /.heading-fullwidth -->
    </section><!-- /#blog-heading -->
    <!-- Blog Heading end -->

    <div class="vc_row-fluid">
        <div class="vc_col-sm-12 wpb_column vc_column_container text-center">
            <div class="wpb_wrapper">
                <section class="shedule section-padding" style="background: url(images/Untitled-1-1.jpg);">
                    <!--                <section class="shedule section-padding" style="background: url(images/vishnu.jpg);">-->
                    <div class="container">
                        <div class="row">

                            <!--                            <iframe width="571" height="293" src="https://w2.countingdownto.com/2808075" frameborder="0"></iframe>-->
                            <div class="disabled">

                                <!--                                       April 18-->
                                <div data-type="countdown" data-id="1850571" class="tickcounter"
                                     style="width: 100%; position: relative; padding-bottom: 25%"><a
                                            href="//www.tickcounter.com/countdown/1850571/next-ekadashi-in"
                                            title="Next Ekadashi in">Next Ekadashi in</a><a href="//www.tickcounter.com/"
                                                                                            title="Countdown">Countdown</a></div>
                                <script>(function (d, s, id) {
                                        var js, pjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = "//www.tickcounter.com/static/js/loader.js";
                                        pjs.parentNode.insertBefore(js, pjs);
                                    }(document, "script", "tickcounter-sdk"));</script>

                                <!--                                May 3rd-->
                                <!--                                <div data-type="countdown" data-id="1903034" class="tickcounter"-->
                                <!--                                     style="width: 100%; position: relative; padding-bottom: 25%"><a-->
                                <!--                                            href="//www.tickcounter.com/countdown/1903034/next-ekadashi-in"-->
                                <!--                                            title="Next Ekadashi in">Next Ekadashi in</a><a href="//www.tickcounter.com/"-->
                                <!--                                                                                            title="Countdown">Countdown</a></div>-->
                                <!--                                <script>(function (d, s, id) {-->
                                <!--                                        var js, pjs = d.getElementsByTagName(s)[0];-->
                                <!--                                        if (d.getElementById(id)) return;-->
                                <!--                                        js = d.createElement(s);-->
                                <!--                                        js.id = id;-->
                                <!--                                        js.src = "//www.tickcounter.com/static/js/loader.js";-->
                                <!--                                        pjs.parentNode.insertBefore(js, pjs);-->
                                <!--                                    }(document, "script", "tickcounter-sdk"));</script>-->

                                <!--                                May 18-->
                                <!--                                <div data-type="countdown" data-id="1903035" class="tickcounter"-->
                                <!--                                     style="width: 100%; position: relative; padding-bottom: 25%"><a-->
                                <!--                                            href="//www.tickcounter.com/countdown/1903035/next-ekadashi-in"-->
                                <!--                                            title="Next Ekadashi in">Next Ekadashi in</a><a href="//www.tickcounter.com/"-->
                                <!--                                                                                            title="Countdown">Countdown</a></div>-->
                                <!--                                <script>(function (d, s, id) {-->
                                <!--                                        var js, pjs = d.getElementsByTagName(s)[0];-->
                                <!--                                        if (d.getElementById(id)) return;-->
                                <!--                                        js = d.createElement(s);-->
                                <!--                                        js.id = id;-->
                                <!--                                        js.src = "//www.tickcounter.com/static/js/loader.js";-->
                                <!--                                        pjs.parentNode.insertBefore(js, pjs);-->
                                <!--                                    }(document, "script", "tickcounter-sdk"));</script>-->

                                <!--                                June 2-->
                                <!--                                <div data-type="countdown" data-id="1903036" class="tickcounter"-->
                                <!--                                     style="width: 100%; position: relative; padding-bottom: 25%"><a-->
                                <!--                                            href="//www.tickcounter.com/countdown/1903036/next-ekadashi-in"-->
                                <!--                                            title="Next Ekadashi in">Next Ekadashi in</a><a href="//www.tickcounter.com/"-->
                                <!--                                                                                            title="Countdown">Countdown</a></div>-->
                                <!--                                <script>(function (d, s, id) {-->
                                <!--                                        var js, pjs = d.getElementsByTagName(s)[0];-->
                                <!--                                        if (d.getElementById(id)) return;-->
                                <!--                                        js = d.createElement(s);-->
                                <!--                                        js.id = id;-->
                                <!--                                        js.src = "//www.tickcounter.com/static/js/loader.js";-->
                                <!--                                        pjs.parentNode.insertBefore(js, pjs);-->
                                <!--                                    }(document, "script", "tickcounter-sdk"));</script>-->
                            </div>


                            <!--                            --><?php
                            //                            include('partials/utility/ekadashi-countdown.html');
                            //                            ?>

                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </section>

            </div>
        </div>
    </div>


    <!--    --><?php //include "ekadashi.html";?>
    <!-- Ekadashi Schedule Image -->
    <div class="full-width">
        <div class="vc_row-fluid">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">


                    <div class="wow fadeIn" data-wow-delay="0.4s"
                         style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp; width: 100%;"><img
                                src="images/ekadashi-2020.png"
                                alt=""
                                title="" style="height: 80%; width: 100%; margin-top: 0px; margin-left: -0.5px;">
                    </div>

                </div>
            </div>
        </div>
    </div>


    <?php
    include('footer.php');
    ?>


</body>
</html>
