<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <title>Pranam prayers - Prayers to Krishna and Guru - ISKCON Pranam prayers
    </title>
    <?php
    include('meta_links.php');
    ?>
</head>
<body class="product-template-default single single-product postid-247 _masterslider _msp_version_3.0.4 woocommerce woocommerce-page wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- /#preloader -->
<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php include('header.php'); ?>

    <section id="shop" style="padding:0 !important;">

        <div class="container">
            <!-- festival -->
            <div class="full-width">
                <div class="vc_row-fluid">
                    <div class="vc_col-sm-12 wpb_column vc_column_container ">
                        <div class="wpb_wrapper">

                            <h2 class="section-title wow fadeInUp" style="padding: 15px 0 15px 0;"
                                data-wow-delay="0.4s"><i
                                        class="fa fa-pagelines fa-2x"></i>ISKCON Pranamas prayers </h2>
                            <!-- /.section-title -->

                            <div class="col-md-12 wow fadeInUp" data-wow-delay="0.4s" style="padding: 15px; text-align: center">
                                <ul class="list-group">
                                    <li class="list-group-item list-group-item-success">
                                        <h2>Srī Guru Praṇāma</h2>
                                    </li>
                                    <li class="list-group-item list-group-item-info">
                                        <p style="font-size: larger">oḿ ajñāna-timirāndhasya jñānāñjana-śalākayā</p>
                                        <p style="font-size: larger">cakṣur unmīlitaḿ yena tasmai śrī-gurave namaḥ</p>
                                    </li>
                                    <li class="list-group-item list-group-item-warning">
                                        <h3>TRANSLATION</h3>
                                        <p style="font-size: larger"> I offer my respectful obeisances unto my spiritual master, who
                                            has opened my eyes, which were blinded by the darkness of ignorance, with the torchlight
                                            of knowledge.</p>

                                    </li>
                                </ul>

                                <ul class="list-group">
                                    <li class="list-group-item list-group-item-success">
                                        <h2>Srī Rūpa Praṇāma</h2>
                                    </li>
                                    <li class="list-group-item list-group-item-info">
                                        <p style="font-size: larger">śrī-caitanya-mano-'bhīṣṭaḿ sthāpitaḿ yena bhū-tale</p>
                                        <p style="font-size: larger">svayaḿ rūpaḥ kadā mahyaḿ dadāti sva-padāntikam</p>
                                    </li>
                                    <li class="list-group-item list-group-item-warning">
                                        <h3>TRANSLATION</h3>
                                        <p style="font-size: larger"> When will Srīla Rūpa Gosvāmī Prabhupāda, who has established
                                            within this material world the mission to fulfill the desire of Lord Caitanya, give me
                                            shelter under his lotus feet?
                                        </p>

                                    </li>
                                </ul>

                                <ul class="list-group">
                                    <li class="list-group-item list-group-item-success">
                                        <h2>Mańgalācaraṇa</h2>
                                    </li>
                                    <li class="list-group-item list-group-item-info">
                                        <p style="font-size: larger">vande 'haḿ śrī-guroḥ śrī-yuta-pada-kamalaḿ śrī-gurun
                                            vaiṣṇavāḿś ca</p>
                                        <p style="font-size: larger">śrī-rūpaḿ sāgrajātaḿ saha-gaṇa-raghunāthānvitaḿ taḿ sa
                                            jīvam</p>
                                        <p style="font-size: larger">sādvaitaḿ sāvadhūtaḿ parijana-sahitaḿ
                                            kṛṣṇa-caitanya-devaḿ</p>
                                        <p style="font-size: larger">śrī-rādhā-kṛṣṇa-pādān saha-gaṇa-lalitā- śrī-viśākhānvitāḿś
                                            ca</p>
                                    </li>
                                    <li class="list-group-item list-group-item-warning">
                                        <h3>TRANSLATION</h3>
                                        <p style="font-size: larger"> I offer my respectful obeisances unto the lotus feet of my
                                            spiritual master and of all the other preceptors on the path of devotional service. I
                                            offer my respectful obeisances unto all the Vaiṣṇavas and unto the six Gosvāmīs,
                                            including Srīla Rūpa Gosvāmī, Srīla Sanātana Gosvāmī, Raghunātha dāsa Gosvāmī, Jīva
                                            Gosvāmī, and their associates. I offer my respectful obeisances unto Advaita Acārya
                                            Prabhu, Srī Nityānanda Prabhu, Srī Caitanya Mahāprabhu, and all His devotees, headed by
                                            Srīvāsa Thākura. I then offer my respectful obeisances unto the lotus feet of Lord
                                            Kṛṣṇa, Srīmatī Rādhārānī, and all the gopīs, headed by Lalitā and Viśākhā.</p>
                                    </li>
                                </ul>

                                <ul class="list-group">
                                    <li class="list-group-item list-group-item-success">
                                        <h2>Śrīla Prabhupāda Praṇati</h2>
                                    </li>
                                    <li class="list-group-item list-group-item-info">
                                        <p style="font-size: larger">nama oḿ viṣṇu-pādāya kṛṣṇa-preṣṭhāya bhū-tale</p>
                                        <p style="font-size: larger">śrīmate bhaktivedānta-svāmin iti nāmine</p>
                                    </li>
                                    <li class="list-group-item list-group-item-warning">
                                        <h3>TRANSLATION</h3>
                                        <p style="font-size: larger"> I offer my respectful obeisances unto His Divine Grace A. C.
                                            Bhaktivedanta Swami Prabhupāda, who is very dear to Lord Kṛṣṇa, having taken shelter
                                            at His lotus feet.</p>
                                    </li>
                                    <li class="list-group-item list-group-item-info">
                                        <p style="font-size: larger">namas te sārasvate deve gaura-vāṇī-pracāriṇe</p>
                                        <p style="font-size: larger">nirviśeṣa-śūnyavādi-pāścātya-deśa-tāriṇe</p>
                                    </li>
                                    <li class="list-group-item list-group-item-warning">
                                        <h3>TRANSLATION</h3>
                                        <p style="font-size: larger">Our respectful obeisances are unto you, O spiritual master,
                                            servant of Sarasvatī Gosvāmī. You are kindly preaching the message of Lord Caitanyadeva
                                            and delivering the Western countries, which are filled with impersonalism and
                                            voidism.</p>
                                    </li>
                                </ul>

                                <ul class="list-group">
                                    <li class="list-group-item list-group-item-success">
                                        <h2>Śrī Vaiṣṇava Praṇāma</h2>
                                    </li>
                                    <li class="list-group-item list-group-item-info">
                                        <p style="font-size: larger">vāñchā-kalpatarubhyaś ca kṛpā-sindhubhya eva ca</p>
                                        <p style="font-size: larger">patitānāḿ pāvanebhyo vaiṣṇavebhyo namo namaḥ</p>
                                    </li>
                                    <li class="list-group-item list-group-item-warning">
                                        <h3>TRANSLATION</h3>
                                        <p style="font-size: larger">I offer my respectful obeisances unto all the Vaiṣṇava
                                            devotees of the Lord. They are just like desire trees who can fulfill the desires of
                                            everyone, and they are full of compassion for the fallen conditioned souls.</p>
                                    </li>
                                </ul>

                                <ul class="list-group">
                                    <li class="list-group-item list-group-item-success">
                                        <h2>Pañca-tattva Mahā-mantra</h2>
                                    </li>
                                    <li class="list-group-item list-group-item-info">
                                        <p style="font-size: larger">(jaya) śrī-kṛṣṇa-caitanya prabhu nityānanda</p>
                                        <p style="font-size: larger">śrī-advaita gadādhara śrīvāsādi-gaura-bhakta-vṛnda</p>
                                    </li>
                                    <li class="list-group-item list-group-item-warning">
                                        <h3>TRANSLATION</h3>
                                        <p style="font-size: larger">Srī Caitanya Mahāprabhu is always accompanied by His plenary
                                            expansion Srī Nityānanda Prabhu, His incarnation Srī Advaita Prabhu, His internal
                                            potency Srī Gadādhara Prabhu, and His marginal potency Srīvāsa Prabhu. He is in the
                                            midst of them as the Supreme Personality of Godhead.</p>
                                    </li>
                                </ul>

                                <ul class="list-group">
                                    <li class="list-group-item list-group-item-success">
                                        <h2>Hare Kṛṣṇa Mahā-mantra</h2>
                                    </li>
                                    <li class="list-group-item list-group-item-info">
                                        <p style="font-size: larger">Hare Kṛṣṇa Hare Kṛṣṇa Kṛṣṇa Kṛṣṇa Hare Hare</p>
                                        <p style="font-size: larger">Hare Rāma Hare Rāma Rāma Rāma Hare Hare</p>
                                    </li>
                                    <li class="list-group-item list-group-item-warning">
                                        <h3>TRANSLATION</h3>
                                        <p style="font-size: larger">The word Harā is the form of addressing the energy of the Lord,
                                            and the words Kṛṣṇa and Rāma are forms of addressing the Lord Himself. Both Kṛṣṇa
                                            and Rāma mean "the supreme pleasure," and Harā is the supreme pleasure energy of the
                                            Lord, changed to Hare in the vocative. The supreme pleasure energy of the Lord helps us
                                            to reach the Lord.</p>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end festivals -->
        </div>

        <?php
        include('footer.php');
        ?>
</body>
</html>
