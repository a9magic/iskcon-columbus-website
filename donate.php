<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Donate</title>
    <?php
    $pageDescription = 'Support ISKCON Columbus in any way possible. Most importantly it increases your spiritual wealth!';
    $pageKeywords = 'ISKCON Columbus,  Columbus Krishna House, Columbus temple, Hindu Temple in Columbus, Radha Krishna, ISKCON Ohio, Radha Natabara, Donate';
    $pageCanonical = 'https://www.iskconcolumbus.com/donate.php';
    include("meta_links.php");
    ?>
</head>
<body class="page-template page-template-page-template page-template-event-page page-template-page-templateevent-page-php page page-id-402 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php
    include("header.php");
    ?>
    <section id="blog-heading" style="height:auto;border:1px solid #e1e1e1;">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text pull-left" style="width:99%;padding-top:41px;">
                    <h1 class="blog-title" style="text-align:center;font-size:31px;">Donate
                    </h1>
                </div>
                <!-- /.heading-text -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.heading-fullwidth -->
    </section>
    <section id="blog-page-container">
        <div class="blog-page-wrapper">
            <div id="primary" class="container">
                <main id="main" class="main-content" role="main">


                    <!-- Donate for New Temple -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">1
                        </span>
                                        <br>
                                        <span class="event-month">July
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="https://www.iskcongreatercolumbus.com/donate.php" target="_blank"
                                           title="Donate for New Temple">Donate for New Temple
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-map-marker">
                          </i> 3520 Walker Rd, Hilliard, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="https://www.iskcongreatercolumbus.com/donate.php" target="_blank">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/new-temple.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/new-temple.jpg 570w, images/new-temple.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- tithe -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">July
                        </span>
                                        <br>
                                        <span class="event-month">
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="tithe.php"
                                           title="Tithe">Tithe - Support Monthly
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-map-marker">
                          </i> 379 West 8th Ave Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="tithe.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/tithes.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/tithes.jpg 570w, images/tithes.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- sponsor Sunday Feast -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">18
                        </span>
                                        <br>
                                        <span class="event-month">June
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="sponsor-sunday-feast.php"
                                           title="Sponsor Festivals">Sponsor Sunday Feast
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-map-marker">
                          </i> 379 West 8th Ave Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="sponsor-sunday-feast.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/govindas-catering.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/govindas-catering.jpg 570w, images/govindas-catering.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- sponsor festivals -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">18
                        </span>
                                        <br>
                                        <span class="event-month">June
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="sponsor-festivals.php"
                                           title="Sponsor Festivals">Sponsor Festivals
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-map-marker">
                          </i> 379 West 8th Ave Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="sponsor-festivals.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/sponsor-festivals.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/sponsor-festivals.jpg 570w, images/sponsor-festivals.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Sponsor book distribution -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">8
                        </span>
                                        <br>
                                        <span class="event-month">July
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="sponsor-book-distribution.php"
                                           title="Sponsor Book Distribution">Sponsor Book Distribution
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-map-marker">
                          </i> Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="sponsor-book-distribution.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/sponsor-book-distribution.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/sponsor-book-distribution.jpg 570w, images/sponsor-book-distribution.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- general Donation -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">June
                        </span>
                                        <br>
                                        <span class="event-month">
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="general-donation.php"
                                           title="One Time Donation">One Time Donation
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-map-marker">
                          </i> 379 West 8th Ave Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="general-donation.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/radha-natabara.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/radha-natabara.jpg 570w, images/radha-natabara.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Amazon Smile -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="all-event">
                                <div class="event-post">
                                    <h2 class="uppercase">
                                        <a href="amazon-smile.php"
                                           title="Amazon Smile Charity">Amazon Smile Charity
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-map-marker">
                          </i> 379 West 8th Ave Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="amazon-smile.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="images/amazon.png"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="images/amazon.png 570w, images/amazon.png 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </
                >
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
    </section>

    <?php
    include("footer.php");
    ?>
</div>
<!-- /.peace layout end -->

</body>
</html>
