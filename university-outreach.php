<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="//gmpg.org/xfn/11">

    <title>University Outreach</title>
    <?php
    include('meta_links.php');
    ?>
</head>

<body class="events-template-default single single-events postid-74 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">

    <?php
    include('header.php');
    ?>
    <!-- Blog Heading -->
    <section id="blog-heading">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text">
                    <h1 class="blog-title text-center">University Outreach</h1>
                </div><!-- /.heading-text -->
            </div><!-- /.container -->
        </div><!-- /.heading-fullwidth -->
    </section><!-- /#blog-heading -->
    <!-- Blog Heading end -->

    <!-- Blog Page Container -->
    <section id="blog-page-container">
        <div class="blog-page-wrapper">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 wow fadeInLeft" data-wow-delay="0.2s">

                        <div class="sermons">

                            <div class="wow fadeInUp" data-wow-delay="0.2s">
                                <div class="container">

                                    <script>

                                        jQuery(window).on("load resize", function (e) {

                                            var $container = jQuery('.isotope-items'),
                                                colWidth = function () {
                                                    var w = $container.width(),
                                                        columnNum = 1,
                                                        columnWidth = 0;
                                                    if (w > 1040) {
                                                        columnNum = 4;
                                                    }
                                                    else if (w > 850) {
                                                        columnNum = 2;
                                                    }
                                                    else if (w > 768) {
                                                        columnNum = 2;
                                                    }
                                                    else if (w > 480) {
                                                        columnNum = 2;
                                                    }
                                                    columnWidth = Math.floor(w / columnNum);

                                                    //Isotop Version 1
                                                    var $containerV1 = jQuery('.isotope-items');
                                                    $containerV1.find('.item').each(function () {
                                                        var $item = jQuery(this),
                                                            multiplier_w = $item.attr('class').match(/item-w(\d)/),
                                                            multiplier_h = $item.attr('class').match(/item-h(\d)/),
                                                            width = multiplier_w ? columnWidth * multiplier_w[1] - 10 : columnWidth,
                                                            height = multiplier_h ? columnWidth * multiplier_h[1] * 0.7 : columnWidth * 0.7;
                                                        $item.css({width: width, height: height,});
                                                    });


                                                    return columnWidth;
                                                },
                                                isotope = function () {
                                                    $container.isotope({
                                                        resizable: true,
                                                        itemSelector: '.item',
                                                        masonry: {
                                                            columnWidth: colWidth(),
                                                            gutterWidth: 10
                                                        }
                                                    });
                                                };
                                            isotope();


                                            // bind filter button click
                                            jQuery('.isotope-filters').on('click', 'button', function () {
                                                var filterValue = jQuery(this).attr('data-filter');
                                                $container.isotope({filter: filterValue});
                                            });

// change active class on buttons
                                            jQuery('.isotope-filters').each(function (i, buttonGroup) {
                                                var $buttonGroup = jQuery(buttonGroup);
                                                $buttonGroup.on('click', 'button', function () {
                                                    $buttonGroup.find('.active').removeClass('active');
                                                    jQuery(this).addClass('active');
                                                });
                                            });


// Masonry Isotope
                                            var $masonryIsotope = jQuery('.isotope-masonry-items').isotope({
                                                itemSelector: '.item',
                                            });

// bind filter button click
                                            jQuery('.isotope-filters').on('click', 'button', function () {
                                                var filterValue = jQuery(this).attr('data-filter');
                                                $masonryIsotope.isotope({filter: filterValue});
                                            });

// change active class on buttons
                                            jQuery('.isotope-filters').each(function (i, buttonGroup) {
                                                var $buttonGroup = jQuery(buttonGroup);
                                                $buttonGroup.on('click', 'button', function () {
                                                    $buttonGroup.find('.active').removeClass('active');
                                                    jQuery(this).addClass('active');
                                                });
                                            });
                                        });
                                    </script>

                                    <div id="category" class="isotope-filters portfolio-filter">

                                        <button class="button is-checked " data-sort-by="original-order">
                                            ALL
                                        </button>
                                        <button data-filter="htb">Higher Taste Beats</button>
                                        <button data-filter="cooking">OSU Cooking Workshop</button>
                                        <button data-filter="thurday">Thoughtful Thursday</button>
                                    </div>
                                </div>


                                <!-- Higher Taste Beats -->
                                <div class="portal item common wow fadeInUp" data-cat="htb" data-wow-delay="0.2s"
                                     style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp; background: #ffffff;">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Higher Taste Beats, ISKCON Columbus</h3>
                                        <hr/>
                                    </div>

                                    <div class="col-md-4">
                                        <img width="300" height="300"
                                             src="images/sacred.jpg"
                                             class="attachment-thumbnail size-thumbnail wp-post-image" alt=""
                                             srcset="images/sacred.jpg 180w, images/sacred.jpg 300w"
                                             sizes="(max-width: 300px) 100vw, 150px">
                                    </div>

                                    <div class="col-md-4 text-center" style="padding-top: 20px;">

                                        <div class="event-detail">
                                            <h4 class="widget-title">Shrestha, Alek, &amp; ISKCON Columbus Youth</h4>
                                            Musical Mantra Meditation - Relaxing techniques, Interactive sessions
                                            discussing benifits of Mantra Meditation, dance & free snacks serve out.
                                            <hr/>
                                            <div class="address text-left" style="padding-top: 25px;">
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-map-marker base-color"></i><strong>
                                                            Where:</strong>
                                                        ISKCON Columbus (off-campus right across OSU Wexner Medical Center)
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-phone base-color"></i><strong>
                                                            Call:</strong>
                                                        (352) 665-3878
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-calendar-o base-color"></i><strong>
                                                            When</strong> Every Friday 7.00 PM
                                                        – 8.00 PM
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-left"><i class="fa fa-money base-color"></i>
                                                    </div>
                                                    <div class="media-body"><strong>Cost:</strong> FREE <br></div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-users base-color"></i><strong>
                                                            Who:</strong> Everyone is invited to
                                                        attend
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="eventMap eventMap3"></div>
                                        <script>
                                            jQuery(document).ready(function () {
                                                var teamID = jQuery(".eventMap3");
                                                if (teamID.length) {
                                                    function isMobile() {
                                                        return ('ontouchstart' in document.documentElement);
                                                    }

                                                    function init_gmap() {
                                                        if (typeof google == 'undefined') return;
                                                        var options = {
                                                            center: [39.9916837, -83.017105],
                                                            zoom: 15,
                                                            mapTypeControl: true,
                                                            mapTypeControlOptions: {
                                                                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                                                            },
                                                            navigationControl: true,
                                                            scrollwheel: false,
                                                            streetViewControl: true
                                                        }

                                                        if (isMobile()) {
                                                            options.draggable = false;
                                                        }

                                                        jQuery('.eventMap3').gmap3({
                                                            map: {
                                                                options: options
                                                            },
                                                            marker: {
                                                                latLng: [39.9916837, -83.017105],
                                                                options: {icon: ""}
                                                            }
                                                        });
                                                    }

                                                    init_gmap();
                                                }

                                            });

                                        </script>
                                        <div>
                                            <form action="//maps.google.com/maps" method="get" target="_blank">
                                                <input type="text" placeholder="Enter Your Location" name="saddr"/>
                                                <input type="hidden" name="daddr"
                                                       value="ISKCON Columbus"/>
                                                <input type="submit" class="btn custom-btn"
                                                       style="color: white; border-radius:20px;"
                                                       value="Get directions"/>
                                            </form>
                                        </div>
                                    </div>

                                </div>

                                <!-- Cooking Workshop -->
                                <div class="portal item cooking wow fadeInUp" data-cat="cooking" data-wow-delay="0.2s"
                                     style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp; background: #ffffff;">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Cooking Workshop, Ohio State Univeristy</h3>
                                        <hr/>
                                    </div>

                                    <div class="col-md-4">
                                        <img width="300" height="300"
                                             src="images/cooking.jpg"
                                             class="attachment-thumbnail size-thumbnail wp-post-image" alt=""
                                             srcset="images/cooking.jpg 180w, images/cooking.jpg 300w"
                                             sizes="(max-width: 300px) 100vw, 150px">
                                    </div>

                                    <div class="col-md-4 text-center" style="padding-top: 20px;">

                                        <div class="event-detail">
                                            <h4 class="widget-title">Naveen Krishna Das</h4>
                                            Learn to cook healthy delicious vegetarian recipes and learn how
                                            vegetarianism contributes to environmental, economical and health factors.
                                            Free classes & Dinner serve out
                                            <hr/>
                                            <div class="address text-left" style="padding-top: 25px;">
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-map-marker base-color"></i><strong>
                                                            Where:</strong>
                                                        OSU Campus, 2nd Floor RPAC Demo Kitchen
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-phone base-color"></i><strong>
                                                            Call:</strong>
                                                        614-316-8936
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-calendar-o base-color"></i><strong>
                                                            When:</strong> Every Thursday at <strong>7 PM -9 PM</strong>
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-left"><i class="fa fa-money base-color"></i>
                                                    </div>
                                                    <div class="media-body"><strong>Cost:</strong> FREE <br></div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-users base-color"></i><strong>
                                                            Who:</strong> Everyone is invited to
                                                        attend
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="eventMap eventMap1"></div>
                                        <script>
                                            jQuery(document).ready(function () {
                                                var teamID = jQuery(".eventMap1");
                                                if (teamID.length) {
                                                    function isMobile() {
                                                        return ('ontouchstart' in document.documentElement);
                                                    }

                                                    function init_gmap() {
                                                        if (typeof google == 'undefined') return;
                                                        var options = {
                                                            center: [39.9981187, -83.0194116],
                                                            zoom: 15,
                                                            mapTypeControl: true,
                                                            mapTypeControlOptions: {
                                                                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                                                            },
                                                            navigationControl: true,
                                                            scrollwheel: false,
                                                            streetViewControl: true
                                                        }

                                                        if (isMobile()) {
                                                            options.draggable = false;
                                                        }

                                                        jQuery('.eventMap1').gmap3({
                                                            map: {
                                                                options: options
                                                            },
                                                            marker: {
                                                                latLng: [39.9981187, -83.0194116],
                                                                options: {icon: ""}
                                                            }
                                                        });
                                                    }

                                                    init_gmap();
                                                }

                                            });

                                        </script>
                                        <div>
                                            <form action="//maps.google.com/maps" method="get" target="_blank">
                                                <input type="text" placeholder="Enter Your Location" name="saddr"/>
                                                <input type="hidden" name="daddr" value="325 W 6th Ave Columbus OH"/>
                                                <input type="submit" class="btn custom-btn"
                                                       style="color: white; border-radius:20px;"
                                                       value="Get directions"/>
                                            </form>
                                        </div>
                                    </div>

                                </div>

                                <!-- Sacred Sounds -->
                                <div class="portal item sacred wow fadeInUp" data-cat="thursday" data-wow-delay="0.2s"
                                     style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp; background: #ffffff;">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Thoughtful Thursday, ISKCON Columbus</h3>
                                        <hr/>
                                    </div>

                                    <div class="col-md-4">
                                        <img width="300" height="300"
                                             src="images/common.jpg"
                                             class="attachment-thumbnail size-thumbnail wp-post-image" alt=""
                                             srcset="images/common.jpg 180w, images/common.jpg 300w"
                                             sizes="(max-width: 300px) 100vw, 150px">
                                    </div>

                                    <div class="col-md-4 text-center" style="padding-top: 20px;">

                                        <div class="event-detail">
                                            <h4 class="widget-title">Prema Sindhu das </h4>
                                            Welcomes you to join him to discuss <strong>Spirituality</strong> practices
                                            and benifits of meditation at OSU Campus.
                                            <hr/>
                                            <div class="address text-left" style="padding-top: 25px;">
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-map-marker base-color"></i><strong>
                                                            Where:</strong>
                                                        ISKCON Columbus (off-campus right across OSU Wexner Medical Center)
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-phone base-color"></i><strong>
                                                            Call:</strong>
                                                        727-804-9836
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-calendar-o base-color"></i><strong>
                                                            When:</strong> Every Thursday at <strong>6.30
                                                            PM
                                                            – 8.00 PM</strong></div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-left"><i class="fa fa-money base-color"></i>
                                                    </div>
                                                    <div class="media-body"><strong>Cost:</strong> FREE <br></div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-users base-color"></i><strong>
                                                            Who:</strong> Everyone is invited to
                                                        attend
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="eventMap eventMap2"></div>
                                        <script>
                                            jQuery(document).ready(function () {
                                                var teamID = jQuery(".eventMap2");
                                                if (teamID.length) {
                                                    function isMobile() {
                                                        return ('ontouchstart' in document.documentElement);
                                                    }

                                                    function init_gmap() {
                                                        if (typeof google == 'undefined') return;
                                                        var options = {
                                                            center: [39.9916837, -83.017105],
                                                            zoom: 15,
                                                            mapTypeControl: true,
                                                            mapTypeControlOptions: {
                                                                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                                                            },
                                                            navigationControl: true,
                                                            scrollwheel: false,
                                                            streetViewControl: true
                                                        }

                                                        if (isMobile()) {
                                                            options.draggable = false;
                                                        }

                                                        jQuery('.eventMap2').gmap3({
                                                            map: {
                                                                options: options
                                                            },
                                                            marker: {
                                                                latLng: [39.9916837, -83.017105],
                                                                options: {icon: ""}
                                                            }
                                                        });
                                                    }

                                                    init_gmap();
                                                }

                                            });

                                        </script>
                                        <div>
                                            <form action="//maps.google.com/maps" method="get" target="_blank">
                                                <input type="text" placeholder="Enter Your Location" name="saddr"/>
                                                <input type="hidden" name="daddr"
                                                       value="ISKCON Columbus"/>
                                                <input type="submit" class="btn custom-btn"
                                                       style="color: white; border-radius:20px;"
                                                       value="Get directions"/>
                                            </form>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>

                </div><!-- row -->
            </div> <!-- #container -->
        </div><!-- #blog-page-wrapper -->
    </section>

    <?php
    include('footer.php');
    ?>


</body>
</html>
