<div class="full-width">
    <div class="vc_row-fluid vc_custom_1441957463603">
        <div class="vc_col-sm-12 wpb_column vc_column_container ">
            <div class="wpb_wrapper">
                <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span>
                </div>
                <h2 class="section-title wow fadeInUp" data-wow-delay="0.4s"> Visit Us </h2>

                <div class="map-container">
                    <div class="googleMaps google-map-container"></div>
                    <div class="hidden-xs contact-info col-sm-4 wow fadeInRight"
                         data - wow - delay="2s">
                        <div class="contact-person ">
                            <div class="info text-center">
                                <h3> ISKCON Columbus </h3>
                            </div><!-- /.info-->
                            <div>
                                <img align="center" width="150" height="150"
                                     src="images/iskcon-columbus.jpg"
                                     class="" alt="ISKCON Columbus">
                            </div>
                        </div><!-- /.contact - person-->

                        <div class="contact-information pull-left">
                            <h2></h2>
                            <div class="address">
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-map-marker base-color"></i></div>
                                    <div class="media-body">379 West 8th Ave Columbus, OH 43201<br></div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-phone-square base-color"></i></div>
                                    <div class="media-body">+1(614)421-1661<br></div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><i class="fa fa-clock-o base-color"></i></div>
                                    <div class="media-body"><strong>Temple Times :</strong> 05:00 am - 09:00
                                        pm<br/>
                                        <strong>Monday - Friday :</strong> Darshan Closed 1:00 pm - 4 pm<br/>
                                        <strong>Lunch everyday:</strong> 12:30 pm<br/>
                                        <strong>Sunday Feast:</strong> 11:00 AM
                                    </div>
                                </div>
                                <div>
                                    <form action="https://maps.google.com/maps" method="get" target="_blank">
                                        <input type="text" placeholder="Enter Your Location" name="saddr"/>
                                        <input type="hidden" name="daddr" value="379 West 8th Ave Columbus"/>
                                        <input type="submit" class="btn custom-btn"
                                               style="color: white; border-radius:20px;" value="Get directions"/>
                                    </form>
                                </div>
                            </div><!-- /.info -->
                        </div><!-- /.contact-person -->
                    </div>
                </div>

                <!-- visible for mobile view only -->
                <div class="visible-xs contact-info col-md-4 wow fadeInRight"
                     data - wow - delay="0.6s">
                    <div class="contact-person ">
                        <div class="info text-center">
                            <h3 class="text-center"> ISKCON Columbus </h3>
                        </div><!-- /.info-->
                        <div>
                            <img align="center" width="100%" height=auto
                                 src="images/iskcon-columbus.jpg"
                                 class="" alt="ISKCON Columbus">
                        </div>
                    </div><!-- /.contact - person-->

                    <div class="contact-information pull-left">
                        <h2></h2>
                        <div class="address">
                            <div class="media">
                                <div class="media-left"><i class="fa fa-map-marker base-color"></i></div>
                                <div class="media-body">379 West 8th Ave Columbus, OH 43201<br></div>
                            </div>
                            <div class="media">
                                <div class="media-left"><i class="fa fa-phone-square base-color"></i></div>
                                <div class="media-body"><a href="tel:+16144211661">+1(614)421-1661</a><br></div>
                            </div>
                            <div class="media">
                                <div class="media-left"><i class="fa fa-clock-o base-color"></i></div>
                                <div class="media-body"><strong>Temple Times :</strong> 05:00 am - 09:00
                                    pm<br/>

                                    <strong>Monday - Saturday :</strong> Darshan Closed 1:00 pm - 4 pm<br/>
                                    <strong>Lunch everyday:</strong> 12:30 pm<br/>
                                    <strong>Sunday Feast:</strong> 11:00 AM
                                </div>
                            </div>
                            <div>
                                <form action="https://maps.google.com/maps" method="get" target="_blank">
                                    <input type="text" placeholder="Enter Your Location" name="saddr"/>
                                    <input type="hidden" name="daddr" value="379 West 8th Ave Columbus"/>
                                    <input type="submit" class="btn custom-btn"
                                           style="color: white; border-radius:20px;" value="Get directions"/>
                                </form>
                            </div>
                        </div><!-- /.info -->
                    </div><!-- /.contact-person -->
                </div>
            </div>
            <!-- Google Map Section End -->
            <script type="text/javascript">

                jQuery(document).ready(function () {
                    var teamID = jQuery(".googleMaps");
                    if (teamID.length) {
                        /*----------- Google Map - with support of gmaps.js ----------------*/
                        function isMobile() {
                            return ('ontouchstart' in document.documentElement);
                        }

                        function init_gmap() {
                            if (typeof google == 'undefined') return;
                            var options = {
                                center: [39.9916837, -83.017105],
                                zoom: 18,
                                mapTypeControl: true,
                                mapTypeControlOptions: {
                                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                                },
                                navigationControl: true,
                                scrollwheel: false,
                                streetViewControl: true
                            }

                            if (isMobile()) {
                                options.draggable = true;
                            }

                            jQuery('.googleMaps').gmap3({
                                map: {
                                    options: options
                                },
                                marker: {
                                    latLng: [39.9916837, -83.017105],

                                }
                            });
                        }

                        init_gmap();
                    }

                });

            </script>

        </div>
    </div>
</div>
</div>
