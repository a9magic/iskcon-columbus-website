<!DOCTYPE html>
<html lang="en-US">
<head>

    <title>ISKCON Columbus</title>
    <?php

    $pageDescription = 'It is the desire of ISKCON Columbus and associates to be a place where all are welcomed, connected to Krishna, encouraged to grow in devotion to Him, enabled to practice Krishna consciousness based on the teachings of Srila AC Bhaktivedanta Swami Prabhupada, and equipped to propagate Krishna consciousness.';
    $pageKeywords = 'ISKCON Columbus,  Columbus Krishna House, Columbus temple, Hindu Temple in Columbus, Radha Krishna, ISKCON Ohio, Radha Natabara, Jagannataha, Baladeva, Subhadara';
    $pageCanonical = 'https://www.iskconcolumbus.com/';

    include("meta_links.php");
    ?>
    <style type="text/css" data-type="vc_shortcodes-custom-css">

        .vc_custom_1475847905924 {
            padding-top: 80px !important;
            padding-bottom: 80px !important;
            background-color: #f7f7f7 !important;
        }

        .vc_custom_1475847924477 {
            padding-bottom: 100px !important;
            background-color: #f7f7f7 !important;
        }

        .vc_custom_1475847937518 {
            padding-top: 115px !important;
            background-color: #f7f7f7 !important;
        }

        .vc_custom_1475847967102 {
            padding-top: 50px !important;
            background-color: #f7f7f7 !important;
        }

        .vc_custom_1475847994262 {
            padding-top: 55px !important;
            background-color: #ffffff !important;
        }

        .vc_custom_1475848013333 {
            padding-top: 110px !important;
            padding-bottom: 110px !important;
            background-color: #f7f7f7 !important;
        }

        .vc_custom_1475848026271 {
            padding-right: 0px !important;
            padding-left: 0px !important;
        }

        .vc_custom_1441954675195 {
            padding-bottom: 55px !important;
        }

        .vc_custom_1441955531097 {
            padding-bottom: 55px !important;
        }</style>
    <noscript>
        <style type="text/css"> .wpb_animate_when_almost_visible {
                opacity: 1;
            }</style>
    </noscript>
</head>

<body class="home page-template page-template-page-template page-template-front-page page-template-page-templatefront-page-php page page-id-336 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php
    include("header.php");
    ?>

</div>
<style type="text/css">

    .wow {
        visibility: visible;
    }

    #fixed-div {
        position: fixed;
        z-index: 20;
        bottom: 1em;
        right: -5em;
    }

    #slide {
        position: fixed;
        bottom: -80%;
        transition: 0.8s;
        -webkit-box-shadow: -7px 41px 103px 0px rgba(0, 0, 0, 1);
        -moz-box-shadow: -7px 41px 103px 0px rgba(0, 0, 0, 1);
        box-shadow: -7px 41px 103px 0px rgba(0, 0, 0, 1);
    }

    #fixed-div:hover #slide {
        transition: 0.5s;
        bottom: 0;
    }

</style>
<div id="fixed-div" class="hidden-xs">
    <div>
        <iframe id="slide" width="200" height="285" src="https://my.sendinblue.com/users/subscribe/js_id/30n98/id/3"
                frameborder="0"
                scrolling="auto" allowfullscreen style="display: block;margin-left: auto;margin-right: auto;"></iframe>
    </div>
    <div>
        <div id="side"><img width="60%" height="auto" src="images/email-template/newsletter.png"></div>
    </div>


</div>
<!-- top image -->
<div class="full-width">
    <div class="vc_row-fluid">
        <div class="vc_col-sm-12 wpb_column vc_column_container ">
            <div class="wpb_wrapper">
                <div class="" data-wow-delay="0.1s"
                     style="animation-delay: 0.2s; animation-name: fadeInUp width: 100%;"><img
                            src="images/coronavirus-columbus.png"
                            alt=""
                            title=""
                            style="height: 80%; width: 100%; margin-top: 0px; margin-left: -0.5px;">
                </div>
            </div>
        </div>
    </div>
</div>
<!--                            src="images/Background-1.jpg"-->

<!--Akshaya Tritiya-->
<!--<div class="vc_row-fluid">-->
<!--    <div class="vc_col-sm-12 wpb_column vc_column_container ">-->
<!--        <div class="wpb_wrapper">-->
<!--            <section class="shedule section-padding" style="background: url(images/corona-1.png);">-->
<!--                <div class="container">-->
<!--                    <div class="row">-->

<!--                        <div class="row  wow fadeInDown" data-wow-delay="0.1s">-->
<!--                            <h2><a href="/akshaya-tritiya.php" style="color:#e36200; font-size: larger"><strong>Happy Akshaya Tritiya!</strong> Watch the video below and get involved in this auspicious festival-->
<!--                                    for Sri Sri Radha Krishna</a></h2>-->
<!--                        </div>-->
<!--                        <div class="col-md-6 wow fadeInLeft" data-wow-delay="0.1s">-->
<!--                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/seIxDcN5x90" frameborder="0"-->
<!--                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
<!--                        </div>-->
<!---->
<!--                        <div class="col-md-6">-->
<!--                            <a target="_blank"-->
<!--                               href="/akshaya-tritiya.php">-->
<!--                                <img style="padding: 5px;" src="images/akshaya-tritya/akshaya-tritiya.png"/></a>-->
<!--                        </div>-->
<!---->
<!--                        <div class="col-md-12 col-sm-12 text-center">-->
<!--                            <div class="vc_box_shadow_border wow fadeInLeft text-center"-->
<!--                                 data-wow-delay="0.6s"-->
<!--                                 style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">-->
<!--                                <h2 style="padding: 10px;">Donate any amount and we will match it!</h2>-->
<!--                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post"-->
<!--                                      target="_blank">-->
<!--                                    <input type="hidden" name="cmd" value="_s-xclick">-->
<!--                                    <input type="hidden" name="hosted_button_id" value="K94FLQYGZG3W2">-->
<!--                                    <input type="image"-->
<!--                                           src="https://www.paypalobjects.com/WEBSCR-640-20110429-1/en_US/i/btn/btn_donateCC_LG.gif"-->
<!--                                           border="0" name="submit"-->
<!--                                           alt="PayPal - The safer, easier way to pay online!">-->
<!--                                    <img alt="" border="0"-->
<!--                                         src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif"-->
<!--                                         width="1" height="1">-->
<!--                                </form>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </section>-->
<!---->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<!-- Dispatch -->
<section>
    <div class="text-center" style="padding: 30px 0">
        <div class="row  wow fadeInDown" data-wow-delay="0.1s">
            <h2><a href="https://www.iskcongreatercolumbus.com" style="color:#e36200;">ISKCON Columbus NEW TEMPLE
                    project</a> featured on <strong><a target="_blank"
                                                       href="http://www.dispatch.com/news/20171013/hare-krishnas-new-temple-takes-environment-into-account"
                                                       style="color:#e36200;">The
                        Columbus Dispatch
                    </a></strong> Oct 13th, 2017, newsletter</h2>
        </div>
        <div class="col-md-6 wow fadeInLeft" data-wow-delay="0.1s">
            <h2 style="color:#000000;font-family:Satisfy;font-size:2em;position:relative">
                <a target="_blank"
                   href="http://www.dispatch.com/news/20171013/hare-krishnas-new-temple-takes-environment-into-account">Read
                    here</a><br/>
            </h2><a target="_blank"
                    href="http://www.dispatch.com/news/20171013/hare-krishnas-new-temple-takes-environment-into-account">
                <img style="padding: 5px;" src="images/dispatch.jpg"/></a>
        </div>
        <div class="col-md-6">
            <h2 style="color:#000000;font-family:Satisfy;font-size:2em;position:relative">
                Watch here<br/>
            </h2>
            <iframe width="80%" height="315" src="https://www.youtube.com/embed/YeixRhaFnxo" frameborder="0"
                    allowfullscreen></iframe>
        </div>
    </div>
    <!-- /.next-event -->
</section>
<!-- Our services-->
<div class="vc_row-fluid vc_custom_1475847905924">
    <div class="container">
        <div class="vc_col-sm-12 wpb_column vc_column_container ">
            <div class="wpb_wrapper">

                <h2 class="section-title wow fadeInUp" data-wow-delay="0.1s"
                    style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">Our Services</h2>
                <!-- /.section-title -->
                <div class="section-detail wow fadeInUp" data-wow-delay="0.1s"
                     style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">By rendering
                    devotional service unto the Personality of Godhead, Sri Krishna, one immediately acquires
                    causeless knowledge and detachment from the world. - <strong>Śrīmad-Bhāgavatam,
                        01.02.07 </strong>
                </div><!-- /.section-detail -->
                <div class="service">
                    <div class="col-md-3 service-details">
                        <div class="aarti aarti1 wow fadeInUp" data-wow-delay="0.1s"
                             style="visibility: visible; ">
                            <a href="festivals.php">
                                <div class="head">FESTIVALS</div>
                                <div class="contenet">
                            </a>
                            With myriad of colourful decorations, multi-course feasts, elaborate rituals,
                            kirtans, dance and dramas, the festivals makes spiritual progress a fun-filled
                            experience.
                        </div>
                    </div>
                    <div class="aarti aarti2 wow fadeInUp" data-wow-delay="0.1s"
                         style="visibility: visible; ">
                        <a href="sunday-feast.php">
                            <div class="head">SUNDAY FEAST</div>
                        </a>
                        <div class="contenet">
                            Ecstatic celebrations on Every Sunday. Please arrive at the temple by 11:00 AM to
                            participate in ecstatic devotional bhajans.
                        </div>
                    </div>
                    <div class="aarti aarti3 wow fadeInUp" data-wow-delay="0.1s"
                         style="visibility: visible; ">
                        <a href="university-outreach.php">
                            <div class="head">UNIVERSITY OUTREACH</div>
                        </a>
                        <div class="contenet">
                            ISKCON volunteers are running a club at Ohio State University under ISKCON Yoga
                            Circle by partnering with current OSU students.
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="provide"
                         style="visibility: visible; ">
                        <div class="circle"
                             style="visibility: visible; ">
                            <span><div class="text"><b>JOIN OUR</b><br/><strong>Family</strong></div></span>
                        </div>
                        <!-- Aarti -->
                        <div class="col-5">
                            <a href="festivals.php">
                                <div class="item hand"
                                     style="visibility: visible; ">
                                    <div class="item-element">
                                        <span class="fa fa-fire"></span>
                                    </div>
                                </div>
                            </a>
                        </div><!-- /.col-md-3 -->

                        <!-- Harinaam -->
                        <div class="col-5">
                            <a href="harinaam.php">
                                <div class="item hand1"
                                     style="visibility: visible; ">
                                    <div class="item-element">
                                        <span class="fa fa-american-sign-language-interpreting"></span>
                                    </div>
                                </div>
                            </a>
                        </div><!-- /.col-md-3 -->

                        <!-- Sunday Feast -->
                        <div class="col-5">
                            <a href="sunday-feast.php">
                                <div class="item hand2"
                                     style="visibility: visible; ">
                                    <div class="item-element">
                                        <span class="fa fa-heart"></span>
                                    </div>
                                </div>
                            </a>
                        </div><!-- /.col-md-3 -->

                        <!-- Book Distribution -->
                        <div class="col-5">
                            <a href="book-distribution.php">
                                <div class="item hand3">
                                    <div class="item-element">
                                        <span class="fa fa-book"></span>
                                    </div>
                                </div>
                            </a>
                        </div><!-- /.col-md-3 -->

                        <!-- University Outreach -->
                        <div class="col-5">
                            <a href="university-outreach.php">
                                <div class="item hand4">
                                    <div class="item-element">
                                        <span class="fa fa-graduation-cap"></span>
                                    </div>
                                </div>
                            </a>
                        </div><!-- /.col-md-3 -->

                        <!-- Sunday School-->
                        <div class="col-5">
                            <a href="sunday-school.php">
                                <div class="item hand5">
                                    <div class="item-element">
                                        <span class="fa fa-pagelines"></span>
                                    </div>
                                </div>
                            </a>
                        </div><!-- /.col-md-3 -->
                    </div>
                </div>
                <div class="col-md-3 service-details">
                    <div class="harinaam harinaam1 wow fadeInUp" data-wow-delay="0.1s"
                         style="visibility: visible; ">
                        <a href="harinaam.php">
                            <div class="head">HARINAAM</div>
                        </a>
                        <div class="contenet">
                            "...this sankirtana or street chanting must go on, it is our most important program.
                            Lord Caitanya's movement means the sankirtana movement.."
                        </div>
                    </div>
                    <div class="harinaam harinaam2 wow fadeInUp" data-wow-delay="0.1s"
                         style="visibility: visible; ">
                        <a href="book-distribution.php">
                            <div class="head">BOOK DISTRIBUTION</div>
                        </a>
                        <div class="contenet">
                            Book distribution of Vedic literature on weekends, special occasions and during
                            festivals by going out on streets, door to door and at different public places.
                        </div>
                    </div>
                    <div class="harinaam harinaam3 wow fadeInUp" data-wow-delay="0.1s"
                         style="visibility: visible; ">
                        <a href="sunday-school.php">
                            <div class="head">SUNDAY SCHOOL</div>
                        </a>
                        <div class="contenet">
                            Two Sunday school classes every Sunday for children with ages 4 to 14. Children are
                            taught Vedic knowledge in a very easy and practical way in a fun field environment.
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>

<!-- Temple Schedule -->
<div class="vc_row-fluid">
    <div class="vc_col-sm-12 wpb_column vc_column_container ">
        <div class="wpb_wrapper">
            <section class="shedule section-padding" style="background: url(images/Untitled-1-1.jpg);">
                <div class="container">
                    <div class="row">

                        <div class="col-md-4 wow fadeInLeft" data-wow-delay="0.1s">
                            <h2 class="section-title">Temple Schedule</h2>
                            <div class="section-detail">
                                <i class="fa fa-map-marker"></i> 379 West 8th Ave Columbus, OH 43201
                                <br/><br/>Lord Krishna’s temple is also the place where we can realize the magic of
                                a real guru. The <strong>Śrīmad-Bhāgavatam (11.3.21)</strong> says, “Any person who
                                seriously
                                desires real happiness must seek a bona fide spiritual master and take shelter of
                                him by initiation.”
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="shedule-list col-md-12 wow fadeInUp" data-wow-delay="0.1s">
                                <h2>Time schedule</h2>
                                <p></p>

                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#daily" style="color:#f57002;">Daily
                                            Schedule</a></li>
                                    <li><a data-toggle="tab" href="#thursday" style="color:#f57002;">Thursday
                                            Feast
                                        </a></li>
                                    <li><a data-toggle="tab" href="#sunday" style="color:#f57002
											;">Sunday Feast Gathering</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div id="daily" class="tab-pane fade in active">
                                        <table class="festivals table thead-inverse table-hover table-responsive">
                                            <thead>
                                            <tr>
                                                <th>
                                                    Time
                                                </th>
                                                <th>
                                                    Event
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    5:00 AM
                                                </td>
                                                <td>
                                                    Mangal Aarti
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    5:30 AM
                                                </td>
                                                <td>
                                                    Tulsi Puja
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    7:15 AM
                                                </td>
                                                <td>
                                                    Guru Puja
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    7:30 AM
                                                </td>
                                                <td>
                                                    Sringar Darshan
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    12:00 PM
                                                </td>
                                                <td>
                                                    Raj Bhoga Aarti
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    12:30 PM
                                                </td>
                                                <td>
                                                    Lunch Prasadam (Vegetarian meal offered to
                                                    the Lord)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    1:00 PM
                                                </td>
                                                <td>
                                                    Deities put to rest
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    1:00 PM to 4 PM
                                                </td>
                                                <td>
                                                    Darshan Closed
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    4:00 PM
                                                </td>
                                                <td>
                                                    Vaikalika Aarti
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    6:30 PM
                                                </td>
                                                <td>
                                                    Gaura Aarti
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    8:45 PM
                                                </td>
                                                <td>
                                                    Shayana Aarti
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    9:00pm
                                                </td>
                                                <td>
                                                    Darshan Closed
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="thursday" class="tab-pane fade in">
                                        <table class="festivals table thead-inverse table-hover table-responsive">
                                            <thead>
                                            <tr>
                                                <th>
                                                    Time
                                                </th>
                                                <th>
                                                    Event
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    6:30 PM
                                                </td>
                                                <td>
                                                    Bhajans
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    7:00 PM
                                                </td>
                                                <td>
                                                    Interactive discussion based on scriptures
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    7:30 PM
                                                </td>
                                                <td>
                                                    Kirtan
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    8:00 AM
                                                </td>
                                                <td>
                                                    Prasadam (Vegetarian meal offered to the Lord)
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="sunday" class="tab-pane fade in">
                                        <table class="festivals table thead-inverse table-hover table-responsive">
                                            <thead>
                                            <tr>
                                                <th>
                                                    Time
                                                </th>
                                                <th>
                                                    Event
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    11:00 PM
                                                </td>
                                                <td>
                                                    Bhajans
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    11:30 AM
                                                </td>
                                                <td>
                                                    Discourse from Vedic Scriptures
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    12:30 PM
                                                </td>
                                                <td>
                                                    Aarti & Kirtan
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    1:00 PM
                                                </td>
                                                <td>
                                                    Announcements
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    1:10 PM
                                                </td>
                                                <td>
                                                    Prasadam (Vegetarian meal offered to
                                                    the Lord)
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                            <div class="sunrise col-md-6 wow fadeInLeft" data-wow-delay="0.1s">
                                <div class="col-md-5">
                                    <img src="images/sun.png" alt="sunrise">
                                </div>
                                <div class="col-md-7">
                                    <h2>TEMPLE OPEN</h2>
                                    <span>5:00 am</span>
                                </div>
                            </div>
                            <div class="sunrise sunset col-md-6 wow fadeInRight" data-wow-delay="0.1s">
                                <div class="col-md-5">
                                    <img src="images/sun2.png" alt="sunset">
                                </div>
                                <div class="col-md-7">
                                    <h2>TEMPLE CLOSED</h2>
                                    <span>9:00 pm</span>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </section>

        </div>
    </div>
</div>

<!-- festival -->
<div class="full-width">
    <div class="vc_row-fluid">
        <div class="vc_col-sm-12 wpb_column vc_column_container ">
            <div class="wpb_wrapper">
                <div class="vc_empty_space" style="height: 115px"><span class="vc_empty_space_inner"></span>
                </div>


                <a target="_blank"
                   href="https://www.youtube.com/channel/UCbhrTLxLFZuH-n3Gz1HhJ-w?sub_confirmation=1">
                    <h2 class="section-title wow fadeInUp" data-wow-delay="0.1s"><i
                                class="fa fa-youtube fa-2x"></i> Watch
                        our Videos </h2>
                </a>
                <!-- /.section-title -->


                <div class="portfolio gallery-section wow fadeInUp" data-wow-delay="0.1s">
                    <div class="container">

                        <div class="isotope-filters portfolio-filter">

                            <button class="button is-checked" data-sort-by="original-order">ALL</button>
                            <button data-filter=".sundayFeast">Sunday Feast</button>
                            <button data-filter=".namayagna">Nama Yagna</button>
                            <button data-filter=".kirtans">Kirtans</button>
                            <button data-filter=".home-programs">Home Programs</button>

                        </div>
                    </div>
                    <div class="clearfix portfolio-item isotope-items isotope-masonry-items">

                        <div class="item  home-programs effect-oscar">
                            <img width="800" height="532" src="https://img.youtube.com/vi/30dkMLBerK8/mqdefault.jpg"
                                 class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                            <div class="item-description">
                                <div class="item-link">
                                    <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                       href="https://www.youtube.com/watch?v=30dkMLBerK8">
                                        <i class="fa fa-desktop"></i>
                                    </a>
                                </div><!-- /.item-link -->

                            </div>
                        </div>

                        <div class="item home-programs effect-oscar">
                            <img width="800" height="532" src="https://img.youtube.com/vi/o1rqSP1N6JI/mqdefault.jpg"
                                 class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                            <div class="item-description">
                                <div class="item-link">
                                    <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                       href="https://www.youtube.com/watch?v=o1rqSP1N6JI">
                                        <i class="fa fa-desktop"></i>
                                    </a>
                                </div><!-- /.item-link -->

                            </div>
                        </div>

                        <div class="item sundayFeast effect-oscar">
                            <img width="800" height="532" src="https://img.youtube.com/vi/e0LfkamYYJU/mqdefault.jpg"
                                 class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                            <div class="item-description">
                                <div class="item-link">
                                    <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                       href="https://www.youtube.com/watch?v=e0LfkamYYJU"
                                       target="_blank">
                                        <i class="fa fa-desktop"></i>
                                    </a>
                                </div><!-- /.item-link -->

                            </div>
                        </div>

                        <div class="item sundayFeast effect-oscar">
                            <img width="800" height="532" src="https://img.youtube.com/vi/o8aZRdKEVCE/mqdefault.jpg"
                                 class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                            <div class="item-description">
                                <div class="item-link">
                                    <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                       href="https://www.youtube.com/watch?v=o8aZRdKEVCE">
                                        <i class="fa fa-desktop"></i>
                                    </a>
                                </div><!-- /.item-link -->

                            </div>
                        </div>

                        <div class="item sundayFeast effect-oscar">
                            <img width="800" height="532" src="https://img.youtube.com/vi/hKhGu1XLsVk/mqdefault.jpg"
                                 class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                            <div class="item-description">
                                <div class="item-link">
                                    <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                       href="https://www.youtube.com/watch?v=hKhGu1XLsVk">
                                        <i class="fa fa-desktop"></i>
                                    </a>
                                </div><!-- /.item-link -->

                            </div>
                        </div>

                        <div class="item kirtans effect-oscar">
                            <img width="800" height="532" src="https://img.youtube.com/vi/Phi2IgLQaMA/mqdefault.jpg"
                                 class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                            <div class="item-description">
                                <div class="item-link">
                                    <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                       href="https://www.youtube.com/watch?v=Phi2IgLQaMA">
                                        <i class="fa fa-desktop"></i>
                                    </a>
                                </div><!-- /.item-link -->

                            </div>
                        </div>

                        <div class="item kirtans effect-oscar">
                            <img width="800" height="532" src="https://img.youtube.com/vi/QYWcuv3xcp8/mqdefault.jpg"
                                 class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                            <div class="item-description">
                                <div class="item-link">
                                    <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                       href="https://www.youtube.com/watch?v=QYWcuv3xcp8">
                                        <i class="fa fa-desktop"></i>
                                    </a>
                                </div><!-- /.item-link -->

                            </div>
                        </div>

                        <div class="item kirtans effect-oscar">
                            <img width="800" height="532" src="https://img.youtube.com/vi/ozhTXbAz1QI/mqdefault.jpg"
                                 class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                            <div class="item-description">
                                <div class="item-link">
                                    <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                       href="https://www.youtube.com/watch?v=ozhTXbAz1QI">
                                        <i class="fa fa-desktop"></i>
                                    </a>
                                </div><!-- /.item-link -->

                            </div>
                        </div>

                        <div class="item kirtans effect-oscar">
                            <img width="800" height="532" src="https://img.youtube.com/vi/0wO3pHKaABk/mqdefault.jpg"
                                 class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                            <div class="item-description">
                                <div class="item-link">
                                    <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                       href="https://www.youtube.com/watch?v=0wO3pHKaABk">
                                        <i class="fa fa-desktop"></i>
                                    </a>
                                </div><!-- /.item-link -->

                            </div>
                        </div>

                        <div class="item namayagna effect-oscar">
                            <img width="800" height="532" src="https://img.youtube.com/vi/6phMsWqeCHk/mqdefault.jpg"
                                 class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                            <div class="item-description">
                                <div class="item-link">
                                    <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                       href="https://www.youtube.com/watch?v=6phMsWqeCHk">
                                        <i class="fa fa-desktop"></i>
                                    </a>
                                </div><!-- /.item-link -->

                            </div>
                        </div>

                        <div class="item namayagna effect-oscar">
                            <img width="800" height="532" src="https://img.youtube.com/vi/BFKz_RbkbEA/mqdefault.jpg"
                                 class="attachment-gallery-thumb size-gallery-thumb wp-post-image"/>
                            <div class="item-description">
                                <div class="item-link">
                                    <a class="boxer" data-boxer-height="500" data-boxer-width="500"
                                       href="https://www.youtube.com/watch?v=BFKz_RbkbEA">
                                        <i class="fa fa-desktop"></i>
                                    </a>
                                </div><!-- /.item-link -->

                            </div>
                        </div>

                    </div><!-- /.YouTube videos -->

                    <a class="read-more"
                       href="https://www.youtube.com/channel/UCbhrTLxLFZuH-n3Gz1HhJ-w?sub_confirmation=1"
                       target="_blank">View
                        More</a>
                    <div class="clearfix"></div>
                </div><!-- /.gallery-section -->


                <script>
                    /*------------------------------------------------------------------------------------------------------------------*/
                    /*   isotop.
                     /*------------------------------------------------------------------------------------------------------------------*/
                    /*-------------------------- Isotope Init --------------------*/
                    jQuery(window).on("load resize", function (e) {

                        var $container = jQuery('.isotope-items'),
                            colWidth = function () {
                                var w = $container.width(),
                                    columnNum = 1,
                                    columnWidth = 0;
                                if (w > 1040) {
                                    columnNum = 4;
                                } else if (w > 850) {
                                    columnNum = 2;
                                } else if (w > 768) {
                                    columnNum = 2;
                                } else if (w > 480) {
                                    columnNum = 2;
                                }
                                columnWidth = Math.floor(w / columnNum);

                                //Isotop Version 1
                                var $containerV1 = jQuery('.isotope-items');
                                $containerV1.find('.item').each(function () {
                                    var $item = jQuery(this),
                                        multiplier_w = $item.attr('class').match(/item-w(\d)/),
                                        multiplier_h = $item.attr('class').match(/item-h(\d)/),
                                        width = multiplier_w ? columnWidth * multiplier_w[1] - 10 : columnWidth,
                                        height = multiplier_h ? columnWidth * multiplier_h[1] * 0.7 : columnWidth * 0.7;
                                    $item.css({width: width, height: height,});
                                });


                                return columnWidth;
                            },
                            isotope = function () {
                                $container.isotope({
                                    resizable: true,
                                    itemSelector: '.item',
                                    masonry: {
                                        columnWidth: colWidth(),
                                        gutterWidth: 10
                                    }
                                });
                            };
                        isotope();


                        // bind filter button click
                        jQuery('.isotope-filters').on('click', 'button', function () {
                            var filterValue = jQuery(this).attr('data-filter');
                            $container.isotope({filter: filterValue});
                        });

// change active class on buttons
                        jQuery('.isotope-filters').each(function (i, buttonGroup) {
                            var $buttonGroup = jQuery(buttonGroup);
                            $buttonGroup.on('click', 'button', function () {
                                $buttonGroup.find('.active').removeClass('active');
                                jQuery(this).addClass('active');
                            });
                        });


// Masonry Isotope
                        var $masonryIsotope = jQuery('.isotope-masonry-items').isotope({
                            itemSelector: '.item',
                        });

// bind filter button click
                        jQuery('.isotope-filters').on('click', 'button', function () {
                            var filterValue = jQuery(this).attr('data-filter');
                            $masonryIsotope.isotope({filter: filterValue});
                        });

// change active class on buttons
                        jQuery('.isotope-filters').each(function (i, buttonGroup) {
                            var $buttonGroup = jQuery(buttonGroup);
                            $buttonGroup.on('click', 'button', function () {
                                $buttonGroup.find('.active').removeClass('active');
                                jQuery(this).addClass('active');
                            });
                        });
                    });
                </script>


            </div>
        </div>
    </div>
</div>
<!-- end festivals -->

<?php
include("map.php");
?>

<div class="full-width">
    <div class="vc_row-fluid">
        <div class="vc_col-sm-12 wpb_column vc_column_container ">
            <div class="wpb_wrapper">
                <section class="quotes section-padding"
                         style="background-image: url(images/devotees.jpg)">
                    <div class="container">
                        <div class="col-md-11 centered">

                            <div class="prophet">
                                <div class="border-svg">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="0 0 223 257" style="enable-background:new 0 0 223 257;"
                                         xml:space="preserve">
						<style type="text/css">
                            .st0 {
                                fill: none;
                                stroke: #f1c152;
                                stroke-width: 2;
                                stroke-miterlimit: 10;
                            }
                        </style>
                                        <g>
                                            <path class="st0" d="M221.9,170.7c0.4,12.1-8.1,26.6-18.8,32.3l-17,9c-10.7,5.7-27.8,15.6-38.1,22l-18,11.3
								c-10.3,6.4-27.1,6.4-37.3,0L77.9,236c-10.3-6.4-27.4-16.3-38.1-22l-20.3-10.9c-10.7-5.7-18.9-20.3-18.4-32.4L2.2,151
								c0.6-12.1,0.6-31.9,0-44l-1-20.8C0.7,74.2,9,59.7,19.7,54.1l16.7-8.7c10.7-5.6,27.9-15.5,38.1-21.9l18.3-11.6
								C103,5.4,120,5,130.4,11.1l73.1,42.2c10.5,6,18.7,20.9,18.4,33l-0.6,19.1c-0.4,12.1-0.4,31.9,0,44L221.9,170.7z"/>
                                        </g>
						</svg>
                                </div>

                                <div class="svg-fill">
                                    <img src="images/hexa_fill_color.svg" alt="">
                                </div>

                                <div class="content">
                                    <span>Quote From</span>
                                    <h3>The Lord</h3>
                                </div>
                            </div>
                            <div class="ref col-md-8 wow fadeInRight" data-wow-delay="0.1s">
                                <p><strong>Krishna says: </strong> <i>"A person who accepts the path of
                                        devotional
                                        service is not bereft of the results derived from studying the Vedas,
                                        performing austere sacrifices, giving charity or pursuing philosophical
                                        and
                                        fruitive activities. At the end he reaches the supreme abode."</i></p>
                                <a href="http://www.vedabase.com/en/bg/8/28" target="_blank"
                                <div class="ref-location">Bhagavad Gita,
                                    <small> 8.28</small>
                                </div>
                                </a>
                            </div>

                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </section>

            </div>
        </div>
    </div>
</div>
<!-- quotes /.end -->

<?php
include("footer.php");
?>

</div>  <!-- /.peace layout end -->


</body>
</html>

