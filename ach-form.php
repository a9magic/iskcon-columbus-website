<?php include "includes/sendemail.php"; ?>
    <div class="col-md-12 col-sm-12 text-center" style="">
        <div class="text-center rightside">
            <button class="btn btn-success donate-btn" data-toggle="modal" data-target="#secondform">Direct Debit
                Collection
                Form
            </button>
        </div>
    </div>

    <div class="modal fade" role="dialog" id="thank">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h2>Thank you for your donation!</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="secondform" role="dialog" data-backdrop="static" data-keyboard="false" style="margin-top: 60px;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Direct Debit Collection Form</h4>
                </div>
                <div class="modal-body text-left">
                    <p>This form authorizes ISKCON Columbus (iskconcolumbus.com) to withdraw an amount specified by you
                        from
                        your checking or savings account, on a periodic basis. Using ACH mode to deduct money from your
                        Checking a/c will save 2.5% fees charged by financial institutions like Paypal/Credit
                        Cards/Square.
                        You can override previous authorizations any time, by submitting a new entry form, with your
                        modifications specified below.</p>
                    <br>
                    <form id="ach-form" method="post">
                        <div class="alert alert-danger" id="err2" style="display:none">
                            Please fill all required filds
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name <span class="red">*</span></label>
                            <input type="text" class="form-control" id="name" name="name" required="true"
                                   placeholder="Your Answer">
                        </div>

                        <div class="form-group">
                            <label>Amount <span class="red">*</span></label>
                            <p>Enter amount of dollars to be deducted every period.</p>
                            <input type="number" class="form-control" placeholder="Your Answer" id="amounts"
                                   name="amounts" required="true">
                        </div>
                        <div class="form-group">
                            <label>Donation Towards </label>
                            <p>Please specify which department you would like to apply your donation.</p>
                            <ul style="list-style:none">
                                <li><input type="checkbox" name="donationtowards[]" value="columbus"> Sri Sri Radha
                                    Natabara, ISKCON Columbus
                                </li>
                                <li><input type="checkbox" name="donationtowards[]" value="monthly"> Monthly Tithing
                                </li>
                                <li><input type="checkbox" name="donationtowards[]" value="sunday"> Sunday Feast</li>
                                <li><input type="checkbox" name="donationtowards[]" value="other"> Other</li>
                            </ul>
                        </div>
                        <div class="form-group">
                            <label>Start date</label>
                            <p>Default is immediately start on submission.</p>
                            <input type="date" class="form-control" placeholder="Your Answer" name="startdate"
                                   id="startdate2">
                        </div>
                        <div class="form-group">
                            <label>Frequency of donation</label>
                            <p>Default is monthly deduction from your account. Please select one of below option if you
                                don't prefer monthly deductions.</p>
                            <select class="form-control" name="frequencyofdonation" id="frequencyofdonation">
                                <option>Monthly</option>
                                <option>One Time Donation</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>End date</label>
                            <p>Please stop my deduction effective date....(leave it empty if you are not sure now, you
                                can
                                send email to iskconcolumbus@gmail anytime)</p>
                            <input type="date" class="form-control" id="start" name="end" placeholder="Your Answer">
                        </div>
                        <div class="form-group">
                            <label>Email <span class="red">*</span></label>
                            <input type="email" class="form-control" name="email2" id="email2" required="true" placeholder="Your Answer">
                        </div>
                        <div class="form-group">
                            <label>Street Address <span class="red">*</span></label>
                            <p>Address</p>
                            <input type="text" class="form-control" required="true" name="streetaddress" id="streetaddress"
                                   placeholder="Your Answer">
                        </div>

                        <div class="form-group">
                            <label>Phone number <span class="red">*</span></label>
                            <input type="tel" class="form-control" name="phone" required="true" id="phone2" placeholder="(555)-555-5555">
                        </div>
                        <div class="form-group">
                            <label>I hearby authorize ISKCON Columbus, 379 W 8th Ave, Columbus, OH 43201 to initiate a
                                debit
                                entry to my checking or savings account at the financial institution name above. I
                                acknowledge that the origination of ACH transactions to my account must comply with the
                                provisions of U.S.law. I understand I can cancel this authorization any time, by sending
                                an
                                email to iskconcolumbus@gmail.com <span class="red">*</span></label>
                            <p>Please enter your full legal name. The entry below will act as your Digital
                                Signature.</p>
                            <input type="text" class="form-control" required="true" name="sign" id="sign2" placeholder="Sign here">
                        </div>
                        <h6>Hare Krishna Hare Krishna Krishna Krishna Hare Hare || <br/>
                            Hare Rama Hare Rama Rama Rama Hare Hare</h6>
                        <br>
                        <button type="submit" name="form1" class="btn center-block btn-success"
                                onclick="return val2();">Submit
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <style type="text/css">
        .modal-content {
            border-radius: 0px !important;
        }

        .modal-header {
            background: #FFBC01;
            color: #fff;
            border-color: #FFBC01;
            font-weight: bold !important;
        }

        .modal-body {
            padding: 40px;
        }

        .modal-body .form-group {
            margin-bottom: 25px;
        }
    </style>

    <script type="text/javascript">

        $('#ach-form').submit(function(e){
            alert("test test");
            e.preventDefault();
            $('#thank').modal('show');
            return false;
        });

        function val2() {
//            flag = true;
//            var firstname = $('#name').val();
//            var amount = $('#amounts').val();
//            var bankroutingnumber = $('#bankroutingnumber').val();
//            var bankaccountnumber = $('#bankaccountnumber').val();
//            var startdate = $('#startdate2').val();
//            var frequencyofdonation = $('#frequencyofdonation').val();
//            var start = $('#start').val();
//            var email = $('#email2').val();
//            var streetaddress = $('#streetaddress').val();
//            var apt = $('#apt').val();
//            var city = $('#city').val();
//            var state = $('#state').val();
//            var zip = $('#zip2').val();
//            var phone = $('#phone2').val();
//            var sign = $('#sign2').val();
//
//            if (firstname == '') {
//                alert('a');
//                $('#err2').show();
//                flag = false;
//            }
//            if (amount == '') {
//                alert('b');
//                $('#err2').show();
//                flag = false;
//            }
//
//            if (bankroutingnumber == '') {
//                alert('c');
//                $('#err2').show();
//                flag = false;
//            }
//            if (bankaccountnumber == '') {
//                alert('d');
//                $('#err2').show();
//                flag = false;
//            }
//            if (startdate == '') {
//                alert('e');
//                $('#err2').show();
//                flag = false;
//            }
//            if (frequencyofdonation == '') {
//                alert('f');
//                $('#err2').show();
//                flag = false;
//            }
//            if (email == '') {
//                alert('g');
//                $('#err2').show();
//                flag = false;
//            }
//            if (streetaddress == '') {
//                alert('h');
//                $('#err2').show();
//                flag = false;
//            }
//            if (apt == '') {
//                alert('i');
//                $('#err2').show();
//                flag = false;
//            }
//            if (city == '') {
//                alert('j');
//                $('#err2').show();
//                flag = false;
//            }
//            if (state == '') {
//                alert('k');
//                $('#err2').show();
//                flag = false;
//            }
//            if (zip == '') {
//                alert('l');
//                $('#err2').show();
//                flag = false;
//            }
//            if (phone == '') {
//                alert('m');
//                $('#err2').show();
//                flag = false;
//            }
//            if (sign == '') {
//                alert('n');
//                $('#err2').show();
//                flag = false;
//            }
//            $('#thank').modal('show');
//            alert("Thank you for your donation! Hare Krishna!");
            return true;
        }

    </script>

<?php
$con = mysql_connect("bkjha1.arvixevps.com", "iskcon", "H@rekrishna9");
mysql_select_db("iskcon");
/*$con  = mysql_connect("localhost","root","");
mysql_select_db("iskcon");*/

if (isset($_POST['form1'])) {
    extract($_POST);

    $query = "insert into pledge values('','$name', '$amounts', '', '', '', '', '', '$startdate', '$phone', '$email2', '$streetaddress', '$sign')";
    $result = mysql_query($query);
    if ($result == true) {
        /*send email to temple devotee*/
        $emailadd = "$email2,support@iskcongreatercolumbus.com";
        $subject = 'Thank You For Your Donation';
        $message2 = "Thank You Very Much $name for your donation.We take pride of service to the Lotus Feet of Sri Sri Radha-Natabara." . "\r\n";
        sendemail($emailadd, $subject, $message2);

        /*send email temple council*/
        $emailadd = 'support@iskcongreatercolumbus.com';
        $subject = 'Donation for the existing Temple';
        $message2 = "$name has pledged for $opportunities , please check CRM tools for more details." . "\r\n";
        sendemail($emailadd, $subject, $message2);

        $con = mysql_connect("bkjha1.arvixevps.com", "iskcon", "H@rekrishna9");
        mysql_select_db("crmdb");
        $note = "email-$email2,	name-$name";
        $db_query = mysql_query("INSERT INTO `donates` (`donation_type`,`date1`,`devotee`,`amount`,`note`,`current_date`) VALUES ('Pledge','$startdate','$name','$amounts','$note','$current_date')");
    }
    ?>
    <script type="text/javascript">
        $('#thank').modal('show');
    </script>
    <?php
}

if (isset($_POST['form2'])) {
    extract($_POST);
    $donationtowards = implode(", ", $donationtowards);
    $query = "insert into directDebit values('','$firstname', '$lastname', '$amount', '$donationtowards', '$bankroutingnumber', '$bankaccountnumber', '$startdate', '$frequencyofdonation', '$start', '$email', '$streetaddress', '$apt', '$city', '$state', '$zip', '$phone', '$sign')";
    $result = mysql_query($query);

    ?>
    <script type="text/javascript">
        $('#thank').modal('show');
    </script>
    <?php
}
?>