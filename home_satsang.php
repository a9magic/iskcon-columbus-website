<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <title>Home Programs</title>
    <?php
    include('meta_links.php');
    ?>
</head>

<body class="events-template-default single single-events postid-74 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">

    <?php
    include('header.php');
    ?>
    <!-- Blog Heading -->
    <section id="blog-heading">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text">
                    <h1 class="blog-title text-center">Home Gatherings/Programs</h1>
                </div><!-- /.heading-text -->
            </div><!-- /.container -->
        </div><!-- /.heading-fullwidth -->
    </section><!-- /#blog-heading -->
    <!-- Blog Heading end -->

    <!-- Blog Page Container -->
    <section id="blog-page-container">
        <div class="blog-page-wrapper">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 wow fadeInLeft" data-wow-delay="0.2s">

                        <div class="sermons">

                            <div class="wow fadeInUp" data-wow-delay="0.2s">
                                <div class="container">

                                    <script>


                                        // Variable
                                        var posts = $('.item');
                                        posts.hide();
                                        $("#category button").click(function () {
                                            // Get data of category
                                            var customType = $(this).data('filter'); // category

                                            $('.item').each(function () {
                                                // Get data of item
                                                data = $(this).data('cat');


                                                // If equal = show
                                                if (data == customType) {
                                                    alert("equal");
                                                    $(this).show()
                                                }


                                            });
                                        });


                                        jQuery(window).on("load resize", function (e) {

                                            var $container = jQuery('.isotope-items'),
                                                colWidth = function () {
                                                    var w = $container.width(),
                                                        columnNum = 1,
                                                        columnWidth = 0;
                                                    if (w > 1040) {
                                                        columnNum = 4;
                                                    }
                                                    else if (w > 850) {
                                                        columnNum = 2;
                                                    }
                                                    else if (w > 768) {
                                                        columnNum = 2;
                                                    }
                                                    else if (w > 480) {
                                                        columnNum = 2;
                                                    }
                                                    columnWidth = Math.floor(w / columnNum);

                                                    //Isotop Version 1
                                                    var $containerV1 = jQuery('.isotope-items');
                                                    $containerV1.find('.item').each(function () {
                                                        var $item = jQuery(this),
                                                            multiplier_w = $item.attr('class').match(/item-w(\d)/),
                                                            multiplier_h = $item.attr('class').match(/item-h(\d)/),
                                                            width = multiplier_w ? columnWidth * multiplier_w[1] - 10 : columnWidth,
                                                            height = multiplier_h ? columnWidth * multiplier_h[1] * 0.7 : columnWidth * 0.7;
                                                        $item.css({width: width, height: height,});
                                                    });


                                                    return columnWidth;
                                                },
                                                isotope = function () {
                                                    $container.isotope({
                                                        resizable: true,
                                                        itemSelector: '.item',
                                                        masonry: {
                                                            columnWidth: colWidth(),
                                                            gutterWidth: 10
                                                        }
                                                    });
                                                };
                                            isotope();


                                            // bind filter button click
                                            jQuery('.isotope-filters').on('click', 'button', function () {
                                                var filterValue = jQuery(this).attr('data-filter');
                                                $container.isotope({filter: filterValue});
                                            });

// change active class on buttons
                                            jQuery('.isotope-filters').each(function (i, buttonGroup) {
                                                var $buttonGroup = jQuery(buttonGroup);
                                                $buttonGroup.on('click', 'button', function () {
                                                    $buttonGroup.find('.active').removeClass('active');
                                                    jQuery(this).addClass('active');
                                                });
                                            });


// Masonry Isotope
                                            var $masonryIsotope = jQuery('.isotope-masonry-items').isotope({
                                                itemSelector: '.item',
                                            });

// bind filter button click
                                            jQuery('.isotope-filters').on('click', 'button', function () {
                                                var filterValue = jQuery(this).attr('data-filter');
                                                $masonryIsotope.isotope({filter: filterValue});
                                            });

// change active class on buttons
                                            jQuery('.isotope-filters').each(function (i, buttonGroup) {
                                                var $buttonGroup = jQuery(buttonGroup);
                                                $buttonGroup.on('click', 'button', function () {
                                                    $buttonGroup.find('.active').removeClass('active');
                                                    jQuery(this).addClass('active');
                                                });
                                            });
                                        });
                                    </script>

                                    <div id="category" class="isotope-filters portfolio-filter">

                                        <button class="button is-checked " data-sort-by="original-order">
                                            ALL
                                        </button>
                                        <button data-filter="downtown">Downtown</button>
                                        <button data-filter="lewis-center">Lewis Center</button>
                                        <button data-filter="new-albany">New Albany</button>
                                        <button data-filter="hilliard">Hilliard</button>

                                    </div>
                                </div>

                                <!-- Downtown -->
                                <div class="portal item downtown wow fadeInUp" data-cat="downtown" data-wow-delay="0.2s"
                                     style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp; background: #ffffff;">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Downtown, Columbus</h3>
                                        <hr/>
                                    </div>

                                    <div class="col-md-4">
                                        <img width="300" height="300"
                                             src="images/premvilas.jpg"
                                             class="attachment-thumbnail size-thumbnail wp-post-image" alt=""
                                             srcset="images/premvilas.jpg 180w, images/premvilas.jpg 300w"
                                             sizes="(max-width: 300px) 100vw, 150px">
                                    </div>

                                    <div class="col-md-4 text-center" style="padding-top: 20px;">

                                        <div class="event-detail">
                                            <h4 class="widget-title">Premvilas Das</h4>
                                            Welcomes you to join them for <strong>Bhagavad Gita</strong> reading session
                                            at
                                            his home.
                                            <hr/>
                                            <div class="address text-left" style="padding-top: 25px;">
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-map-marker base-color"></i><strong>
                                                            Where:</strong>
                                                        325 W 6th Ave Columbus
                                                        OH – 43201
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-phone base-color"></i><strong>
                                                            Call:</strong>
                                                        614-946-5568
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-calendar-o base-color"></i><strong>
                                                            When:</strong> Every Monday at <strong>7.30
                                                            PM
                                                            – 9.00 PM</strong></div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-users base-color"></i><strong>
                                                            Who:</strong> Everyone is invited to
                                                        attend
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="eventMap eventMap0"></div>
                                        <script>
                                            jQuery(document).ready(function () {
                                                var teamID = jQuery(".eventMap0");
                                                if (teamID.length) {
                                                    function isMobile() {
                                                        return ('ontouchstart' in document.documentElement);
                                                    }

                                                    function init_gmap() {
                                                        if (typeof google == 'undefined') return;
                                                        var options = {
                                                            center: [39.9882391,-83.016804],
                                                            zoom: 15,
                                                            mapTypeControl: true,
                                                            mapTypeControlOptions: {
                                                                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                                                            },
                                                            navigationControl: true,
                                                            scrollwheel: false,
                                                            streetViewControl: true
                                                        }

                                                        if (isMobile()) {
                                                            options.draggable = false;
                                                        }

                                                        jQuery('.eventMap0').gmap3({
                                                            map: {
                                                                options: options
                                                            },
                                                            marker: {
                                                                latLng: [39.9882391,-83.016804],
                                                                options: {icon: ""}
                                                            }
                                                        });
                                                    }

                                                    init_gmap();
                                                }

                                            });

                                        </script>
                                        <div>
                                            <form action="https://maps.google.com/maps" method="get" target="_blank">
                                                <input type="text" placeholder="Enter Your Location" name="saddr"/>
                                                <input type="hidden" name="daddr" value="325 W 6th Ave Columbus OH"/>
                                                <input type="submit" class="btn custom-btn"
                                                       style="color: white; border-radius:20px;"
                                                       value="Get directions"/>
                                            </form>
                                        </div>
                                    </div>

                                </div>

                                <!-- Lewis Center -->
                                <div class="portal item lewis-center wow fadeInUp" data-cat="lewis-center" data-wow-delay="0.2s"
                                     style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp; background: #ffffff;">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Lewis Center, Columbus</h3>
                                        <hr/>
                                    </div>

                                    <div class="col-md-4">
                                        <img width="300" height="300"
                                             src="images/ram-tirtha.jpg"
                                             class="attachment-thumbnail size-thumbnail wp-post-image" alt=""
                                             srcset="images/ram-tirtha.jpg 180w, images/ram-tirtha.jpg 300w"
                                             sizes="(max-width: 300px) 100vw, 150px">
                                    </div>

                                    <div class="col-md-4 text-center" style="padding-top: 20px;">

                                        <div class="event-detail">
                                            <h4 class="widget-title">Ram Tirtha Das</h4>
                                            Welcomes you to join them for <strong>Journey Of Self Discovery</strong>
                                            reading
                                            session at his home.
                                            <hr/>
                                            <div class="address text-left" style="padding-top: 25px;">
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-map-marker base-color"></i><strong>
                                                            Where:</strong>
                                                        6445 Morningside Drive, Lewis Center, OH – 43035
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-phone base-color"></i><strong>
                                                            Call:</strong>
                                                        614-404-8570
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-calendar-o base-color"></i><strong>
                                                            When:</strong> Every Friday at <strong>6.30
                                                            PM
                                                            – 8.30 PM</strong></div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-users base-color"></i><strong>
                                                            Who:</strong> Everyone is invited to
                                                        attend
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="eventMap eventMap1"></div>
                                        <script>
                                            jQuery(document).ready(function () {
                                                var teamID = jQuery(".eventMap1");
                                                if (teamID.length) {
                                                    function isMobile() {
                                                        return ('ontouchstart' in document.documentElement);
                                                    }

                                                    function init_gmap() {
                                                        if (typeof google == 'undefined') return;
                                                        var options = {
                                                            center: [40.189849,-82.9995937],
                                                            zoom: 15,
                                                            mapTypeControl: true,
                                                            mapTypeControlOptions: {
                                                                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                                                            },
                                                            navigationControl: true,
                                                            scrollwheel: false,
                                                            streetViewControl: true
                                                        }

                                                        if (isMobile()) {
                                                            options.draggable = false;
                                                        }

                                                        jQuery('.eventMap1').gmap3({
                                                            map: {
                                                                options: options
                                                            },
                                                            marker: {
                                                                latLng: [40.189849,-82.9995937],
                                                                options: {icon: ""}
                                                            }
                                                        });
                                                    }

                                                    init_gmap();
                                                }

                                            });

                                        </script>
                                        <div>
                                            <form action="https://maps.google.com/maps" method="get" target="_blank">
                                                <input type="text" placeholder="Enter Your Location" name="saddr"/>
                                                <input type="hidden" name="daddr"
                                                       value="6445 Morningside Drive, Lewis Center, OH"/>
                                                <input type="submit" class="btn custom-btn"
                                                       style="color: white; border-radius:20px;"
                                                       value="Get directions"/>
                                            </form>
                                        </div>
                                    </div>

                                </div>

                                <!-- New Albany -->
                                <div class="portal item new-albany wow fadeInUp" data-wow-delay="0.2s"
                                     style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp; background: #ffffff;">
                                    <div class="col-md-12">
                                        <h3 class="text-center">New Albany, OH</h3>
                                        <hr/>
                                    </div>

                                    <div class="col-md-4">
                                        <img width="300" height="300"
                                             src="images/ramesh.jpg"
                                             class="attachment-thumbnail size-thumbnail wp-post-image" alt=""
                                             srcset="images/ramesh.jpg 180w, images/ramesh.jpg 300w"
                                             sizes="(max-width: 300px) 100vw, 150px">
                                    </div>

                                    <div class="col-md-4 text-center" style="padding-top: 20px;">

                                        <div class="event-detail">
                                            <h4 class="widget-title">Bhakta Ramesh</h4>
                                            Welcomes you to join them to discuss <strong>Vedic Scriptures</strong> at
                                            his home.
                                            <hr/>
                                            <div class="address text-left" style="padding-top: 25px;">
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-map-marker base-color"></i><strong>
                                                            Where:</strong>
                                                        4392 Cordova Drive, New Albany, OH – 43054
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-phone base-color"></i><strong>
                                                            Call:</strong>
                                                        630-464-8604
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-calendar-o base-color"></i><strong>
                                                            Friday 6:30 PM
                                                            – 9.00 PM</strong></div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-users base-color"></i><strong>
                                                            Who:</strong> Everyone is invited to
                                                        attend
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="eventMap eventMap2"></div>
                                        <script>
                                            jQuery(document).ready(function () {
                                                var teamID = jQuery(".eventMap2");
                                                if (teamID.length) {
                                                    function isMobile() {
                                                        return ('ontouchstart' in document.documentElement);
                                                    }

                                                    function init_gmap() {
                                                        if (typeof google == 'undefined') return;
                                                        var options = {
                                                            center: [40.0692799,-82.8548776],
                                                            zoom: 15,
                                                            mapTypeControl: true,
                                                            mapTypeControlOptions: {
                                                                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                                                            },
                                                            navigationControl: true,
                                                            scrollwheel: false,
                                                            streetViewControl: true
                                                        }

                                                        if (isMobile()) {
                                                            options.draggable = false;
                                                        }

                                                        jQuery('.eventMap2').gmap3({
                                                            map: {
                                                                options: options
                                                            },
                                                            marker: {
                                                                latLng: [40.0692799,-82.8548776],
                                                                options: {icon: ""}
                                                            }
                                                        });
                                                    }

                                                    init_gmap();
                                                }

                                            });

                                        </script>
                                        <div>
                                            <form action="https://maps.google.com/maps" method="get" target="_blank">
                                                <input type="text" placeholder="Enter Your Location" name="saddr"/>
                                                <input type="hidden" name="daddr"
                                                       value="4392 Cordova Drive, New Albany, OH"/>
                                                <input type="submit" class="btn custom-btn"
                                                       style="color: white; border-radius:20px;"
                                                       value="Get directions"/>
                                            </form>
                                        </div>
                                    </div>

                                </div>

                                <!-- Lewis Center -->
                                <div class="portal item lewis-center wow fadeInUp" data-wow-delay="0.2s"
                                     style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp; background: #ffffff;">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Lewis Center, OH</h3>
                                        <hr/>
                                    </div>

                                    <div class="col-md-4">
                                        <img width="300" height="300"
                                             src="images/naveen.jpg"
                                             class="attachment-thumbnail size-thumbnail wp-post-image" alt=""
                                             srcset="images/naveen.jpg 180w, images/naveen.jpg 300w"
                                             sizes="(max-width: 300px) 100vw, 150px">
                                    </div>

                                    <div class="col-md-4 text-center" style="padding-top: 20px;">

                                        <div class="event-detail">
                                            <h4 class="widget-title">Naveen Krishna Das</h4>
                                            Welcomes you to join them for <strong>Bhagavad Gita</strong> reading session
                                            at
                                            his home.
                                            <hr/>
                                            <div class="address text-left" style="padding-top: 25px;">
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-map-marker base-color"></i><strong>
                                                            Where:</strong>
                                                        1944 Rocklake Court, Lewis Center, OH 43035
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-phone base-color"></i><strong>
                                                            Call:</strong>
                                                        614-316-8936
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-calendar-o base-color"></i><strong>
                                                            When:</strong> Every Saturday at <strong>6.00
                                                            PM
                                                            – 8.00 PM</strong></div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-users base-color"></i><strong>
                                                            Who:</strong> Everyone is invited to
                                                        attend
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="eventMap eventMap3"></div>
                                        <script>
                                            jQuery(document).ready(function () {
                                                var teamID = jQuery(".eventMap3");
                                                if (teamID.length) {
                                                    function isMobile() {
                                                        return ('ontouchstart' in document.documentElement);
                                                    }

                                                    function init_gmap() {
                                                        if (typeof google == 'undefined') return;
                                                        var options = {
                                                            center: [40.1867217,-83.0010009],
                                                            zoom: 15,
                                                            mapTypeControl: true,
                                                            mapTypeControlOptions: {
                                                                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                                                            },
                                                            navigationControl: true,
                                                            scrollwheel: false,
                                                            streetViewControl: true
                                                        }

                                                        if (isMobile()) {
                                                            options.draggable = false;
                                                        }

                                                        jQuery('.eventMap3').gmap3({
                                                            map: {
                                                                options: options
                                                            },
                                                            marker: {
                                                                latLng: [40.1867217,-83.0010009],
                                                                options: {icon: ""}
                                                            }
                                                        });
                                                    }

                                                    init_gmap();
                                                }

                                            });

                                        </script>
                                        <div>
                                            <form action="https://maps.google.com/maps" method="get" target="_blank">
                                                <input type="text" placeholder="Enter Your Location" name="saddr"/>
                                                <input type="hidden" name="daddr" value="1944 Rocklake Court, Lewis Center, OH"/>
                                                <input type="submit" class="btn custom-btn"
                                                       style="color: white; border-radius:20px;"
                                                       value="Get directions"/>
                                            </form>
                                        </div>
                                    </div>

                                </div>

                                <!-- Hilliard -->
                                <div class="portal item hilliard wow fadeInUp" data-wow-delay="0.2s"
                                     style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp; background: #ffffff;">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Hilliard, OH</h3>
                                        <hr/>
                                    </div>

                                    <div class="col-md-4">
                                        <img width="300" height="300"
                                             src="images/prem-sindhu.jpg"
                                             class="attachment-thumbnail size-thumbnail wp-post-image" alt=""
                                             srcset="images/prem-sindhu.jpg 180w, images/prem-sindhu.jpg 300w"
                                             sizes="(max-width: 300px) 100vw, 150px">
                                    </div>

                                    <div class="col-md-4 text-center" style="padding-top: 20px;">

                                        <div class="event-detail">
                                            <h4 class="widget-title">Prema Sindhu Das</h4>
                                            Welcomes you to join them for <strong>Bhagavat Katha</strong> - discussion
                                            at
                                            his home.
                                            <hr/>
                                            <div class="address text-left" style="padding-top: 25px;">
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-map-marker base-color"></i><strong>
                                                            Where:</strong>
                                                        4265 Summit Bend Rd, Hilliard OH- 43026
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-phone base-color"></i><strong>
                                                            Call:</strong>
                                                        727-804-9835
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-calendar-o base-color"></i><strong>
                                                            When:</strong> Every Saturday at <strong>5.30
                                                            PM
                                                            – 8.00 PM</strong></div>
                                                </div>
                                                <div class="media">
                                                    <div class="media-body"><i
                                                                class="fa fa-users base-color"></i><strong>
                                                            Who:</strong> Everyone is invited to
                                                        attend
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="eventMap eventMap4"></div>
                                        <script>
                                            jQuery(document).ready(function () {
                                                var teamID = jQuery(".eventMap4");
                                                if (teamID.length) {
                                                    function isMobile() {
                                                        return ('ontouchstart' in document.documentElement);
                                                    }

                                                    function init_gmap() {
                                                        if (typeof google == 'undefined') return;
                                                        var options = {
                                                            center: [40.0363871,-83.1942867],
                                                            zoom: 15,
                                                            mapTypeControl: true,
                                                            mapTypeControlOptions: {
                                                                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                                                            },
                                                            navigationControl: true,
                                                            scrollwheel: false,
                                                            streetViewControl: true
                                                        }

                                                        if (isMobile()) {
                                                            options.draggable = false;
                                                        }

                                                        jQuery('.eventMap4').gmap3({
                                                            map: {
                                                                options: options
                                                            },
                                                            marker: {
                                                                latLng: [40.0363871,-83.1942867],
                                                                options: {icon: ""}
                                                            }
                                                        });
                                                    }

                                                    init_gmap();
                                                }

                                            });

                                        </script>
                                        <div>
                                            <form action="https://maps.google.com/maps" method="get" target="_blank">
                                                <input type="text" placeholder="Enter Your Location" name="saddr"/>
                                                <input type="hidden" name="daddr" value="4265 Summit Bend Rd, Hilliard OH"/>
                                                <input type="submit" class="btn custom-btn"
                                                       style="color: white; border-radius:20px;"
                                                       value="Get directions"/>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                </div><!-- row -->
            </div> <!-- #container -->
        </div><!-- #blog-page-wrapper -->
    </section>

    <?php
    include('footer.php');
    ?>


</body>
</html>