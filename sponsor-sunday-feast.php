<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <title>Sponsor Sunday Feast
    </title>
    <?php
    include('meta_links.php');
    ?>
</head>
<body class="product-template-default single single-product postid-247 _masterslider _msp_version_3.0.4 woocommerce woocommerce-page wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- /#preloader -->
<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php include('header.php'); ?>
    <section id="blog-heading" style="height:auto;">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text pull-left" style="width:99%;padding-top:25px;">
                    <h1 class="blog-title" style="text-align:center;font-size:31px;">Sponsor Sunday Feast
                    </h1>
                </div>
                <!-- /.heading-text -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.heading-fullwidth -->
    </section>
    <script type="text/javascript">
        $(document).ready(function () {

        });

        $('#festival').click(function () {
            $('html,body').animate({'scrollTop': $('#festival-schedule').position().top}, 500);
        });

    </script>

    <!-- header image -->
    <div class="full-width">
        <div class="vc_row-fluid">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">


                    <div class="wow fadeIn" data-wow-delay="0.4s"
                         style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp width: 100%;"><img
                                src="images/sunday-feast-header.jpg"
                                alt=""
                                title="" style="height: 80%; width: 100%; margin-top: 0px; margin-left: -0.5px;">
                    </div>

                </div>
            </div>
        </div>
    </div>

    <section id="shop" style="padding:0 !important;">

        <div class="container">
            <!-- festival -->
            <div class="full-width">
                <div class="vc_row-fluid">
                    <div class="vc_col-sm-12 wpb_column vc_column_container ">
                        <div class="wpb_wrapper">

                            <h2 class="section-title wow fadeInUp" style="padding: 15px 0 15px 0;"
                                data-wow-delay="0.4s"><i
                                        class="fa fa-cogs fa-2x"></i> How to Sponsor Sunday Feast </h2>
                            <!-- /.section-title -->

                            <div class="sermons">

                                <div class="col-md-8 col-sm-8 text-center">
                                    <h1 class="wow fadeInUp" data-wow-delay="0.4s" style="padding: 10px;"><u>Use
                                            Credit/Debit</u></h1>
                                    <div class="col-md-6 col-sm-6 text-center">
                                        <div class="vc_box_shadow_border wow fadeInLeft text-center"
                                             data-wow-delay="0.6s"
                                             style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                            <h2 style="padding: 10px;">Sunday Feast</h2>

                                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post"
                                                  target="_blank">
                                                <input type="hidden" name="cmd" value="_s-xclick">
                                                <input type="hidden" name="hosted_button_id" value="TQ9GSAYJTCCZW">
                                                <table>
                                                    <tr>
                                                        <td><input type="hidden" name="on0" value="Sponsor Prasadam">
                                                            <h3>Sponsor Prasadam</h3>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><select name="os0">
                                                                <option value="Prasada -">Prasada - $250.00 USD</option>
                                                                <option value="Maha Prasada -">Maha Prasada - $400.00
                                                                    USD
                                                                </option>
                                                            </select></td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="hidden" name="on1" value="Memo">
                                                            <h3> Please add memo </h3></td>
                                                    </tr>
                                                    <tr>
                                                        <td><input type="text" name="os1" maxlength="200"></td>
                                                    </tr>
                                                </table>
                                                <input type="hidden" name="currency_code" value="USD">
                                                <input type="image"
                                                       src="http://www.pngall.com/wp-content/uploads/2016/05/PayPal-Donate-Button.png"
                                                       border="0" name="submit"
                                                       alt="PayPal - The safer, easier way to pay online!">
                                                <img alt="" border="0"
                                                     src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1"
                                                     height="1">
                                            </form>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6 text-center">
                                        <div class="vc_box_shadow_border wow fadeInLeft text-center"
                                             data-wow-delay="0.6s"
                                             style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                            <h2 style="padding: 10px;">Sponsor Other Amount</h2>
                                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post"
                                                  target="_blank">
                                                <input type="hidden" name="cmd" value="_s-xclick">
                                                <input type="hidden" name="hosted_button_id" value="K94FLQYGZG3W2">
                                                <input type="image"
                                                       src="https://www.paypalobjects.com/WEBSCR-640-20110429-1/en_US/i/btn/btn_donateCC_LG.gif"
                                                       border="0" name="submit"
                                                       alt="PayPal - The safer, easier way to pay online!">
                                                <img alt="" border="0"
                                                     src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif"
                                                     width="1" height="1">
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="vc_box_shadow_border wow fadeInRight text-center" data-wow-delay="0.6s"
                                         style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                        <h1 style="padding: 10px;"><u>ACH Debit Form</u></h1>
                                        <?php include('ach-form.php') ?>
                                    </div>
                                </div>


                                <div class="coll-md-12 col-sm-12 text-center wow fadeInUp" data-wow-delay="0.6s"
                                     style="padding: 30px;">
                                    If setting up on your own: Payee name: ISKCON Columbus, Address: 379 West 8th Ave
                                    Columbus, OH 43201. If you have any questions, contact <strong>Ram Tirtha
                                        Das </strong> at 614-404-8570 or stop by in the temple.
                                </div>

                            </div>

                            <div class="col-md-12 wow fadeInUp" data-wow-delay="0.4s" style="padding: 30px;">
                                <strong>“One should distribute viṣṇu-prasāda to everyone. Know that Lord Viṣṇu (Krishna)
                                    is very pleased when everyone is sumptuously fed with viṣṇu-prasāda”. – Śrīmad-Bhāgavatam 8.16.56</strong>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end Sunday Feast -->
        </div>

        <div id="festival-schedule"><?php include('festival-schedule.php') ?></div>


        <div class="vc_empty_space" style="height: 50px"><span class="vc_empty_space_inner"></span>
        </div>

        <h6 class="wow fadeInUp text-center" data-wow-delay="0.4s">
            We are a 501 (C) (3) non-profit charity organization. Our IRS tax id is <strong>34-1225-440</strong> and all
            donations
            made to 'ISKCON Columbus' are tax deductible.
        </h6>
        <?php
        include('footer.php');
        ?>
</body>
</html>
