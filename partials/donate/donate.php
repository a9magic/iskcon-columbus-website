<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Donate</title>
    <?php
    include("meta_links.php");
    ?>
</head>
<body class="page-template page-template-page-template page-template-event-page page-template-page-templateevent-page-php page page-id-402 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php
    include("partials/header.php");
    ?>
    <section id="blog-heading" style="height:251px;border:1px solid #e1e1e1;">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text pull-left" style="width:99%;padding-top:41px;">
                    <h1 class="blog-title" style="text-align:center;font-size:31px;">DONATE
                    </h1>
                </div>
                <!-- /.heading-text -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.heading-fullwidth -->
    </section>
    <section id="blog-page-container">
        <div class="blog-page-wrapper">
            <div id="primary" class="container">
                <main id="main" class="main-content" role="main">
                    <div class="blog-content">

                        <div class="blog-item col-md-4">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                                        <span class="event-date">June</span>
                                        <br>
                                        <span class="event-month"></span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="tithe.php" title="Tithe">Tithe - Support Monthly</a>
                                    </h2>
                                    <!-- /.event-period -->
                                    <div class="feature-img">
                                        <img width="570" height="341" src="../../images/tithes.jpg"
                                             class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt=""
                                             sizes="(max-width: 570px) 100vw, 570px"/>
                                    </div>
                                    <div class="event-description">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="blog-item col-md-4">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                                        <span class="event-date">12</span>
                                        <br>
                                        <span class="event-month">Oct</span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="#" title="Spacial Monazat">Sponsor Activities</a>
                                    </h2>

                                    <!-- /.event-period -->
                                    <div class="feature-img">
                                        <img width="570" height="341"
                                             src="../../images/Aarti_Arati_Lamp_for_Puja_Prayers_Hinduism-239x134.jpg"
                                             class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                             alt=" sizes=" (max-width: 570px) 100vw, 570px" />
                                    </div>
                                    <div class="event-description">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="blog-item col-md-4">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                                        <span class="event-date">25</span>
                                        <br>
                                        <span class="event-month">Sep</span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="#" title="Open Rededication 1">Sponsor the Sunday Feast</a>
                                    </h2>
                                    <!-- /.event-period -->
                                    <div class="feature-img">
                                        <img width="570" height="341" src="../../images/IMG_3260-1024x683-239x134.jpg"
                                             class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt=""
                                             sizes="(max-width: 570px) 100vw, 570px"/>
                                    </div>
                                    <div class="event-description"></div>
                                </div>
                            </div>
                        </div>

                        <div class="blog-item col-md-4">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                                        <span class="event-date">10</span>
                                        <br>
                                        <span class="event-month">Jan</span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="#" title="Big Event This Year">Sponsor Book Distribution</a>
                                    </h2>
                                    <!-- /.event-period -->
                                    <div class="feature-img">
                                        <img width="570" height="341"
                                             src="../../images/book-distribution-wallpapers_1920x1440-239x134.jpg"
                                             class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt=""
                                             sizes="(max-width: 570px) 100vw, 570px"/>
                                    </div>
                                    <div class="event-description"></div>
                                </div>
                            </div>
                        </div>

                        <div class="blog-item col-md-4">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                                        <span class="event-date">16</span>
                                        <br>
                                        <span class="event-month">Dec</span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="#" title="General Donations">General Donations</a>
                                    </h2>
                                    <!-- /.event-period -->
                                    <div class="feature-img">
                                        <img width="570" height="341" src="../../images/RS238-239x134.jpg"
                                             class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt=""
                                             sizes="(max-width: 570px) 100vw, 570px"/>
                                    </div>
                                    <div class="event-description"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
    </section>

    <?php
    include("footer.php");
    ?>
</div>
<!-- /.peace layout end -->
<!-- Color Variation Switcher End -->
<script>
    /*-----------------------------------------------------------------------------------
     /* Styles Switcher
     -----------------------------------------------------------------------------------*/

    window.console = window.console || (function () {
            var c = {};
            c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function () {
            };
            return c;
        })();


    jQuery(document).ready(function ($) {

        jQuery('#layout1').click(function () {
            $("#peace-layout").removeClass('peace-box-layout');
        });

        jQuery('#layout2').click(function () {
            $("#peace-layout").addClass('peace-box-layout');
        });

        $("ul.colors .color1").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/default.css");
            return false;
        });

        $("ul.colors .color2").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-1.css");
            return false;
        });

        $("ul.colors .color3").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-2.css");
            return false;
        });

        $("ul.colors .color4").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-3.css");
            return false;
        });

        $("ul.colors .color5").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-4.css");
            return false;
        });

        $("ul.colors .color6").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-5.css");
            return false;
        });

        $("ul.colors .color7").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-6.css");
            return false;
        });
        $("ul.colors .color8").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-7.css");
            return false;
        });
        $("ul.colors .color9").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-8.css");
            return false;
        });
        $("ul.colors .color10").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-9.css");
            return false;

        });


        // jQuery('#colors').farbtastic(function(color) {
        //     // cool stuff goes here
        //     jQuery('#picked').text(color);

        //     jQuery('#result').css('background', color);

        //     // hover font color
        //     jQuery('.blog-sidebar .btn,.blog-section .comments,.blog-section .post-title a,.blog-sidebar .widget_archive a,.blog-sidebar .widget_categories a,.blog-sidebar .widget_nav_menu a,.blog-sidebar .widget_recent_entries a,.causes-post .caption-txt .donated,.copyrights a,.footer-social-btn a,.galleryFilter .current,.galleryFilter a:focus,.galleryFilter a,.navbar-default .navbar-nav>li>a:focus').hover().css('color', color);

        //     //hover border color
        //     jQuery('.contact-form-container .custom-btn').hover().css('border-color', color);

        //     //hover backgound color
        //     jQuery('.link-hex, .blog-sidebar .btn').hover().css('background-color', color);

        //     //hover in a class
        //     jQuery('.gallery-item figure, .service-box, .pricing-item').hover(function(){
        //     	jQuery('.item-head').css('background-color', color);
        //     });


        //     jQuery('.causes-post .caption-txt .donated').css('color', color);

        //     jQuery('.progress-bar-container .progress-bar-warning, .donate-btn, .archive #main-menu.navbar-default,.blog #main-menu.navbar-default,.carousel-indicators li.active,.causes-post .custom-progress-bar,.donate-btn:hover,.event-timeline,.gallery-item figure:hover .item-description,.hex.scroll-top:after,.hex.scroll-top:before,.hex:after,.hex:before,.link-hex:hover,.news-article .meta-icon,.owl-page.active,.page-template-default #main-menu.navbar-default,.parallax-title:after,.pricing-item .item-name:after,.pricing-item:hover .item-head,.publish-date,.section-title:after,.service-box:hover .service-icon-hex,.service-box:hover .service-icon-hex:after,.single #main-menu.navbar-default,.single-causes-post .custom-progress-bar,.slide-nav:hover,.team-member-box:before,.team-member-box:hover .member-designation:after,.section-title:after').css('background-color', color);

        //     jQuery('.page-template-page-templatefront-page-php .menu-bg').css('background-color', color);

        //     jQuery('.carousel-indicators li.active,.comment-form .form-control:focus,.contact-form-container .custom-btn,.contact-form-container .form-control:focus,.contact-info .contact-address li:before,.galleryFilter .current,.gray-bg .custom-btn,.link-hex,.owl-page.active,.post-box .custom-btn,.single-event-post .time-circle .time-number,.time-circle .time-number,.white-bg .custom-btn').css('border-color', color);

        // });


        jQuery("#color-style-switcher .bottom a.settings").click(function (e) {
            e.preventDefault();
            var div = jQuery("#color-style-switcher");
            if (div.css("left") === "-215px") {
                jQuery("#color-style-switcher").animate({
                    left: "0px"
                });
            } else {
                jQuery("#color-style-switcher").animate({
                    left: "-215px"
                });
            }
        })

        jQuery("ul.colors li a").click(function (e) {
            e.preventDefault();
            jQuery(this).parent().parent().find("a").removeClass("active");
            jQuery(this).addClass("active");
        })

    });
</script>
</body>
</html>
<!-- Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/

Object Caching 2338/2556 objects using disk
Content Delivery Network via N/A
Database Caching 9/88 queries in 0.015 seconds using disk

Served from: themes.codexcoder.com @ 2017-06-12 12:03:16 by W3 Total Cache -->