<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <title>Happy Ninja &#8211; Peace
    </title>
    <?php
include('meta_links.php');
?>
  </head>
  <body class="product-template-default single single-product postid-247 _masterslider _msp_version_3.0.4 woocommerce woocommerce-page wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

    <!-- /#preloader -->
    <!-- peace layout start. end in footer.php -->
    <div id="peace-layout">
      <?php include('partials/header.php'); ?>
      <div class="shop-page">
        <div class="container">
          <h2 class="page-title title m0 pull-left">Tithe - Support Monthly
          </h2>
          <div class="pull-right">
            <nav class="woocommerce-breadcrumb">
              <ul>
                <li>
                  <i class="fa fa-home">
                  </i> 
                </li>
                <li class="breadcrumb-item">
                  <a href="http://themes.codexcoder.com/peace">Home
                  </a>
                </li>&nbsp;&#47;&nbsp;
                <li class="breadcrumb-item">
                  <a href="http://themes.codexcoder.com/peace/?product_cat=clothing">Tithe - Support Monthly
                  </a>
                </li>
              </ul>
            </nav>	
          </div>
        </div>
      </div>
      <section id="shop" style="padding:0 !important;">
        <div itemscope itemtype="http://schema.org/Product" id="product-247" class="post-247 product type-product status-publish has-post-thumbnail product_cat-clothing product_cat-hoodies first instock shipping-taxable purchasable product-type-simple">
          <div class="container">
            <div class="row">
			
			   <div class="col-md-3">
                <div id="blog-sidebar" class="blog-sidebar">
                  <div id="secondary" class="widget-area" role="complementary">
          
          
             
                    <aside id="meta-2" class="widget widget_meta">
                      <h2 class="widget-title">Donate For The Tample
                      </h2>			
                      <ul>
                        <li>
                          <a href="sponsor_festivals.php">Sponsor Sunday Feast
                          </a>
                        </li>
                        <li>
                          <a href="sponsor_book_distribution.php">Sponsor Book Distribution
                            
                          </a>
                        </li>
                        <li>
                          <a href="general_donation.php">General Donation
                            
                          </a>
                        </li>
                        <li>
                          <a href="new_temple.php">Donate For New temple
                          </a>
                        </li>			
                      </ul>
                    </aside>		
                  </div>
                  <!-- #secondary -->
                </div>
              </div>

              <div class="col-md-9 product-single">
			 
                <div class="product-detail">
						
                  <div class="col-md-6">
                    <div class="images">
                      <a href="images/IMG_1468.jpg" itemprop="image" class="woocommerce-main-image zoom" title="" data-rel="prettyPhoto[product-gallery]">
                        <img width="600" height="600" src="images/IMG_1468.jpg" class="attachment-shop_single size-shop_single wp-post-image" alt="hoodie_4_front" title="hoodie_4_front"/>
                      </a>
                      <div class="thumbnails columns-3">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 product-content">
                    
                
                    <div itemprop="description">
                      <p>A temple needs the support of its congregation to remain solvent and stable and to provide services for the community on a regular basis. Tithing, the regular commitment of a monthly donation to support the functioning of our temple, defines your relationship with Krishna. 95% of our community does not have time on a daily basis to offer physical service at the temple. But you can still be a direct part of the seva here by supporting these activities through financial contributions.
                      </p>
                    </div>
                
               
                  </div>
                </div>
                <div class="product-description clearfix">
                  <div role="tabpanel">
                    <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="description_tab active">
                        <a role="tab"  data-toggle="tab" href="#tab-description">Principle Reasons to Tithe
                        </a>
                      </li>
                      <li role="presentation" class="reviews_tab  ">
                        <a role="tab"  data-toggle="tab" href="#tab-reviews">Donate For Tample
                        </a>
                      </li>
                    </ul>
                    <div class="tab-content">
                      <div role="tabpanel" class="panel tab-pane active" id="tab-description">
                        <h2>Product Description
                        </h2>
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
                        </p>
						 <h2>Product Description
                        </h2>
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
                        </p>
						 <h2>Product Description
                        </h2>
                        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
                        </p>
                      </div>
					
				
                      <div role="tabpanel" class="panel tab-pane  " id="tab-reviews">
                        <div id="reviews">
                          <div id="comments">
                            <h2>Donate
                            </h2>
                           
                          </div>
                          <div id="review_form_wrapper">
                            <div id="review_form">
                              <div id="respond" class="comment-respond">
                                <h3 id="reply-title" class="comment-reply-title">SUPPORT THE TEMPLE
                                
                                </h3>			
									<div class="col-md-6" style="">
										<img src="../../images/paypal-logo.png">
									</div>
									<div class="col-md-6" style="padding:21px;border:1px solid #e6e6e6;>
										<form  id="form_sample_3" class="form-horizontal" method="POST" enctype="multipart/form-data">
											 <div class="form-body">
											<div class="form-group">
                                                <label class="control-label col-md-3">Donate
                                                </label>
                                                <div class="col-md-9">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-dollar"></i>
														</span>
														<input type="number" name="ctgrynme" required="required" placeholder="" class="form-control" value="" >
													</div>
												</div>
                                            </div>
											</div>
											   <div class="form-actions" style="padding-top:5px;">
                                            <div class="row">
                                                <div class="col-md-offset-6 col-md-5">
                                                    <input type="submit" class="btn green" name="submit_property" value="Submit">
                                                </div>
                                            </div>
                                        </div>
										</form>
									</div>
                              </div>
                              <!-- #respond -->
                            </div>
                          </div>
                          <div class="clear">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Related Products Carousel -->
             
                <!-- /#related-products-carousel  -->
                <!-- End Related Products Carousel -->
              </div>
     
            </div>
          </div>
          <meta itemprop="url" content="http://themes.codexcoder.com/peace/?product=happy-ninja-2" />
        </div>
        <!-- #product-247 -->
      </section> 
      <!-- shop -->
    </div>
    </div>
  <div class="clearfix">
  </div>
  <div id="location">
    <div class="container">
      <div class="row">
        <div class="social-icons">
          <ul>
            <li class="facebook">
              <a href="http://facebook.com/id">
                <i class="fa fa-facebook">
                </i>
                <span>Facebook
                </span>
              </a>
            </li>
            <li class="twitter">
              <a href="http://twitter.com/id">
                <i class="fa fa-twitter">
                </i>
                <span>Twitter
                </span>
              </a>
            </li>
            <li class="vimeo">
              <a href="http://vimeo.com/id">
                <i class="fa fa-vimeo-square">
                </i>
                <span>Vimeo
                </span>
              </a>
            </li>
            <li class="behance">
              <a href="http://behance.com/id">
                <i class="fa fa-behance">
                </i>
                <span>Behance
                </span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
  </div>
  <?php
include('footer.php');
?>
  </body>
</html>
