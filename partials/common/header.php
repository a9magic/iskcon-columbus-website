<header id="top-section" style="background:;">
    <div class="container">
        <div class="col-sm-4 hidden-xs address">
            <div class="pull-left">
                <ul>
                    <li><i class="fa fa-phone"></i>+1(614)421-1661</li>
                    <li><i class="fa fa-envelope-o"></i>iskconcolumbus@gmail.com</li>
                    <li><i class="fa fa-map-marker"></i>379 West 8th Ave Columbus, OH 43201</li>
                </ul>
            </div>
        </div><!-- /.address -->

        <div class="hidden-xs col-sm-4 col-xs-12">
            <div class="logo text-center">
                <a href="/" alt="ISKCON Columbus"/>
                <img src="../../images/logo-new.png">
                </a>
            </div><!-- /.logo -->
        </div>

        <div class="social-search pull-right hidden-xs">
            <ul>
                <li><a href="https://www.youtube.com/channel/UCbhrTLxLFZuH-n3Gz1HhJ-w?sub_confirmation=1"
                       rel="nofollow"><i class="fa fa-youtube fa-2x"></i><span></span></a></li>
                <li><a href="https://www.facebook.com/KrishnaHouseColumbus" rel="nofollow"
                       target="_blank"><i class="fa fa-facebook fa-2x"></i><span></span></a></li>
                <li><a href="https://www.instagram.com/iskconcolumbus/" rel="nofollow"
                       target="_blank"><i class="fa fa-instagram fa-2x"></i><span></span></a></li>
            </ul>
        </div><!-- /.social -->


    </div><!-- /.container -->

    <div class="menu-slider">
        <nav id="peace-menu">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <span style="font-family: 'Garamond'; font-size: 20px;" class="hidden-lg hidden-md hidden-sm">
                        <a href="/" alt="ISKCON Columbus"/>
                        <img style="margin: 2%; height: 30px; width:55px;" src="../../images/iskcon-logo.png">
                        <strong>ISKCON Columbus</strong>
                        </a>
                    </span><!-- /.logo -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#responsive-icon" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="responsive-icon">
                    <div class="menu-menu-1-container">
                        <ul id="menu-menu-1" class="nav navbar-nav text-center nav navbar-nav text-center">

                            <li id="menu-item-105"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-has-children menu-item-105 dropdown">
                                <a title="Home" href="../../index.php" class="dropdown-toggle" aria-haspopup="true">Home</a>
                            </li>

                            <li id="menu-item-500"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-500 dropdown">
                                <a title="Causes" href="http://new.krishnatemple.co" target="_blank"> New Temple </a>
                            </li>

                            <li id="menu-item-390"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-390 dropdown">
                                <a title="Events" href="../events/events.php" class="dropdown-toggle" aria-haspopup="true">Events
                                    <i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class=" dropdown-menu hidden-xs">
                                    <li id="menu-item-401"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-406">
                                        <a title="Event Defaults" href="../events/sunday_feast.php">Sunday Feast</a></li>
                                    <li id="menu-item-402"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-406">
                                        <a title="Event Defaults" href="../events/thursday_feast.php">Thursday Program</a></li>
                                    <li id="menu-item-403"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-406">
                                        <a title="Event Defaults" href="../events/harinam.php">Friday Night Harinaam</a></li>
                                    <li id="menu-item-404"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-407"><a
                                                title="Event Single" href="../events/college_outreach.php">University Outreach</a>
                                    </li>
                                    <li id="menu-item-405"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-405">
                                        <a title="Event 2 Col" href="../events/home_satsang.php">Home Gathering/Satsang</a></li>
                                    <li id="menu-item-406"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-404">
                                        <a title="Event 3 Col" href="../events/maha_satsang.php">Maha Satsang</a></li>
                                    <li id="menu-item-407"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-404">
                                        <a title="Event 3 Col" href="https://www.facebook.com/ColumbusBhaktiYoga/"
                                           target="_blank">Bhakti Yoga</a></li>
                                </ul>
                            </li>

                            <li id="menu-item-431"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-431 dropdown">
                                <a title="Gallery" href="../services/services.php" class="dropdown-toggle" aria-haspopup="true">Services
                                    <i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class=" dropdown-menu hidden-xs">
                                    <li id="menu-item-420"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-428">
                                        <a title="Gallery 2 Columns" href="../services/sunday_school.php">Sunday School</a></li>
                                    <li id="menu-item-421"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-428">
                                        <a title="Gallery 2 Columns" href="../services/govindas-catering.php">Govinda's catering</a>
                                    </li>
                                    <li id="menu-item-422"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-428">
                                        <a title="Gallery 2 Columns"
                                           href="https://www.facebook.com/108delectabledelights/" target="_blank">Govinda's
                                            Bakery</a>
                                    </li>
                                    <li id="menu-item-424"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-429">
                                        <a title="Gallery 3 Columns" href="../services/book_distribution.php">Book Distribution</a>
                                    </li>
                                    <li id="menu-item-425"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-429">
                                        <a title="Gallery 3 Columns" href="../services/festival.php">Festivals</a></li>
                                </ul>
                            </li>

                            <li id="menu-item-551"

                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-551 dropdown">
                                <a title="Contact" href="../contact/contact.php">Contact</i></a>

                            <li id="menu-item-431"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-431 dropdown">
                                <a title="Donate" class="donate" href="../donate/donate.php" aria-haspopup="true"> Donate <i
                                            class="fa fa-heart"></i></a>
                                <ul role="menu" class=" dropdown-menu hidden-xs">
                                    <li id="menu-item-430"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-430">
                                        <a title="Gallery Default" href="../donate/tithe.php">Tithe - Support Monthly</a>
                                    </li>
                                    <li id="menu-item-431"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-428">
                                        <a title="Gallery 2 Columns" href="../donate/sponsor_festivals.php">Sponsor
                                            Festivals</a></li>
                                    <li id="menu-item-432"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-429">
                                        <a title="Gallery 3 Columns" href="../donate/sponsor_book_distribution.php">Sponsor Book
                                            Distribution</a></li>
                                    <li id="menu-item-433"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-429">
                                        <a title="Gallery 3 Columns" href="../donate/general_donation.php">General Donation</a>
                                    </li>
                                    <li id="menu-item-434"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-429">
                                        <a title="Gallery 3 Columns" href="../donate/new_temple.php">Donate For New temple</a>
                                    </li>
                                </ul>
                            </li>


                        </ul>
                    </div>
                </div><!-- /.navbar-collapse -->
            </div><!-- /#peace-menu -->
        </nav>


    </div><!-- /.menu-slider -->
</header>