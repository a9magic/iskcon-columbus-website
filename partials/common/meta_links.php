<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="#/xmlrpc.php">


	<meta name='robots' content='noindex,follow' />
<link rel='dns-prefetch' href='//maps.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Peace &raquo; Feed" href="#/?feed=rss2" />
<link rel="alternate" type="application/rss+xml" title="Peace &raquo; Comments Feed" href="#/?feed=comments-rss2" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/themes.codexcoder.com\/peace\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.8"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b===c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='contact-form-7-css' href='../../css/styles.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='jquery.mCustomScrollbar-css' href='../../css/jquery.mCustomScrollbar.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css' href='../../css/font-awesome.min.css?ver=4.12.1' type='text/css' media='all' />
<link rel='stylesheet' id='style-css' href='../../css/style.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-layout-css' href='../../css/woocommerce-layout.css?ver=3.0.8' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-smallscreen-css' href='../../css/woocommerce-smallscreen.css?ver=3.0.8' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='woocommerce-general-css' href='../../css/woocommerce.css?ver=3.0.8' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-css' href='../../css/bootstrap.min.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='fontawesome-css' href='../../css/font-awesome.min1.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='theme-css-css' href='../../css/theme-style.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='peace-style-css' href='../../css/style1.css?ver=4.8' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css' href='../../css/js_composer.min.css?ver=4.12.1' type='text/css' media='all' />
<link rel='stylesheet' id='ms-main-css' href='../../css/masterslider.main.css?ver=3.0.4' type='text/css' media='all' />
<link rel='stylesheet' id='ms-custom-css' href='../../css/custom.css?ver=25.1' type='text/css' media='all' />
<script type='text/javascript' src='../../js/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='../../js/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/peace\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/peace\/?page_id=336&wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/themes.codexcoder.com\/peace\/?page_id=484","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script>
    function initMap() {
        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -34.397, lng: 150.644},
            scrollwheel: false,
            zoom: 8
        });
    }
</script>
<script type='text/javascript' src='../../js/add-to-cart.min.js?ver=3.0.8'></script>
<script type='text/javascript' src='../../js/woocommerce-add-to-cart.js?ver=4.12.1'></script>
<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCk4wFk0xL3OsBDALxj0KF-Lumlcu1mzVM&#038;callback=initMap&#038;ver=4.8'></script>
<link rel='https://api.w.org/' href='#/?rest_route=/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="#/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="#/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.8" />
<meta name="generator" content="WooCommerce 3.0.8" />
<link rel="canonical" href="#/" />
<link rel='shortlink' href='#/' />
<link rel="alternate" type="application/json+oembed" href="#/?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Fthemes.codexcoder.com%2Fpeace%2F" />
<link rel="alternate" type="text/xml+oembed" href="#/?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Fthemes.codexcoder.com%2Fpeace%2F&#038;format=xml" />
<script>var ms_grabbing_curosr='images/common/grabbing.cur',ms_grab_curosr='images/common/grab.cur';</script>
<meta name="generator" content="MasterSlider 3.0.4 - Responsive Touch Image Slider" />

		<style>
			
				
										</style>

						<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
	<meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="#/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><!--[if IE  8]><link rel="stylesheet" type="text/css" href="#/wp-content/plugins/js_composer/assets/css/vc-ie8.min.css" media="screen"><![endif]--><style>
		
</style>
	<style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1475847905924{padding-top: 80px !important;padding-bottom: 80px !important;background-color: #f7f7f7 !important;}.vc_custom_1475847924477{padding-bottom: 100px !important;background-color: #f7f7f7 !important;}.vc_custom_1475847937518{padding-top: 115px !important;background-color: #f7f7f7 !important;}.vc_custom_1475847967102{padding-top: 50px !important;background-color: #f7f7f7 !important;}.vc_custom_1475847994262{padding-top: 55px !important;background-color: #ffffff !important;}.vc_custom_1475848013333{padding-top: 110px !important;padding-bottom: 110px !important;background-color: #f7f7f7 !important;}.vc_custom_1475848026271{padding-right: 0px !important;padding-left: 0px !important;}.vc_custom_1441954675195{padding-bottom: 55px !important;}.vc_custom_1441955531097{padding-bottom: 55px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>