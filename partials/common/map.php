<div class="vc_row-fluid vc_custom_1475847937518">
    <div class="container">
        <div class="vc_col-sm-12 wpb_column vc_column_container ">
            <div class="wpb_wrapper">

                <h2 class="section-title wow fadeInUp" data - wow - delay="0.4s"> Visit Us </h2>
                <!-- /.section - title-->
                <div class="section-detail wow fadeInUp" data - wow - delay="0.6s"> ISKCON Columbus welcomes you to
                    please come with family & friends
                </div><!-- /.section - detail-->
                <div class="sermons" style="position:relative;background:transparent;">

                    <div class="col-lg-12 col-lg-12" style="position:relative;background:transparent;">
                        <div class="portal wow fadeInUp" data - wow - delay="0.6s">
                            <div class="map-container wow fadeInUp" data - wow - delay="0.4s">

                                <div class="mapOverlay" onClick="style.pointerEvents='none'" style="height: 100% !important;">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3056.732827642373!2d-83.01912858510767!3d39.99207557941673!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88388eea2e220463%3A0x4f95e3ec9b88a211!2sISKCON+Temple!5e0!3m2!1sen!2sus!4v1494100183136"
                                            width="100%" height="100%" frameborder="0" style="border:0"
                                            allowfullscreen></iframe>
                                </div>

                                <div class="hidden-xs hidden-md hidden-sm contact-info col-md-3 pull-right wow fadeInRight"
                                     data - wow - delay="2s">
                                    <div class="contact-person">
                                        <img width="150" height="150"
                                             src="../../images/partha.jpg"
                                             class="img-circle" alt="team-3">
                                        <div class="info">
                                            <h3> Partha Das </h3>
                                            <p> Head Priest </p>
                                            <span><i class="fa fa-phone"></i> (614)725 - 9530 </span><br>
                                        </div><!-- /.info-->
                                    </div><!-- /.contact - person-->
                                    <br/>
                                    <div class="contact-person">
                                        <img width="150" height="150"
                                             src="../../images/jagannath.jpg"
                                             class="img-circle" alt="team-3">
                                        <div class="info">
                                            <h3> Jagannatha Das </h3>
                                            <p> Head of Outreach programs </p>
                                            <span><i class="fa fa-phone"></i> (614)327 - 6805 </span><br>
                                        </div><!-- /.info-->
                                    </div><!-- /.contact - person-->
                                </div>
                            </div>

                            <!-- visible for mobile view only -->
                            <div class="visible-xs visible-md visible-sm contact-info col-md-3 text-center wow fadeInRight"
                                 data - wow - delay="0.6s">
                                <br/>
                                <div class="col-md-12 col-sm-12">
                                    <div class="portal wow fadeInUp" data-wow-delay="0.6s">
                                        <img width="150" height="150"
                                             src="../../images/partha.jpg"
                                             class="img-circle" alt="team-3">
                                        <div class="content">Partha Das</div>
                                        <div class="post-date">
                                            Head Priest
                                        </div>
                                        <div class="resource">
                                            <a href=""><i class="fa fa-phone"> (614)725-9530</i></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.contact - person-->

                                <div class="col-md-12 col-sm-12">
                                    <div class="portal wow fadeInUp" data-wow-delay="0.6s">
                                        <img width="150" height="150"
                                             src="../../images/jagannath.jpg"
                                             class="img-circle" alt="team-3">
                                        <div class="content">Jagannatha Das</div>
                                        <div class="post-date">
                                            Head of Outreach programs
                                        </div>
                                        <div class="resource">
                                            <a href=""><i class="fa fa-phone"> (614)327-6805</i></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.contact - person-->
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>