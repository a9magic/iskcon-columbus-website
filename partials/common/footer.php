<div class="clearfix"></div>
<div id="location">
    <div class="container">
        <div class="row">

            <div class="social-icons">
                <ul>
                    <li class="youtube"><a
                                href="https://www.youtube.com/channel/UCbhrTLxLFZuH-n3Gz1HhJ-w?sub_confirmation=1"
                                target="_blank"><i class="fa fa-youtube"></i><span>YouTube</span></a>
                    </li>
                    <li class="facebook"><a href="https://www.facebook.com/KrishnaHouseColumbus" target="_blank"><i
                                    class="fa fa-facebook"></i><span>Facebook</span></a></li>

                    <li class="behance"><a href="https://www.instagram.com/iskconcolumbus/" target="_blank"><i
                                    class="fa fa-instagram"></i><span>Instagram</span></a></li>
                </ul>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container -->
</div>
<div class="footer-widget">
    <div class="container">

        <aside id="text-2" class="widget widget_text">
            <div class="col-md-3"><h2 class="widget-title">ISKCON Columbus</h2>
                <div class="textwidget"><img src="/images/iskcon-seal.png" alt="ISKCON Seal"/>
                    <br/>
                    <br/>
                    It is the desire of ISKCON Columbus and associates to be a place where all are welcomed, connected
                    to
                    Krishna, encouraged to grow in devotion to Him, enabled to practice <strong>Krishna
                        consciousness </strong> based on
                    the teachings of Srila AC Bhaktivedanta Swami Prabhupada, and equipped to propagate Krishna
                    consciousness.
                </div>
                <br/><i class="fa fa-map-marker">
                </i> 379 West 8th Ave Columbus, OH 43201
            </div>
        </aside>
        <aside id="tag_cloud-4" class="widget widget_tag_cloud">
            <div class="col-md-3"><h2 class="widget-title">Temple Schedule</h2>
                <div class="tagcloud">
                    <form>
                        <table>
                            <tr>
                                <th style="color:white;">Time</th>
                                <th style="color:white;">Event</th>
                            </tr>
                            <tr>
                                <td style="color:white;">5:00 AM</td>
                                <td style="color:white;">Mangal Arati
                                </td>
                            </tr>
                            <tr>
                                <td style="color:white;">5:30 AM</td>
                                <td style="color:white;">Tulsi Puja
                                </td>
                            </tr>
                            <tr>
                                <td style="color:white;">7:15 AM</td>
                                <td style="color:white;">Guru Puja
                                </td>
                            </tr>
                            <tr>
                                <td style="color:white;">7:30 AM</td>
                                <td style="color:white;">Sringar Darshan
                                </td>
                            </tr>
                            <tr>
                                <td style="color:white;">12:00 PM</td>
                                <td style="color:white;">Raj Bhoga Arati
                                </td>
                            </tr>
                            <tr>
                                <td style="color:white;">1:00 PM</td>
                                <td style="color:white;">Deities put to rest
                                </td>
                            </tr>
                            <tr>
                                <td style="color:white;">4:00 PM</td>
                                <td style="color:white;">Vaikalika Arati
                                </td>
                            </tr>
                            <tr>
                                <td style="color:white;">7:00 PM</td>
                                <td style="color:white;">Gaura Arati
                                </td>
                            </tr>
                            <tr>
                                <td style="color:white;">8:45 PM</td>
                                <td style="color:white;">Shayana Arati
                                </td>
                            </tr>
                            <tr>
                                <td style="color:white;">6:30pm</td>
                                <td style="color:white;">Darshan Closed
                                </td>
                            </tr>
                        </table>
                    </form>
                    <!-- <a href="#" class="tag-cloud-link tag-link-37 tag-link-position-1" style="font-size: 8pt;" aria-label="Christian (1 item)">Christian</a>
                    <a href="#" class="tag-cloud-link tag-link-38 tag-link-position-2" style="font-size: 8pt;" aria-label="Hindu (1 item)">Hindu</a>
                    <a href="#" class="tag-cloud-link tag-link-32 tag-link-position-3" style="font-size: 8pt;" aria-label="Magazine (1 item)">Magazine</a>
                    <a href="#" class="tag-cloud-link tag-link-36 tag-link-position-4" style="font-size: 8pt;" aria-label="Muslims (1 item)">Muslims</a>
                    <a href="#" class="tag-cloud-link tag-link-31 tag-link-position-5" style="font-size: 8pt;" aria-label="News (1 item)">News</a>
                    <a href="#" class="tag-cloud-link tag-link-34 tag-link-position-6" style="font-size: 8pt;" aria-label="SEO (1 item)">SEO</a>
                    <a href="#" class="tag-cloud-link tag-link-35 tag-link-position-7" style="font-size: 8pt;" aria-label="Themes (1 item)">Themes</a>
                    <a href="#" class="tag-cloud-link tag-link-33 tag-link-position-8" style="font-size: 8pt;" aria-label="WordPress (1 item)">WordPress</a> -->
                </div>
            </div>
        </aside>
        <aside id="meta-3" class="widget widget_meta">
            <div class="col-md-3"><h2 class="widget-title">External Links</h2>
                <ul>
                    <li><a href="https://www.tovp.org/" target="_blank"> TOVP</a></li>
                    <li><a href="http://krishna.com/" target="_blank"> Krishna</a></li>
                    <li><a href="http://news.iskcon.com/" target="_blank"> ISKCON News</a></li>
                    <li><a href="http://mayapur.tv/" target="_blank"> Mayapur.tv</a></li>
                    <li><a href="http://gbc.iskcon.org/" target="_blank"> GBC</a></li>
                    <li><a href="http://bbt.org/" target="_blank"> Bhaktivedanta Book Trust (BBT)</a></li>
                    <li><a href="http://www.dandavats.com/" target="_blank"> Dandavats</a></li>
                </ul>
            </div>
        </aside>
        <aside id="ccr_popular_posts-5" class="widget widget_ccr_popular_posts">
            <div class="col-md-3"><h2 class="widget-title">Founder Acharya</h2>
                <img src="../../images/SP-Footer.png" style="">
            </div>
        </aside>
    </div>
</div>
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="clearfix">
            </div>
            <div class="logo text-center">
                <div class="col-4 pull-right">
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                        <input type="hidden" name="cmd" value="_s-xclick">
                        <input type="hidden" name="hosted_button_id" value="K94FLQYGZG3W2">
                        <input type="image"
                               src="https://www.paypalobjects.com/WEBSCR-640-20110429-1/en_US/i/btn/btn_donateCC_LG.gif"
                               border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1"
                             height="1">
                    </form>
                </div>

                <a class="col-4 pull-left" href="../../index.php">
                    <img src="../../images/footer-logo.jpg" alt="ISKCON Columbus" align="middle">
                </a>
            </div>

            <!-- /.logo -->
            <div class="col-4 copyright text-center">
                <p>Copyright &#169; 2017 - All Rights with ISKCON Columbus
                </p>
            </div>
            <!-- .copyright -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</footer>
<link rel='stylesheet' id='layout-switer-css-css' href='../../css/switch.css?ver=4.8' type='text/css' media='all'/>
<link rel='stylesheet' id='color-css' href='../../css/default.css?ver=4.8' type='text/css' media='all'/>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {"root": "", "namespace": "contact-form-7\/v1"},
        "recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}},
        "cached": "1"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='../../js/scripts.js?ver=4.8'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var mejsL10n = {
        "language": "en-US",
        "strings": {
            "Close": "Close",
            "Fullscreen": "Fullscreen",
            "Turn off Fullscreen": "Turn off Fullscreen",
            "Go Fullscreen": "Go Fullscreen",
            "Download File": "Download File",
            "Download Video": "Download Video",
            "Play": "Play",
            "Pause": "Pause",
            "Captions\/Subtitles": "Captions\/Subtitles",
            "None": "None",
            "Time Slider": "Time Slider",
            "Skip back %1 seconds": "Skip back %1 seconds",
            "Video Player": "Video Player",
            "Audio Player": "Audio Player",
            "Volume Slider": "Volume Slider",
            "Mute Toggle": "Mute Toggle",
            "Unmute": "Unmute",
            "Mute": "Mute",
            "Use Up\/Down Arrow keys to increase or decrease volume.": "Use Up\/Down Arrow keys to increase or decrease volume.",
            "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.": "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds."
        }
    };
    var _wpmejsSettings = {"pluginPath": "\/peace\/wp-includes\/js\/mediaelement\/"};
    /* ]]> */
</script>
<script type='text/javascript' src='../../js/mediaelement-and-player.min.js?ver=2.22.0'></script>
<script type='text/javascript' src='../../js/wp-mediaelement.min.js?ver=4.8'></script>
<script type='text/javascript' src='../../js/jquery.jplayer.min.js?ver=2.8.3'></script>
<script type='text/javascript' src='../../js/jquery.mCustomScrollbar.concat.min.js?ver=1.0'></script>
<script type='text/javascript' src='../../js/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript' src='../../js/js.cookie.min.js?ver=2.1.4'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var woocommerce_params = {
        "ajax_url": "\/peace\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/peace\/?page_id=397&wc-ajax=%%endpoint%%"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='../../js/woocommerce.min.js?ver=3.0.8'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {
        "ajax_url": "\/peace\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/peace\/?page_id=397&wc-ajax=%%endpoint%%",
        "fragment_name": "wc_fragments"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='../../js/cart-fragments.min.js?ver=3.0.8'></script>
<script type='text/javascript' src='../../js/plugins.js?ver=1.0'></script>
<script type='text/javascript' src='../../js/functions.js?ver=1.0'></script>
<script type='text/javascript' src='../../js/jquery.easing.min.js?ver=3.0.4'></script>
<script type='text/javascript' src='../../js/masterslider.min.js?ver=3.0.4'></script>
<script type='text/javascript' src='../../js/wp-embed.min.js?ver=4.8'></script>
<script type='text/javascript' src='../../js/farbtastic.js?ver=1.0.0'></script>