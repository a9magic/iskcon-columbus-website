
<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<title>Open Rededication &#8211; Peace</title>
		<?php
			include('meta_links.php');
		?>
</head>

<body class="events-template-default single single-events postid-74 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">




 <!-- peace layout start. end in footer.php -->
<div id="peace-layout">

<?php
include('partials/header.php');
?>
<!-- Blog Heading -->
<section id="blog-heading">
	<div class="heading-section">
		<div class="container">
			<div class="heading-text pull-left">
				<h1 class="blog-title">Event</h1>
			</div><!-- /.heading-text -->

			<div class="breadcrumb pull-right">
				<a href="../../index.php">Home</a><i class="fa fa-angle-double-right"></i> Sunday Feast</div>
		</div><!-- /.container -->
	</div><!-- /.heading-fullwidth -->		
</section><!-- /#blog-heading -->
<!-- Blog Heading end -->

<!-- Blog Page Container -->
<section id="blog-page-container">
	<div class="blog-page-wrapper">
		<div class="container">
			<div class="row">


				 
				<div class="col-md-8">
					<div class="all-event">
						<div class="event-post">
							

															<div class="events-date">
									<span class="event-date">10</span> <br>

									<span class="event-month">Nov</span>
								</div>
								
								<h2 class="uppercase"><a href="http://themes.codexcoder.com/peace/?events=open-house-and-rededication" title="Open Rededication"> Sunday Feast</a></h2>
								<div class="event-period">
									<span class="period-session">
										<i class="fa fa-clock-o"></i> 3 Hours <span class="separator"></span> <i class="fa fa-map-marker"></i> PO Box 16122, Collins Street									</span><span class="separator"></span>
									<span class="comments-share">
										<i class="fa fa-share-alt">
											<span class="social-button">
												<a href="http://www.facebook.com/sharer.php?u=http://themes.codexcoder.com/peace/?events=open-house-and-rededication&amp;t=Open Rededication" title="Share this post on Facebook!" onclick="window.open(this.href); return false;"><i class="fa fa-facebook"></i></a>
												<a href="http://twitter.com/home?status=Reading: http://themes.codexcoder.com/peace/?events=open-house-and-rededication" title="Share this post on Twitter!" target="_blank"><i class="fa fa-twitter"></i></a>
												<a href="https://plus.google.com/share?url=http://themes.codexcoder.com/peace/?events=open-house-and-rededication" title="Share this post on google plus!" target="_blank"><i class="fa fa-google-plus"></i></a>
												<a class="linkedin" href="http://www.linkedin.com/shareArticle?mini=true&amp;title=Open Rededication&amp;url=http://themes.codexcoder.com/peace/?events=open-house-and-rededication" title="Share on LinkedIn" rel="external nofollow" rel="nofollow" target="_blank"><i class="fa fa-linkedin"></i></a>
												<a class="pinterest" href="http://pinterest.com/pin/create/button/?url=http://themes.codexcoder.com/peace/?events=open-house-and-rededication&media=http://themes.codexcoder.com/peace/wp-content/uploads/2015/09/gallery271.jpg" title="Pinterest" rel="nofollow" target="_blank"><i class="fa fa-pinterest"></i></a>
											</span>
										</i>

									</span><!-- /.comments-share -->
								</div><!-- /.event-period -->
																<div class="feature-img">
									<img width="800" height="550" src="http://themes.codexcoder.com/peace/wp-content/uploads/2015/09/gallery271.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="http://themes.codexcoder.com/peace/wp-content/uploads/2015/09/gallery271.jpg 800w, http://themes.codexcoder.com/peace/wp-content/uploads/2015/09/gallery271-300x206.jpg 300w" sizes="(max-width: 800px) 100vw, 800px" />								</div>
																<div class="event-description">
									<p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus suscipit tortor eget felis porttitor volutpat. Proin eget tortor risus. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur aliquet quam id dui posuere blandit. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem.</p>
<p>Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Curabitur aliquet quam id dui posuere blandit. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sollicitudin molestie malesuada. Proin eget tortor risus.</p>
								</div>

							</div>
						</div>
				</div>
								<div class="col-md-4">
					<div class="event-location">
							<h2 class="widget-title">View Location</h2>
														<div class="eventMap"></div>
							<script>
								jQuery(document).ready(function() {
									var teamID = jQuery(".eventMap");
									if(teamID.length) {
										function isMobile() { 
											return ('ontouchstart' in document.documentElement);
										}

										function init_gmap() {
											if ( typeof google == 'undefined' ) return;
											var options = {
												center: [-37.817148, 144.955663],
												zoom: 15,
												mapTypeControl: true,
												mapTypeControlOptions: {
													style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
												},
												navigationControl: true,
												scrollwheel: false,
												streetViewControl: true
											}

											if (isMobile()) {
												options.draggable = false;
											}

											jQuery('.eventMap').gmap3({
												map: {
													options: options
												},
												marker: {
													latLng: [-37.817148, 144.955663],
													options: { icon: "" }
												}
											});
										}

										init_gmap();
									}

								});

							</script>
						
						</div>

						<div class="event-detail">
							<h2 class="widget-title">Event Details</h2>
							<div class="address">
								<div class="media">
									<div class="media-left"><i class="fa fa-map-marker base-color"></i></div> <div class="media-body">PO Box 16122, Collins Street West Victoria, 8007 Australia<br></div> 
								</div>
								<div class="media">
									<div class="media-left"><i class="fa fa-calendar-o base-color"></i></div> <div class="media-body">10 November, 2015<br></div> 
								</div>
								<div class="media">
									<div class="media-left"><i class="fa fa-clock-o base-color"></i></div> <div class="media-body">10.00 AM<br></div>
								</div>
								<div class="media">
									<div class="media-left"><i class="fa fa-envelope-o base-color"></i></div> <div class="media-body">demo@demo.com<br></div> 
								</div>
							</div>
							<div class="btn custom-btn"><a href="#">Join Now</a></div>
						</div>
					</div>
			</div><!-- row -->
		</div> <!-- #container -->
	</div><!-- #blog-page-wrapper -->
</section>

<div class="clearfix"></div>
<div id="location">
	<div class="container">
		<div class="row">

			<div class="social-icons">
							 <ul>
										<li class="facebook"><a href="http://facebook.com/id"><i class="fa fa-facebook"></i><span>Facebook</span></a></li>
															<li class="twitter"><a href="http://twitter.com/id"><i class="fa fa-twitter"></i><span>Twitter</span></a></li>
															<li class="vimeo"><a href="http://vimeo.com/id"><i class="fa fa-vimeo-square"></i><span>Vimeo</span></a></li>
															<li class="behance"><a href="http://behance.com/id"><i class="fa fa-behance"></i><span>Behance</span></a></li>
									</ul>
			</div>
		</div><!-- /.row -->
	</div><!-- /.container -->
</div>
<?php
include('footer.php');
?>

	
</body>
</html>

<!-- Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/

Object Caching 2174/2207 objects using disk
Content Delivery Network via N/A
Database Caching 9/21 queries in 0.005 seconds using disk

 Served from: themes.codexcoder.com @ 2017-06-14 08:07:33 by W3 Total Cache -->