<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <title>Services
    </title>
    <?php
    include('meta_links.php');
    ?>
</head>
<body class="page-template page-template-page-template page-template-event-page page-template-page-templateevent-page-php page page-id-399 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- /#preloader -->
<!-- peace layout start. end in footer.php -->
<div id="peace-layout">
    <?php
    include('partials/header.php');
    ?>
    <!-- Blog Heading -->
    <section id="blog-heading" style="height:auto;border:2px solid #e1e1e1;">
        <div class="heading-section">
            <div class="container">
                <div class="heading-text pull-left" style="width:99%;padding-top:41px;">
                    <h1 class="blog-title" style="text-align:center;font-size:31px;">Services
                    </h1>
                    <div class="event-description col-6 text-center" style="padding:11px;">
                        The best way to begin devotional service is to try to offer everything you do, or some portion
                        of everything you do, to Krishna. Devotional service is dependent on the mercy of Krishna and
                        His Devotees.
                    </div>
                </div>
                <!-- /.heading-text -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.heading-fullwidth -->
    </section>
    <!-- /#blog-heading -->
    <!-- Blog Heading end -->
    <section id="blog-page-container">
        <div class="blog-page-wrapper">
            <div id="primary" class="container">
                <main id="main" class="main-content" role="main">

                    <!-- Sunday School -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">18
                        </span>
                                        <br>
                                        <span class="event-month">June
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="sunday_school.php"
                                           title="Sunday School">Sunday School
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-clock-o">
                          </i> 1 Hour
                          <span class="separator">
                          </span>
                          <i class="fa fa-map-marker">
                          </i> 379 West 8th Ave Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="sunday_school.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="../../images/sunday-school.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="../../images/sunday-school.jpg 570w, images/sunday-school.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>

                    <!-- Govinda's catering-->
                    <div class="blog-content">
                        <div class="blog-item col-md-4">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">1
                        </span>
                                        <br>
                                        <span class="event-month">July
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="govindas-catering.php"
                                           title="Govinda's catering">Govinda's catering
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-map-marker">
                          </i> 379 West 8th Ave Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="govindas-catering.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="../../images/govindas-catering.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="../../images/govindas-catering.jpg 570w, images/govindas-catering.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>

                    <!-- Govinda's Bakery -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">30
                        </span>
                                        <br>
                                        <span class="event-month">June
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="https://www.facebook.com/108delectabledelights" target="_blank"
                                           title="Govinda's Bakery">Govinda's Bakery
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-clock-o">
                          </i> 9 am - 5 pm
                          <span class="separator">
                          </span>
                          <i class="fa fa-map-marker">
                          </i> Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="https://www.facebook.com/108delectabledelights" target="_blank">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="../../images/govindas-bakery.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="../../images/govindas-bakery.jpg 570w, images/govindas-bakery.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>

                    <!-- Book Distribution -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">June
                        </span>
                                        <br>
                                        <span class="event-month">
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="book_distribution.php"
                                           title="Book Distribution">Book Distribution
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">
                          <i class="fa fa-map-marker">
                          </i> Across Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="book_distribution.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="../../images/book-distribution.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="../../images/book-distribution.jpg 570w, images/book-distribution.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>

                    <!-- Festivals -->
                    <div class="blog-content">
                        <div class="blog-item col-md-4">
                            <div class="all-event">
                                <div class="event-post">
                                    <div class="events-date">
                        <span class="event-date">8
                        </span>
                                        <br>
                                        <span class="event-month">July
                        </span>
                                    </div>
                                    <h2 class="uppercase">
                                        <a href="festival.php"
                                           title="Festivals">Festivals
                                        </a>
                                    </h2>
                                    <div class="event-period">
                        <span class="period-session">s
                          <i class="fa fa-map-marker">
                          </i> 379 West 8th Ave Columbus, OH
                        </span>
                                        <span class="separator">
                        </span>
                                        <?php include('socialmedia-share.php'); ?>
                                        <!-- /.comments-share -->
                                    </div>
                                    <!-- /.event-period -->
                                    <a href="festival.php">
                                        <div class="feature-img">
                                            <img width="570" height="341"
                                                 src="../../images/festivals.jpg"
                                                 class="attachment-post-thumbnail size-post-thumbnail wp-post-image"
                                                 alt=""
                                                 srcset="../../images/festivals.jpg 570w, images/festivals.jpg 300w"
                                                 sizes="(max-width: 570px) 100vw, 570px"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>


                    </div>

                </main>
                <!-- #main -->
            </div>
            <!-- #primary -->
        </div>
    </section>

    <?php
    include('footer.php');
    ?>

</div>
<!-- /.peace layout end -->
<!-- Color Variation Switcher  -->

<!-- Color Variation Switcher End -->
<link rel='stylesheet' id='layout-switer-css-css'
      href='http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/switch.css?ver=4.8'
      type='text/css' media='all'/>
<link rel='stylesheet' id='color-css'
      href='http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/default.css?ver=4.8'
      type='text/css' media='all'/>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "http:\/\/themes.codexcoder.com\/peace\/?rest_route=\/", "namespace": "contact-form-7\/v1"
        }
        , "recaptcha": {
            "messages": {
                "empty": "Please verify that you are not a robot."
            }
        }
        , "cached": "1"
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='http://themes.codexcoder.com/peace/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=4.8'>
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var mejsL10n = {
        "language": "en-US", "strings": {
            "Close": "Close",
            "Fullscreen": "Fullscreen",
            "Turn off Fullscreen": "Turn off Fullscreen",
            "Go Fullscreen": "Go Fullscreen",
            "Download File": "Download File",
            "Download Video": "Download Video",
            "Play": "Play",
            "Pause": "Pause",
            "Captions\/Subtitles": "Captions\/Subtitles",
            "None": "None",
            "Time Slider": "Time Slider",
            "Skip back %1 seconds": "Skip back %1 seconds",
            "Video Player": "Video Player",
            "Audio Player": "Audio Player",
            "Volume Slider": "Volume Slider",
            "Mute Toggle": "Mute Toggle",
            "Unmute": "Unmute",
            "Mute": "Mute",
            "Use Up\/Down Arrow keys to increase or decrease volume.": "Use Up\/Down Arrow keys to increase or decrease volume.",
            "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds.": "Use Left\/Right Arrow keys to advance one second, Up\/Down arrows to advance ten seconds."
        }
    };
    var _wpmejsSettings = {
        "pluginPath": "\/peace\/wp-includes\/js\/mediaelement\/"
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='http://themes.codexcoder.com/peace/wp-includes/js/mediaelement/mediaelement-and-player.min.js?ver=2.22.0'>
</script>
<script type='text/javascript'
        src='http://themes.codexcoder.com/peace/wp-includes/js/mediaelement/wp-mediaelement.min.js?ver=4.8'>
</script>
<script type='text/javascript'
        src='http://themes.codexcoder.com/peace/wp-content/plugins/verse-reader-with-audio/js/jquery.jplayer.min.js?ver=2.8.3'>
</script>
<script type='text/javascript'
        src='http://themes.codexcoder.com/peace/wp-content/plugins/verse-reader-with-audio/js/jquery.mCustomScrollbar.concat.min.js?ver=1.0'>
</script>
<script type='text/javascript'
        src='//themes.codexcoder.com/peace/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'>
</script>
<script type='text/javascript'
        src='//themes.codexcoder.com/peace/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'>
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var woocommerce_params = {
        "ajax_url": "\/peace\/wp-admin\/admin-ajax.php", "wc_ajax_url": "\/peace\/?page_id=399&wc-ajax=%%endpoint%%"
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='//themes.codexcoder.com/peace/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=3.0.8'>
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {
        "ajax_url": "\/peace\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/peace\/?page_id=399&wc-ajax=%%endpoint%%",
        "fragment_name": "wc_fragments"
    };
    /* ]]> */
</script>
<script type='text/javascript'
        src='//themes.codexcoder.com/peace/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=3.0.8'>
</script>
<script type='text/javascript'
        src='http://themes.codexcoder.com/peace/wp-content/themes/peace/assets/js/plugins.js?ver=1.0'>
</script>
<script type='text/javascript'
        src='http://themes.codexcoder.com/peace/wp-content/themes/peace/assets/js/functions.js?ver=1.0'>
</script>
<script type='text/javascript'
        src='http://themes.codexcoder.com/peace/wp-content/plugins/masterslider/public/assets/js/jquery.easing.min.js?ver=3.0.4'>
</script>
<script type='text/javascript'
        src='http://themes.codexcoder.com/peace/wp-content/plugins/masterslider/public/assets/js/masterslider.min.js?ver=3.0.4'>
</script>
<script type='text/javascript' src='http://themes.codexcoder.com/peace/wp-includes/js/wp-embed.min.js?ver=4.8'>
</script>
<script type='text/javascript'
        src='http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/js/farbtastic.js?ver=1.0.0'>
</script>
<script>
    /*-----------------------------------------------------------------------------------
     /* Styles Switcher
     -----------------------------------------------------------------------------------*/
    window.console = window.console || (function () {
            var c = {};
            c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function () {
            };
            return c;
        })();
    jQuery(document).ready(function ($) {
            jQuery('#layout1').click(function () {
                    $("#peace-layout").removeClass('peace-box-layout');
                }
            );
            jQuery('#layout2').click(function () {
                    $("#peace-layout").addClass('peace-box-layout');
                }
            );
            $("ul.colors .color1").click(function () {
                    $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/default.css");
                    return false;
                }
            );
            $("ul.colors .color2").click(function () {
                    $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-1.css");
                    return false;
                }
            );
            $("ul.colors .color3").click(function () {
                    $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-2.css");
                    return false;
                }
            );
            $("ul.colors .color4").click(function () {
                    $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-3.css");
                    return false;
                }
            );
            $("ul.colors .color5").click(function () {
                    $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-4.css");
                    return false;
                }
            );
            $("ul.colors .color6").click(function () {
                    $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-5.css");
                    return false;
                }
            );
            $("ul.colors .color7").click(function () {
                    $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-6.css");
                    return false;
                }
            );
            $("ul.colors .color8").click(function () {
                    $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-7.css");
                    return false;
                }
            );
            $("ul.colors .color9").click(function () {
                    $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-8.css");
                    return false;
                }
            );
            $("ul.colors .color10").click(function () {
                    $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-9.css");
                    return false;
                }
            );
            // jQuery('#colors').farbtastic(function(color) {
            //     // cool stuff goes here
            //     jQuery('#picked').text(color);
            //     jQuery('#result').css('background', color);
            //     // hover font color
            //     jQuery('.blog-sidebar .btn,.blog-section .comments,.blog-section .post-title a,.blog-sidebar .widget_archive a,.blog-sidebar .widget_categories a,.blog-sidebar .widget_nav_menu a,.blog-sidebar .widget_recent_entries a,.causes-post .caption-txt .donated,.copyrights a,.footer-social-btn a,.galleryFilter .current,.galleryFilter a:focus,.galleryFilter a,.navbar-default .navbar-nav>li>a:focus').hover().css('color', color);
            //     //hover border color
            //     jQuery('.contact-form-container .custom-btn').hover().css('border-color', color);
            //     //hover backgound color
            //     jQuery('.link-hex, .blog-sidebar .btn').hover().css('background-color', color);
            //     //hover in a class
            //     jQuery('.gallery-item figure, .service-box, .pricing-item').hover(function(){
            //     	jQuery('.item-head').css('background-color', color);
            //     });
            //     jQuery('.causes-post .caption-txt .donated').css('color', color);
            //     jQuery('.progress-bar-container .progress-bar-warning, .donate-btn, .archive #main-menu.navbar-default,.blog #main-menu.navbar-default,.carousel-indicators li.active,.causes-post .custom-progress-bar,.donate-btn:hover,.event-timeline,.gallery-item figure:hover .item-description,.hex.scroll-top:after,.hex.scroll-top:before,.hex:after,.hex:before,.link-hex:hover,.news-article .meta-icon,.owl-page.active,.page-template-default #main-menu.navbar-default,.parallax-title:after,.pricing-item .item-name:after,.pricing-item:hover .item-head,.publish-date,.section-title:after,.service-box:hover .service-icon-hex,.service-box:hover .service-icon-hex:after,.single #main-menu.navbar-default,.single-causes-post .custom-progress-bar,.slide-nav:hover,.team-member-box:before,.team-member-box:hover .member-designation:after,.section-title:after').css('background-color', color);
            //     jQuery('.page-template-page-templatefront-page-php .menu-bg').css('background-color', color);
            //     jQuery('.carousel-indicators li.active,.comment-form .form-control:focus,.contact-form-container .custom-btn,.contact-form-container .form-control:focus,.contact-info .contact-address li:before,.galleryFilter .current,.gray-bg .custom-btn,.link-hex,.owl-page.active,.post-box .custom-btn,.single-event-post .time-circle .time-number,.time-circle .time-number,.white-bg .custom-btn').css('border-color', color);
            // });
            jQuery("#color-style-switcher .bottom a.settings").click(function (e) {
                    e.preventDefault();
                    var div = jQuery("#color-style-switcher");
                    if (div.css("left") === "-215px") {
                        jQuery("#color-style-switcher").animate({
                                left: "0px"
                            }
                        );
                    }
                    else {
                        jQuery("#color-style-switcher").animate({
                                left: "-215px"
                            }
                        );
                    }
                }
            )
            jQuery("ul.colors li a").click(function (e) {
                    e.preventDefault();
                    jQuery(this).parent().parent().find("a").removeClass("active");
                    jQuery(this).addClass("active");
                }
            )
        }
    );
</script>
</body>
</html>
<!-- Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/
Object Caching 2393/2419 objects using disk
Content Delivery Network via N/A
Database Caching 9/17 queries in 0.021 seconds using disk
Served from: themes.codexcoder.com @ 2017-06-14 08:10:14 by W3 Total Cache -->
