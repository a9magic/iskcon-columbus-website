<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Contact Us &#8211; Peace</title>
    <?php
    include("partials/meta_links.php");
    ?>

<body class="page-template page-template-page-template page-template-front-page page-template-page-templatefront-page-php page page-id-384 _masterslider _msp_version_3.0.4 wpb-js-composer js-comp-ver-4.12.1 vc_responsive">

<!-- peace layout start. end in footer.php -->
<div id="peace-layout">

    <?php
    include("contact.php");
    ?>


    <div class="vc_row-fluid vc_custom_1441963525429">
        <div class="container">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">
                    <h2 class="section-title wow fadeInUp" data-wow-delay="0.4s">Send Us a Message</h2>
                    <!-- /.section-title -->
                    <div class="section-detail wow fadeInUp" data-wow-delay="0.6s">Enthusiastically underwhelm quality
                        benefits rather than professional outside the box thinking. Distinctively network highly
                        efficient leadership skills
                    </div><!-- /.section-detail -->
                    <div role="form" class="wpcf7" id="wpcf7-f7-p384-o1" lang="en-US" dir="ltr">
                        <div class="screen-reader-response"></div>
                        <form action="/peace/?page_id=384#wpcf7-f7-p384-o1" method="post" class="wpcf7-form"
                              novalidate="novalidate">
                            <div style="display: none;">
                                <input type="hidden" name="_wpcf7" value="7"/>
                                <input type="hidden" name="_wpcf7_version" value="4.8"/>
                                <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f7-p384-o1"/>
                                <input type="hidden" name="_wpcf7_container_post" value="384"/>
                                <input type="hidden" name="_wpcf7_nonce" value="bfbea92b91"/>
                            </div>
                            <div class="col-md-6 form-group"><label class="sr-only">Full Name</label><span
                                        class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name"
                                                                                         value="" size="40"
                                                                                         class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control"
                                                                                         aria-required="true"
                                                                                         aria-invalid="false"
                                                                                         placeholder="Full Name"/></span>
                            </div>
                            <div class="col-md-6 form-group"><label class="sr-only">Email Address</label><span
                                        class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email"
                                                                                          value="" size="40"
                                                                                          class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control"
                                                                                          aria-required="true"
                                                                                          aria-invalid="false"
                                                                                          placeholder="Email Address"/></span>
                            </div>
                            <div class="form-group col-md-12"><label class="sr-only">Subject</label><span
                                        class="wpcf7-form-control-wrap your-subject"><input type="text"
                                                                                            name="your-subject" value=""
                                                                                            size="40"
                                                                                            class="wpcf7-form-control wpcf7-text form-control"
                                                                                            aria-invalid="false"
                                                                                            placeholder="Subject"/></span>
                            </div>
                            <div class="form-group col-md-12">
                                <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message"
                                                                                             cols="40" rows="5"
                                                                                             class="wpcf7-form-control wpcf7-textarea form-control input-md"
                                                                                             aria-invalid="false"
                                                                                             placeholder="YOUR MESSAGE"></textarea></span>
                            </div>
                            <p class="event-btn-container col-md-12">
                                <input type="submit" value="SEND NOW"
                                       class="wpcf7-form-control wpcf7-submit btn custom-btn"/>
                            </p>
                            <div class="wpcf7-response-output wpcf7-display-none"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="full-width">
        <div class="vc_row-fluid vc_custom_1441957463603">
            <div class="vc_col-sm-12 wpb_column vc_column_container ">
                <div class="wpb_wrapper">

                    <div class="map-container wow fadeInUp" data-wow-delay="0.4s">
                        <div class="googleMaps google-map-container"></div>
                        <div class="contact-info col-md-6 pull-right wow fadeInRight" data-wow-delay="0.6s">
                            <div class="contact-person">
                                <h2></h2>
                                <img width="150" height="150"
                                     src="http://themes.codexcoder.com/peace/wp-content/uploads/2015/09/personimg02.jpg"
                                     class="img-circle" alt="team-3">
                                <div class="info">
                                    <h3>Partha Das</h3>
                                    <p>Head Priest</p>
                                    <span><b>TEL:</b> 02 9185 587</span><br>
                                    <span><b>Email:</b> iskconcolumbus@gmail.com</span>
                                </div><!-- /.info -->
                            </div><!-- /.contact-person -->

                            <div class="contact-information">
                                <h2></h2>
                                <div class="address">
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-map-marker base-color"></i></div>
                                        <div class="media-body">379 West 8th Ave Columbus, OH 43201<br></div>
                                    </div>
                                    <div class="media">
                                        <div class="media-left"><i class="fa fa-clock-o base-color"></i></div>
                                        <div class="media-body"><strong>Monday-Thursday :</strong> 05:00 am - 09:00
                                            pm<br/>
                                            <strong>Friday :</strong> 5:00 am - 12:30 am<br/>
                                            <strong>Saturday :</strong> 5:30 am - 12:30 pm<br/>
                                            <strong>Sunday :</strong> 05:00 am - 09:00
                                        </div>
                                    </div>
                                </div><!-- /.info -->
                            </div><!-- /.contact-person -->
                        </div>
                    </div>
                    <!-- Google Map Section End -->
                    <script type="text/javascript">

                        jQuery(document).ready(function () {
                            var teamID = jQuery(".googleMaps");
                            if (teamID.length) {
                                /*----------- Google Map - with support of gmaps.js ----------------*/
                                function isMobile() {
                                    return ('ontouchstart' in document.documentElement);
                                }

                                function init_gmap() {
                                    if (typeof google == 'undefined') return;
                                    var options = {
                                        center: [23.739393, 90.389195],
                                        zoom: 15,
                                        mapTypeControl: true,
                                        mapTypeControlOptions: {
                                            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                                        },
                                        navigationControl: true,
                                        scrollwheel: false,
                                        streetViewControl: true
                                    }

                                    if (isMobile()) {
                                        options.draggable = false;
                                    }

                                    jQuery('.googleMaps').gmap3({
                                        map: {
                                            options: options
                                        },
                                        marker: {
                                            latLng: [23.739393, 90.389195],
                                            options: {icon: "http://themes.codexcoder.com/peace/wp-content/uploads/2015/09/map-icon.png"}
                                        }
                                    });
                                }

                                init_gmap();
                            }

                        });

                    </script>

                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div id="location">
        <div class="container">
            <div class="row">

                <div class="social-icons">
                    <ul>
                        <li class="facebook"><a href="http://facebook.com/id"><i class="fa fa-facebook"></i><span>Facebook</span></a>
                        </li>
                        <li class="twitter"><a href="http://twitter.com/id"><i
                                        class="fa fa-twitter"></i><span>Twitter</span></a></li>
                        <li class="vimeo"><a href="http://vimeo.com/id"><i
                                        class="fa fa-vimeo-square"></i><span>Vimeo</span></a></li>
                        <li class="behance"><a href="http://behance.com/id"><i
                                        class="fa fa-behance"></i><span>Behance</span></a></li>
                    </ul>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>

    <?php
    include("footer.php");
    ?>

</div>  <!-- /.peace layout end -->
<!-- Color Variation Switcher End -->

<script>
    /*-----------------------------------------------------------------------------------
     /* Styles Switcher
     -----------------------------------------------------------------------------------*/

    window.console = window.console || (function () {
            var c = {};
            c.log = c.warn = c.debug = c.info = c.error = c.time = c.dir = c.profile = c.clear = c.exception = c.trace = c.assert = function () {
            };
            return c;
        })();


    jQuery(document).ready(function ($) {

        jQuery('#layout1').click(function () {
            $("#peace-layout").removeClass('peace-box-layout');
        });

        jQuery('#layout2').click(function () {
            $("#peace-layout").addClass('peace-box-layout');
        });

        $("ul.colors .color1").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/default.css");
            return false;
        });

        $("ul.colors .color2").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-1.css");
            return false;
        });

        $("ul.colors .color3").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-2.css");
            return false;
        });

        $("ul.colors .color4").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-3.css");
            return false;
        });

        $("ul.colors .color5").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-4.css");
            return false;
        });

        $("ul.colors .color6").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-5.css");
            return false;
        });

        $("ul.colors .color7").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-6.css");
            return false;
        });
        $("ul.colors .color8").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-7.css");
            return false;
        });
        $("ul.colors .color9").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-8.css");
            return false;
        });
        $("ul.colors .color10").click(function () {
            $("#color-css").attr("href", "http://themes.codexcoder.com/peace/wp-content/plugins/theme-layout-switch/css/color-9.css");
            return false;

        });


        // jQuery('#colors').farbtastic(function(color) {
        //     // cool stuff goes here
        //     jQuery('#picked').text(color);

        //     jQuery('#result').css('background', color);

        //     // hover font color
        //     jQuery('.blog-sidebar .btn,.blog-section .comments,.blog-section .post-title a,.blog-sidebar .widget_archive a,.blog-sidebar .widget_categories a,.blog-sidebar .widget_nav_menu a,.blog-sidebar .widget_recent_entries a,.causes-post .caption-txt .donated,.copyrights a,.footer-social-btn a,.galleryFilter .current,.galleryFilter a:focus,.galleryFilter a,.navbar-default .navbar-nav>li>a:focus').hover().css('color', color);

        //     //hover border color
        //     jQuery('.contact-form-container .custom-btn').hover().css('border-color', color);

        //     //hover backgound color
        //     jQuery('.link-hex, .blog-sidebar .btn').hover().css('background-color', color);

        //     //hover in a class
        //     jQuery('.gallery-item figure, .service-box, .pricing-item').hover(function(){
        //     	jQuery('.item-head').css('background-color', color);
        //     });


        //     jQuery('.causes-post .caption-txt .donated').css('color', color);

        //     jQuery('.progress-bar-container .progress-bar-warning, .donate-btn, .archive #main-menu.navbar-default,.blog #main-menu.navbar-default,.carousel-indicators li.active,.causes-post .custom-progress-bar,.donate-btn:hover,.event-timeline,.gallery-item figure:hover .item-description,.hex.scroll-top:after,.hex.scroll-top:before,.hex:after,.hex:before,.link-hex:hover,.news-article .meta-icon,.owl-page.active,.page-template-default #main-menu.navbar-default,.parallax-title:after,.pricing-item .item-name:after,.pricing-item:hover .item-head,.publish-date,.section-title:after,.service-box:hover .service-icon-hex,.service-box:hover .service-icon-hex:after,.single #main-menu.navbar-default,.single-causes-post .custom-progress-bar,.slide-nav:hover,.team-member-box:before,.team-member-box:hover .member-designation:after,.section-title:after').css('background-color', color);

        //     jQuery('.page-template-page-templatefront-page-php .menu-bg').css('background-color', color);

        //     jQuery('.carousel-indicators li.active,.comment-form .form-control:focus,.contact-form-container .custom-btn,.contact-form-container .form-control:focus,.contact-info .contact-address li:before,.galleryFilter .current,.gray-bg .custom-btn,.link-hex,.owl-page.active,.post-box .custom-btn,.single-event-post .time-circle .time-number,.time-circle .time-number,.white-bg .custom-btn').css('border-color', color);

        // });


        jQuery("#color-style-switcher .bottom a.settings").click(function (e) {
            e.preventDefault();
            var div = jQuery("#color-style-switcher");
            if (div.css("left") === "-215px") {
                jQuery("#color-style-switcher").animate({
                    left: "0px"
                });
            } else {
                jQuery("#color-style-switcher").animate({
                    left: "-215px"
                });
            }
        })

        jQuery("ul.colors li a").click(function (e) {
            e.preventDefault();
            jQuery(this).parent().parent().find("a").removeClass("active");
            jQuery(this).addClass("active");
        })

    });
</script>


</body>
</html>

<!-- Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/

Object Caching 1993/2408 objects using disk
Content Delivery Network via N/A
Database Caching using disk

 Served from: themes.codexcoder.com @ 2017-06-13 06:26:36 by W3 Total Cache -->